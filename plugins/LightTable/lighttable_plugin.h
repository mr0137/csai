#ifndef LIGHTTABLE_PLUGIN_H
#define LIGHTTABLE_PLUGIN_H

#include <QQmlExtensionPlugin>

class LightTablePlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override;
};

#endif // LIGHTTABLE_PLUGIN_H
