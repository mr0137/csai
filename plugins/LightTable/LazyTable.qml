import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import LightTable 1.0
import LightTablePrivate 1.0
/*!
    \qmltype LazyTable
    \inqmlmodule LightTable
    \since 1.0
    \brief A Table that allows the user to view database's content.
    Example of usage:
    \qml
    LazyTable {
        width: 200
        height: 200
        provider: form.provider
        style: LazyTableStyle.Excel
    }
    \endqml
    \note You can create a custom appearance for a LazyTable by
    assigning a \l {TableStyle}.
*/

Rectangle{
    id: root
    /*!
      \qmlproperty ITableDataProvider LazyTable::provider
      \required
      \brief data provider of table
    */
    required property var provider
    /*!
      \qmlproperty var LazyTable::style
      \brief style could be enum LazyTableStyle::Style or TableStyle type.
      Possible enumerations:
      \list
      \li LazyTableStyle.Default (default)
      \li LazyTableStyle.Excel
      \endlist
    */
    property var style: LazyTableStyle.Default
    /*!
        \qmlproperty bool LazyTable::horizontalScrollbarAvailable
        \readonly
        \brief horizontalScrollbarAvailable used to enable or disable horizontal scrollbar.
    */
    property bool horizontalScrollbarAvailable: true
    /*!
        \qmlproperty bool LazyTable::verticalScrollbarAvailable
        \readonly
        \brief verticalScrollbarAvailable enable or disable horizontal scrollbar.
    */
    property bool verticalScrollbarAvailable: true
    /*!
        \qmlproperty bool LazyTable::resizing
        \readonly
        \brief resizing enable or disable resizing mode.
    */
    readonly property alias resizing: table.resizing
    /*!
        \qmlproperty QRectF LazyTable::viewPos
        \readonly
        Display geometry of visual rect.
        Internal fields:
        \list
        \li \c x
        \li \c y
        \li \c width
        \li \c height
        \endlist
    */
    readonly property alias viewPos: table.viewPos

    readonly property alias currentRow: table.currentRow
    readonly property alias currentColumn: table.currentColumn
    readonly property alias selectedRowsCount: table.selectedRowsCount
    readonly property alias selectedColumnCount: table.selectedColumnsCount
    readonly property real contentHeight: table.contentHeight + table.style.horizontalHeader.height
    readonly property real contentWidth: table.contentWidth + (table.style.verticalHeaderAvailable ? table.style.verticalHeader.width : 0)
    readonly property alias editable: table.editable
    property color scrollbarColor: "transparent"
    property var editableColumns: new Array
    property color backgroundColor: Qt.darker("gray", 1.1)
    /*!
        \qmlsignal LazyTable::headerPressed(int column, int headerSpace, var rect)
        Signal is emited when the user press the header.
        Params:
        \list
            \li \c column show where it been pressed
            \li \c headerSpace display special position of click by enumeration LazyTableStyle::HeaderSpace.
            \li \c rect show geometry of specified header column.
        \endlist
        The corresponding handler is \c onHeaderPressed.
    */
    signal headerPressed(int button, int column, int headerSpace, var rect)
    signal keyPressed(var event, int row, int column)
    signal doubleClicked(var event, int row, int column)
    signal showContextMenu(var pos)

    property var enterEditBehaviour: () => {
                                         table.setCellValue(table.currentRow, table.currentColumn, textEdit.text)
                                         table.increaseRow()
                                     }



    color: backgroundColor

    onVerticalScrollbarAvailableChanged: {
        verticalScrollBar.visible = verticalScrollbarAvailable
    }

    onHorizontalScrollbarAvailableChanged: {
        horizontalScrollBar.visible = horizontalScrollbarAvailable
    }

    Popup{
        id: editPopup
        enabled: root.editable
        property rect editPos: enabled ? table.cellPos(table.currentRow, table.currentColumn) : Qt.rect(0,0,0,0)
        x: editPos.x + 2 - table.viewPos.x
        y: editPos.y + 2 - table.viewPos.y
        width: editPos.width - 2
        height: editPos.height - 2
        z: 10
        focus: true

        function editWithReplace(firstLetter){
            editPopup.forceActiveFocus()
            textEdit.text = "" + firstLetter
            textEdit.forceActiveFocus()
            editPopup.open()
        }

        function edit(){
            editPopup.forceActiveFocus()
            textEdit.text = table.cellValue(table.currentRow, table.currentColumn)
            textEdit.selectAll()
            textEdit.forceActiveFocus()
            editPopup.open()
        }

        onClosed:{
            table.forceActiveFocus()
        }

        background: Item{
            //! [background]
            Rectangle{
                anchors.fill: parent
                anchors.bottomMargin: 5
                anchors.rightMargin: 5
                color: editPopup.opened ? table.cellBackgroundColor(table.currentRow, table.currentColumn) : "transparent"
            }
            Rectangle{
                anchors.fill: parent
                anchors.topMargin: parent.height - 5
                anchors.rightMargin: 5
                color: editPopup.opened ? table.cellBackgroundColor(table.currentRow, table.currentColumn) : "transparent"
            }
            //! [background]

            TextField{
                id: textEdit
                anchors.fill: parent
                selectByMouse: true
                color: table.style.cell.textColor
                font.family: table.style.cell.font.family
                font.pointSize: table.style.cell.font.pointSize
                horizontalAlignment: table.style.cell.alignment

                Keys.onEnterPressed: {
                    enterEditBehaviour()
                    editPopup.close()
                }

                Keys.onReturnPressed: {
                    enterEditBehaviour()
                    editPopup.close()
                }

                Keys.onDownPressed: {
                    table.setCellValue(table.currentRow, table.currentColumn, textEdit.text)
                    editPopup.close()
                    table.increaseRow()
                }

                Keys.onUpPressed: {
                    table.setCellValue(table.currentRow, table.currentColumn, textEdit.text)
                    editPopup.close()
                    table.decreaseRow()
                }

                Keys.onLeftPressed: {
                    table.setCellValue(table.currentRow, table.currentColumn, textEdit.text)
                    editPopup.close()
                    table.decreaseColumn()
                }

                Keys.onRightPressed: {
                    table.setCellValue(table.currentRow, table.currentColumn, textEdit.text)
                    editPopup.close()
                    table.increaseColumn()
                }

                Keys.onEscapePressed: {
                    editPopup.close()
                }

                background: Rectangle{
                    color: "transparent"
                }
            }
        }
    }

    onStyleChanged: {
        if (typeof(style) === 'number'){
            switch(style){
            case 0:
                table.style = LazyTableStyle.getDefaultStyle();
                break;
            case 1:
                table.style = LazyTableStyle.getExcelStyle();
                break;
            }
        }else if (typeof(style) === typeof(TableStyle)){
            table.style = style
        }
    }

    ColumnLayout{
        anchors.fill: root
        anchors.margins: 1
        spacing: 0
        RowLayout{
            Layout.fillHeight: true
            Layout.fillWidth: true
            spacing: 0

            TablePrivate {
                id: table
                Layout.fillWidth: true
                Layout.fillHeight: true
                provider: root.provider
                property rect prevViewPos: Qt.rect(0,0,0,0)
                onViewPosChanged: {
                    //! не убирать, а то будет вылетать
                    if (prevViewPos.x === viewPos.x && prevViewPos.y === viewPos.y) return;
                    //!
                    if (!horizontalScrollBar.pressed){
                        var horPosition = Math.min(Math.max(0, viewPos.x / (table.contentWidth - table.width)), 1)
                        if (horPosition !== horizontalScrollBar.position){
                            horizontalScrollBar.setPosition(horPosition)
                        }
                        console.log(horPosition)
                    }
                    if (!verticalScrollBar.pressed){
                        var verPosition = Math.min(Math.max(0, viewPos.y / (table.contentHeight - table.height)), 1)
                        if (verPosition !== verticalScrollBar.position){
                             verticalScrollBar.setPosition(verPosition)
                        }
                    }
                    prevViewPos = viewPos
                }

                Connections {
                    target: table

                    function onHeaderPressed(button, columnIndex, headerSpace, geometry){
                        headerPressed(button, columnIndex, headerSpace, geometry)
                    }

                    function onKeyPressed(event, row, column){
                        var ev = event
                        ev["accepted"] = false
                        root.keyPressed(ev, row, column)

                        if (!ev["accepted"]){
                            if (table.checkForEditable()){
                                if (ev.key === Qt.Key_F2){
                                    editPopup.forceActiveFocus()
                                    editPopup.edit()
                                }else if (ev.key >= Qt.Key_A && ev.key <= Qt.Key_Z){
                                    editPopup.forceActiveFocus()
                                    editPopup.editWithReplace(String.fromCharCode(ev.key))
                                }else if (ev.key >= Qt.Key_0 && ev.key <= Qt.Key_9){
                                    editPopup.forceActiveFocus()
                                    editPopup.editWithReplace(String.fromCharCode(ev.key))
                                }else if (ev.key === Qt.Key_Space){
                                    editPopup.forceActiveFocus()
                                    editPopup.editWithReplace("")
                                }
                            }

                        }
                    }

                    function onShowContextMenu(pos){
                        root.showContextMenu(pos)
                    }

                    function onDoubleClicked(event, row, column){
                        var ev = event
                        ev["accepted"] = false
                        root.doubleClicked(ev, row, column)
                        if (!ev["accepted"]){
                            if (table.checkForEditable()){
                                editPopup.forceActiveFocus()
                                editPopup.edit()
                            }
                        }
                    }
                }

                function checkForEditable(){
                    let res = false
                    if (!root.editable) {
                        return false
                    }else if (root.editableColumns.length === 0){
                        res = true
                    }else{
                        res = (root.editableColumns.indexOf(table.headerTitle(table.currentColumn)) !== -1)
                    }
                    return res
                }
            }

            TableScrollBar{
                id: verticalScrollBar
                orientation: Qt.Vertical
                stepSize: 50 / table.contentHeight
                pageSize: Math.min((table.height) / table.contentHeight, 1)
                visible: pageSize < 1
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.maximumWidth: 12
                Layout.minimumWidth: 12
                handleColor: scrollbarColor
                backgroundColor: root.backgroundColor
                onPositionChanged: {
                    table.moveToPos(table.viewPos.x, position * (table.contentHeight - table.height))
                }
            }
        }
        TableScrollBar{
            id: horizontalScrollBar
            orientation: Qt.Horizontal
            pageSize: Math.min((table.width - table.style.verticalHeader.width) / table.contentWidth, 1)

            visible: pageSize < 1

            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.maximumHeight: 12
            Layout.minimumHeight: 12
            Layout.rightMargin: 12
            handleColor: scrollbarColor
            backgroundColor: root.backgroundColor
            onPositionChanged: {
                table.moveToPos(position * (table.contentWidth - table.width), table.viewPos.y)
            }
        }
    }
    /*!
        \qmlmethod void LazyTable::coordsToCellPos(Qt.point coords)
        Calculate row and column from coords (items regular coords)
    */
    function coordsToCellPos(coords){
        var cell = new Map
        var r = table.normalToCell(coords)
        cell["column"] = r.x
        cell["row"] = r.y
        return cell
    }
    /*!
        \qmlmethod void LazyTable::coordsToCellPos(QtObjectt provider)
        Append color provider to table (for combining)
    */
    function appendColorProvider(provider){
        table.appendColorProvider(provider)
    }
    /*!
        \qmlmethod void LazyTable::coordsToCellPos(QtObject provider)
        Remove color provider
    */
    function removeColorProvider(provider){
        table.removeColorProvider(provider)
    }
    /*!
        \qmlmethod void LazyTable::setColorProvider(QtObject provider)
        Erase all color provider internal data and set new color provider
    */
    function setColorProvider(provider){
        table.setColorProvider(provider)
    }

    function setCellValue(row, column, value){
        if (table.checkForEditable()) table.setCellValue(row, column, value)
        else console.log("You have no rights to edit cell")
    }

    function increaseRow(){
        table.increaseRow()
    }

    function decreaseRow(){
        table.decreaseRow()
    }

    function increaseColumn(){
        table.increaseColumn()
    }

    function decreaseColumn(){
        table.decreaseColumn()
    }

    function setCurrentRow(row){
        table.setCurrentCell(row, table.currentColumn)
    }

    function setCurrentColumn(column){
        table.setCurrentCell(table.currentRow, column)
    }

    function columnIndexByName(name){
        return table.headerIndex(name)
    }

    function getInternalIndex(index){
        return table.getInternalIndex(index)
    }
}

