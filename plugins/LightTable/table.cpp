#include "table.h"

#include <QSGFlatColorMaterial>
#include <QSGNode>
#include <QDebug>
#include <QSGSimpleTextureNode>
#include <QQuickWindow>
#include <QPainter>
#include <itablecolorprovider.h>
#include <QTimer>
#include <QSGGeometryNode>

#include <scenegraph/shadowedrectanglenode.h>

class HighlightNode : public QSGNode
{
public:
    HighlightNode() {}
    //! childs
    QSGGeometryNode *highlight = nullptr;
    QSGGeometryNode *background = nullptr;

    //! material
    QSGFlatColorMaterial *bgMaterial = nullptr;
    QSGFlatColorMaterial *highlightMaterial = nullptr;
};

/*!
 * \brief The SelectionNode class
 */
class SelectionNode : public QSGNode
{
public:
    SelectionNode() {}
};

class MovingHeaderNode : public QSGGeometryNode
{
public:
    MovingHeaderNode() {}
    QSGSimpleTextureNode *texture = nullptr;
    ShadowedRectangleNode *shadowNode = nullptr;
    int prevColumnIndex = -1;
    QImage img;
};

class PinnedPagesNode: public QSGNode
{
public:
    PinnedPagesNode() {}
};

/*!
 * \brief The MainNode class Class that used only for simple access to GSNODE and all his childs
 */
class MainNode : public QSGNode
{
public:
    MainNode() {};
    //! subchilds
    QSGNode *pages = nullptr;
    QSGGeometryNode *grid = nullptr;
    QSGGeometryNode *selectedCell = nullptr;
    QSGGeometryNode *columnResizer = nullptr;
    QSGNode *verticalHeader = nullptr;
    QSGNode *horizontalHeader = nullptr;
    QSGNode *styleCellBackground = nullptr;
    SelectionNode *colorProviderBackground = nullptr;
    HighlightNode *verticalHeaderHighlight = nullptr;
    HighlightNode *horizontalHeaderHighlight = nullptr;
    MovingHeaderNode *movingHeader = nullptr;
    PinnedPagesNode * pinnedPagesNode = nullptr;

    //! childs
    QSGTransformNode *shiftXTransformNode = nullptr;
    QSGTransformNode *shiftYTransformNode = nullptr;
    QSGTransformNode *shiftXYTransformNode = nullptr;
};

/*!
 * \brief Table::Table
 */
Table::Table(QQuickItem *parent) : QQuickItem(parent)
{
    setFlag(QQuickItem::ItemHasContents, true);
    setFlag(QQuickItem::ItemAcceptsInputMethod, true);
    setFlag(QQuickItem::ItemIsFocusScope, true);
    setAcceptedMouseButtons(Qt::LeftButton | Qt::RightButton | Qt::MiddleButton);
    setClip(true);
    setSmooth(true);
    setAntialiasing(true);
    setAcceptHoverEvents(true);
    forceActiveFocus();

    m_timer = new QTimer(this);
    m_timer->setSingleShot(true);

    connect(m_timer, &QTimer::timeout, this, [this](){
        if (pressed()){
            m_moveInternalPosPress = m_mousePos.x() - m_columns[m_columnIndeces[m_movingColumnIndex]].offset - (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0);
            qDebug() << "moving ON" << m_moveInternalPosPress << m_movingColumnIndex;
            m_moveCurrentXPos = m_mousePos.x() - m_moveInternalPosPress;
            setColumnsMoving(true);
            setCursor(Qt::DragMoveCursor);
            updateReasons.setFlag(UPDATE_HEADER_MOVING);
            updateReasons.setFlag(UPDATE_HEADER);
            update();
        }
    });

    m_style = LazyTableStyle::instance()->getDefaultStyle();

    connect(this, &Table::widthChanged, this, [this](){
        setViewPos(QRectF{m_viewPos.x(), m_viewPos.y(), width() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), m_viewPos.height()});
    });

    connect(this, &Table::heightChanged, this, [this](){
        setViewPos(QRectF{m_viewPos.x(), m_viewPos.y(), m_viewPos.width(), height()});
    });

    updateHeaders();
}

bool Table::editable() const
{
    if (m_provider) {
        return m_provider->isEditable();
    }
    return false;
}

bool Table::interractive() const
{
    return m_interractive;
}

void Table::increaseRow()
{
    if (m_currentRow < (m_rowsCount-1)) {
        setCurrentRow(m_currentRow + 1);
    }
}

void Table::increaseColumn()
{
    if (m_currentColumn < (m_columns.size() - 1)) {
        setCurrentColumn(m_currentColumn + 1);
    }
}

void Table::decreaseRow()
{
    if (m_currentRow > 0) {
        setCurrentRow(m_currentRow - 1);
    }
}

void Table::decreaseColumn()
{
    if (m_currentColumn > 0) {
        setCurrentColumn(m_currentColumn - 1);
    }
}

void Table::moveToPos(double x, double y)
{
    //x side
    if (x < 0 || contentWidth() - viewPos().width() < 0){
        x = 0;
    }else if (x > contentWidth() - viewPos().width()){
        x = contentWidth() - viewPos().width();
    }

    if (y < 0){
        y = 0;
    }else if (y > contentHeight() - viewPos().height()){
        // y = contentHeight() - viewPos().height();
    }
    if (viewPos().x() != x)
        updateReasons.setFlag(SHIFT_X);
    if (viewPos().y() != y)
        updateReasons.setFlag(SHIFT_Y);

    if (viewPos().x() == x && viewPos().y() == y){
        return;
    }
    setViewPos({x, y, m_viewPos.width(), m_viewPos.height()});
    update();
}

void Table::reset()
{
    m_pages.clear();
    setRowsCount(0);
    setPagesCount(0);
    setCurrentColumn(0);
    setCurrentRow(0);
    setReachedBottom(false);
    auto h = m_provider->headerData();
    if (h.size() != m_prevHeaderData.size() && h != m_prevHeaderData){
        setContentWidth(0);
        m_columnIndeces.clear();
        m_columns.clear();
        m_columns.resize(m_provider->columnCount());
        double r = 0;
        for (int  i=0, l = m_provider->columnCount(); i <l; ++i) {
            m_columns[i].width = m_style->horizontalHeader()->width();
            m_columns[i].name = h[i];
            m_columns[i].offset = r;
            m_columnIndeces.insert(i,i);
            r+= m_columns[i].width;
        }
        setContentWidth(r);
        setColumnsCount(m_columns.size());
    }
    if (rowsCount() == 0) {
        setCurrentRow(-1);
    }

    m_prevHeaderData = h;
    updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
    updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
    updateReasons.setFlag(RESIZE_COLUMN);
    moveToPos(0, 0);
    setRowsCount(m_provider->rowCount());
    updatePageVisibility();
    updateAllPages();
    updateHeaders();
}

void Table::updateRows(int startRow, int endRow, ITableDataProvider::Reason r)
{
    Q_UNUSED(r)
    setRowsCount(m_provider->rowCount());
    // поиск страниц, где происходит редактирование
    int startPage = pageIndex(startRow), endPage = pageIndex(endRow);
    for (int i = startPage; i <= endPage; i++){
        // проверка на наличие страницы в кэше
        updatePage(i);
    }

    if (rowsCount() == 0){
        //setCurrentColumn(-1);
        setCurrentRow(-1);
    }

    updateHeaders();
    updatePageVisibility();
}

void Table::updateContentWidth()
{
    double r = 0;
    for (const auto &c : qAsConst(m_columns)){
        r += c.width;
    }
    setContentWidth(r + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0));
}

int Table::rowIndex(double y)
{
    return (y - m_style->horizontalHeader()->height()) / m_style->cellStyle()->height();
}

int Table::pageIndex(int row)
{
    return row / m_pageRowsCount;
}

QSGNode *Table::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    MainNode* node = static_cast<MainNode*>(oldNode);
    if (m_columns.size() == 0) return node;
    if (node == nullptr){
        node = new MainNode();
        node->shiftXTransformNode = new QSGTransformNode();
        node->shiftYTransformNode = new QSGTransformNode();
        node->shiftXYTransformNode = new QSGTransformNode();
        node->appendChildNode(node->shiftXYTransformNode);
        node->appendChildNode(node->shiftYTransformNode);
        node->appendChildNode(node->shiftXTransformNode);

        node->grid =                            drawGrid(nullptr);
        node->pages =                           drawPages(nullptr);
        node->selectedCell =                    drawSelectedCell(nullptr);
        node->columnResizer =                   drawColumnResizer(nullptr);
        node->verticalHeader =                  drawVerticalHeader(nullptr);
        node->horizontalHeader =                drawHorizontalHeader(nullptr);
        node->verticalHeaderHighlight =         drawVerticalHeaderHightLight(nullptr);
        node->horizontalHeaderHighlight =       drawHorizontalHeaderHightLight(nullptr);
        node->styleCellBackground =             drawBackground(nullptr);
        node->colorProviderBackground =         drawColorProviderBackground(nullptr);
        node->movingHeader =                    drawMovingHeader(nullptr);
        node->pinnedPagesNode =                 drawPinnedPages(nullptr);

        node->shiftXTransformNode->appendChildNode(node->horizontalHeader);
        node->shiftXTransformNode->appendChildNode(node->horizontalHeaderHighlight);
        node->shiftXTransformNode->appendChildNode(node->movingHeader);
        node->shiftXTransformNode->appendChildNode(node->columnResizer);
        node->shiftYTransformNode->appendChildNode(node->verticalHeader);
        node->shiftYTransformNode->appendChildNode(node->verticalHeaderHighlight);
        node->shiftXYTransformNode->appendChildNode(node->styleCellBackground);
        node->shiftXYTransformNode->appendChildNode(node->colorProviderBackground);
        node->shiftXYTransformNode->appendChildNode(node->pages);
        node->shiftXYTransformNode->appendChildNode(node->grid);
        node->shiftYTransformNode->appendChildNode(node->pinnedPagesNode);
        node->shiftXYTransformNode->appendChildNode(node->selectedCell);
    }

    if (m_columns.size() == 0) return node;

    if (updateReasons.testFlag(SHIFT_X)) {
        node->shiftXTransformNode->setMatrix(QTransform().translate(-viewPos().x(),0));
        node->shiftXTransformNode->markDirty(QSGNode::DirtyMatrix);
    }
    if (updateReasons.testFlag(SHIFT_Y)) {
        node->shiftYTransformNode->setMatrix(QTransform().translate(0, -viewPos().y()));
        node->shiftYTransformNode->markDirty(QSGNode::DirtyMatrix);
    }
    if (updateReasons.testFlag(SHIFT_X) || updateReasons.testFlag(SHIFT_Y)) {
        node->shiftXYTransformNode->setMatrix(QTransform().translate(-viewPos().x(), -viewPos().y()));
        node->shiftXYTransformNode->markDirty(QSGNode::DirtyMatrix);
    }

    drawVerticalHeaderHightLight(node->verticalHeaderHighlight);
    drawHorizontalHeaderHightLight(node->horizontalHeaderHighlight);
    drawMovingHeader(node->movingHeader);
    drawPinnedPages(node->pinnedPagesNode);
    //
    drawHorizontalHeader(node->horizontalHeader);
    //
    drawColumnResizer(node->columnResizer);
    //
    node->verticalHeader = drawVerticalHeader(node->verticalHeader);

    drawGrid(node->grid);

    drawBackground(node->styleCellBackground);
    node->colorProviderBackground = drawColorProviderBackground(node->colorProviderBackground);
    node->pages = drawPages(node->pages);
    node->selectedCell = drawSelectedCell(node->selectedCell);

    if (updateReasons.testFlag(SELECT_CHANGED)) {
        node->shiftXYTransformNode->markDirty(QSGNode::DirtySubtreeBlocked);
    }

    if (updateReasons.testFlag(RESIZE_COLUMN)) {
        node->shiftXTransformNode->markDirty(QSGNode::DirtySubtreeBlocked);
    }

    node->markDirty(QSGNode::DirtySubtreeBlocked);
    updateReasons = NONE;

    if (m_autoMove != QPoint{0, 0}){
        updateReasons.setFlag(SELECT_CHANGED);
        if (viewPos().y() + viewPos().height() < contentHeight()){
            moveToPos(viewPos().x() + m_autoMove.x(), viewPos().y() + m_autoMove.y());
        }
    }
    return node;
}

PinnedPagesNode *Table::drawPinnedPages(PinnedPagesNode *n)
{
    if (n == nullptr){
        n = new PinnedPagesNode();
    }

    while(n->childCount() != 0){
        delete n->firstChild();
    }

    for (const auto &index : qAsConst(m_pinnedColumns)){
        auto col = m_columns[m_columnIndeces[index]];
        if (col.offset  < viewPos().x() ){
            for (const auto &page : qAsConst(m_pages)){
                if (page.visible){
                    auto textNode = new QSGSimpleTextureNode;
                    textNode->setOwnsTexture(true);
                    auto& img = page.pinnedPages[index];
                    auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
                    textNode->setRect(QRectF((m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), page.yoffset, img.width(), img.height()));
                    textNode->setTexture(t);
                    n->appendChildNode(textNode);
                }
            }
        }else if (col.offset + col.width + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) > viewPos().x() + viewPos().width()){
            for (const auto &page : qAsConst(m_pages)){
                if (page.visible){
                    auto textNode = new QSGSimpleTextureNode;
                    textNode->setOwnsTexture(true);
                    auto& img = page.pinnedPages[index];
                    auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
                    textNode->setRect(QRectF(viewPos().width() - img.width() + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), page.yoffset, img.width(), img.height()));
                    textNode->setTexture(t);
                    n->appendChildNode(textNode);
                }
            }
        }
    }



    n->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);
    return n;
}

void Table::mousePressEvent(QMouseEvent *event)
{
    if (!interractive()) return;
    event->setAccepted(true);
    forceActiveFocus();

    m_mousePos = event->pos();

    if (m_columns.size() == 0) return;
    auto visualX = event->x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    if (event->button() == Qt::LeftButton){
        setPressed(true);
        int currentColumn = 0;
        //! [resizing]
        if(event->y() < m_style->horizontalHeader()->height()){
            for (int i = 0, l = m_columns.size(); i < l; ++i){
                auto x = visualX - m_columns[m_columnIndeces[i]].width;
                double delta = x - m_columns[m_columnIndeces[i]].offset;
                if (qAbs(delta) < 5 ){ // time to resize
                    qDebug() << "resize: " << i;
                    m_resizeColumnIndex = i;
                    m_resizeCurrentXPos = x;
                    setResizing(true);
                    break;
                }else if (m_columns[m_columnIndeces[i]].offset  <= visualX && m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width >= visualX ){
                    currentColumn = i;
                }
            }
            //! last column
            auto ll = m_columns[m_columnIndeces[m_columns.size() - 1]];
            if (qAbs(visualX -  ll.offset - ll.width - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)) < 10){
                m_resizeColumnIndex = m_columns.size() - 1;
                m_resizeCurrentXPos = visualX;
                setResizing(true);
            }
            if (resizing()) return;

            if (m_timer->isActive()){
                m_timer->stop();
            }
            m_movingColumnIndex = currentColumn;

            qDebug() << "Start moving";

            m_timer->setSingleShot(true);
            m_timer->start(500);

            return;
        }
        //! [resizing]

        int index = m_currentColumn + selectedColumnsCount() - (selectedColumnsCount() > 0 ? 1 : 0);
        if (index < 0) index = 0;
        auto p  = internalToNormal(QPointF{m_columns[m_columnIndeces[index]].offset + m_columns[m_columnIndeces[index]].width + 5,
                                           (m_currentRow + selectedRowsCount() - (selectedRowsCount() > 0 ? 1 : 0)) * m_style->cellStyle()->height() + m_style->cellStyle()->height() + 5});
        auto pos = event->pos();
        if (p.x() >= pos.x() && p.x() <= pos.x() + 10
                && p.y() >= pos.y() && p.y() <= pos.y() + 10) {
            m_selecting = true;
            updateReasons.setFlag(SELECT_CHANGED);
            update();
            return;
        }

        //! [selection]
        int row = (event->y() - m_style->horizontalHeader()->height() + viewPos().y()) / m_style->cellStyle()->height();
        if (row >= m_rowsCount) row = m_rowsCount - 1;
        setCurrentRow(row);

        for (int i =0, l = m_columns.size(); i< l; ++i) {
            if (m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width > visualX) {
                setCurrentColumn(i);
                break;
            }
        }
        m_selecting = true;
        //! [selection]
    }else if (event->button() == Qt::RightButton){
        if(event->y() < m_style->horizontalHeader()->height()){
            double iconWidth = m_style->iconsWidth();
            for (int i = 0, l = m_columns.size(); i < l; ++i){
                auto x = visualX - m_columns[m_columnIndeces[i]].width;
                double delta = x - m_columns[m_columnIndeces[i]].offset;
                if (qAbs(delta) < iconWidth + 5){
                    if (delta < 0
                            && event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                            && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2){
                        emit headerPressed(event->button(),i, LazyTableStyle::Button, {m_columns[m_columnIndeces[i]].offset - viewPos().x() + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), 0, m_columns[m_columnIndeces[i]].width, m_style->horizontalHeader()->height()});
                        break;
                    }
                }else if (m_columns[m_columnIndeces[i]].offset  <= visualX && m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width >= visualX ){
                    emit headerPressed(event->button(), i, LazyTableStyle::Free, {m_columns[m_columnIndeces[i]].offset - viewPos().x() + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0),
                                                                                  0,
                                                                                  m_columns[m_columnIndeces[i]].width,
                                                                                  m_style->horizontalHeader()->height()});
                    break;
                }
            }
        }else{
            int row = (event->y() - m_style->horizontalHeader()->height() + viewPos().y()) / m_style->cellStyle()->height();
            if (row >= m_rowsCount) row = m_rowsCount - 1;
            int column = 0;
            for (int i =0, l = m_columns.size(); i< l; ++i) {
                if (m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width > visualX) {
                    column = i;
                    break;
                }
            }

            int startColumn = selectedColumnsCount() > 0 ? m_currentColumn : m_currentColumn + selectedColumnsCount() + 1;
            int endColumn = selectedColumnsCount() < 0 ? m_currentColumn : m_currentColumn + selectedColumnsCount() - 1;
            int startRow = selectedRowsCount() > 0 ? m_currentRow : m_currentRow + selectedRowsCount() + 1;
            int endRow = selectedRowsCount() < 0 ? m_currentRow : m_currentRow + selectedRowsCount() - 1;

            if (column >= startColumn && column <= endColumn){
                if (row >= startRow && row <= endRow){}
                else{
                    setCurrentCell(row, column);
                }
            }else{
                setCurrentCell(row, column);
            }

            emit showContextMenu(event->pos());
        }
    }
}

void Table::mouseMoveEvent(QMouseEvent *event)
{
    if (!interractive()) return;
    auto x = event->x();
    auto y = event->y();
    if (resizing()){
        m_resizeCurrentXPos = x;
        updateReasons.setFlag(RESIZE_COLUMN);
        update();
    }else if (m_selecting){
        checkSelect(x, y);
    }else if (columnsMoving()){
        m_moveCurrentXPos = x - m_moveInternalPosPress;
        if (!checkHeaderSwap()){
            updateReasons.setFlag(UPDATE_HEADER_MOVING);
            update();
        }
    }
}

void Table::mouseReleaseEvent(QMouseEvent *event)
{
    if (!interractive()) return;
    if (m_columns.size() == 0) return;
    if (event->button() == Qt::LeftButton){
        auto visualX = event->x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
        if(event->y() < m_style->horizontalHeader()->height() && !resizing() && !columnsMoving()){
            double iconWidth = m_style->iconsWidth();
            for (int i = 0, l = m_columns.size(); i < l; ++i){
                auto x = visualX - m_columns[m_columnIndeces[i]].width;
                double delta = x - m_columns[m_columnIndeces[i]].offset;
                if (qAbs(delta) < iconWidth + 5){
                    if (delta < 0
                            && event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                            && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2){
                        emit headerPressed(event->button(),i, LazyTableStyle::Button, {m_columns[m_columnIndeces[i]].offset - viewPos().x() + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), 0, m_columns[m_columnIndeces[i]].width, m_style->horizontalHeader()->height()});
                        break;
                    }
                }else if (m_columns[m_columnIndeces[i]].offset  <= visualX && m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width >= visualX ){
                    emit headerPressed(event->button(), i, LazyTableStyle::Free, {m_columns[m_columnIndeces[i]].offset - viewPos().x() + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0),
                                                                                  0,
                                                                                  m_columns[m_columnIndeces[i]].width,
                                                                                  m_style->horizontalHeader()->height()});
                    break;
                }
            }
            setPressed(false);
            return;
        }

        // event->setAccepted(true);
        m_selecting = false;
        if (columnsMoving()){
            updateReasons.setFlag(UPDATE_HEADER_MOVING);
            updateAllPages();
        }
        setColumnsMoving(false);
        m_autoMove = QPoint{0,0};
        auto d = m_resizeCurrentXPos - m_columns[m_columnIndeces[m_resizeColumnIndex]].offset + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
        //фикс от изменения размера к минимальному при случайном нажатии на позицию резайза в хедере
        if (d < 5) {
            setResizing(false);
            updateReasons.setFlag(UPDATE_HEADER);
            update();
            setPressed(false);
            setCursor(Qt::ArrowCursor);
            return;
        }
        if (resizing()) {
            auto&c = m_columns[m_columnIndeces[m_resizeColumnIndex]];
            c.width = d;
            for (int i= m_resizeColumnIndex + 1, l = m_columns.size();i<l;++i){
                m_columns[m_columnIndeces[i]].offset = m_columns[m_columnIndeces[i-1]].offset + m_columns[m_columnIndeces[i-1]].width;
            }
            setResizing(false);
            updateAllPages();
            updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
            updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
            updateReasons.setFlag(UPDATE_HEADER);
            update();
        }
        setCursor(Qt::ArrowCursor);
    }
    setPressed(false);
}

void Table::mouseDoubleClickEvent(QMouseEvent *event)
{
    event->setAccepted(true);
    if (event->button() == Qt::LeftButton){
        if (event->pos().y() > m_style->horizontalHeader()->height()){
            auto visualX = event->x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
            int row = (event->y() - m_style->horizontalHeader()->height() + viewPos().y()) / m_style->cellStyle()->height();
            if (row >= m_rowsCount) row = m_rowsCount - 1;
            setCurrentRow(row);

            for (int i =0, l = m_columns.size(); i< l; ++i) {
                if (m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width > visualX) {
                    setCurrentColumn(i);
                    break;
                }
            }

        }
        QVariantMap eventObject;
        eventObject["key"] = event->button();
        eventObject["modifiers"] = int(event->modifiers());
        emit doubleClicked(eventObject, m_currentRow, m_currentColumn);
    }
}

void Table::keyPressEvent(QKeyEvent *event)
{
    //qDebug() << "key";
    if (event->modifiers() & Qt::ShiftModifier){
        qDebug() << "AAAA";
        bool needUpdate = false;
        if (event->key() == Qt::Key_Up){
            if (m_selectedRowsCount - 1 == 0 && m_currentRow != 0){
                setSelectedRowsCount(-2);
            }else if (m_selectedRowsCount - 1 < -m_currentRow){
                setSelectedRowsCount(-m_currentRow - 1);
            }else{
                setSelectedRowsCount(m_selectedRowsCount - 1);
            }
            needUpdate = true;

        }
        if (event->key() == Qt::Key_Down){
            if (m_selectedRowsCount + 1 == -1 && m_currentRow != m_rowsCount - 1){
                setSelectedRowsCount(1);
            }else if (m_selectedRowsCount + 1 <= m_rowsCount - m_currentRow){
                setSelectedRowsCount(m_selectedRowsCount + 1);
            }
            needUpdate = true;
        }

        if (event->key() == Qt::Key_Left){
            if (m_selectedColumnsCount - 1 == 0 && m_currentColumn != 0){
                setSelectedColumnsCount(-2);
            }else if (m_selectedColumnsCount - 1 < -m_currentColumn){
                setSelectedColumnsCount(-m_currentColumn - 1);
            }else{
                setSelectedColumnsCount(m_selectedColumnsCount - 1);
            }
            needUpdate = true;
        }

        if (event->key() == Qt::Key_Right){
            if (m_selectedColumnsCount + 1 == -1 && m_currentColumn != m_columnsCount - 1){
                setSelectedColumnsCount(1);
            }else if (m_selectedColumnsCount + 1 <= m_columnsCount - m_currentColumn){
                setSelectedColumnsCount(m_selectedColumnsCount + 1);
            }
            needUpdate = true;
        }

        if (needUpdate){
            updateReasons.setFlag(SELECT_CHANGED);
            QPointF newPos = {viewPos().x(), viewPos().y()};
            int index = m_currentColumn + selectedColumnsCount() - (selectedColumnsCount() > 0 ? 1 : -1);
            if (index < 0) index = 0;
            auto x = m_columns[m_columnIndeces[index]].offset + (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0);
            auto y = (m_currentRow + m_selectedRowsCount - (m_selectedRowsCount > 0 ? 1 : -1)) * m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
            auto w1 = m_columns[m_columnIndeces[index]].width;
            auto w2 = viewPos().height() - m_style->horizontalHeader()->height();

            if ((y-w2) > viewPos().y()) {
                newPos.setY(y-w2);
            }else if (y - m_style->horizontalHeader()->height() < viewPos().y()){
                newPos.setY(y - m_style->horizontalHeader()->height());
            }

            if (x > (viewPos().x() + viewPos().width() - w1)) {
                newPos.setX(-(viewPos().width() + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) - x - w1));
            } else if (x < viewPos().x() + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)) {
                newPos.setX(x - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0));
            }

            moveToPos(newPos.x(), newPos.y());
            qDebug() << selectedRowsCount() << x << y;
            event->setAccepted(true);
        }else{
            QVariantMap eventObject;
            eventObject["key"] = event->key();
            eventObject["modifiers"] = int(event->modifiers());
            eventObject["nativeVirtualKey"] = event->nativeVirtualKey();
            eventObject["nativeModifiers"] = event->nativeModifiers();
            emit keyPressed(eventObject, m_currentRow, m_currentColumn);
        }
    }else{
        if (event->key() == Qt::Key_Up) {
            decreaseRow();
        }else if (event->key() == Qt::Key_Down) {
            increaseRow();
        }else if (event->key() == Qt::Key_Left) {
            decreaseColumn();
        }else if (event->key() == Qt::Key_Right) {
            increaseColumn();
        }else{
            QVariantMap eventObject;
            eventObject["key"] = event->key();
            eventObject["modifiers"] = int(event->modifiers());
            eventObject["nativeVirtualKey"] = event->nativeVirtualKey();
            eventObject["nativeModifiers"] = event->nativeModifiers();
            emit keyPressed(eventObject, m_currentRow, m_currentColumn);
        }
    }
}

void Table::hoverMoveEvent(QHoverEvent *event)
{
    event->setAccepted(true);
    if (!resizing()){
        bool res = false;
        if(event->pos().y() < m_style->horizontalHeader()->height() && m_style->horizontalHeader()->height() != 0){
            auto visualX = event->pos().x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
            double iconWidth = m_style->iconsWidth();
            for (int i = 0, l = m_columns.size(); i < l; ++i){
                auto x = visualX - m_columns[m_columnIndeces[i]].width;
                double delta = x - m_columns[m_columnIndeces[i]].offset;
                if (qAbs(delta) < 5 ){ // time to resize
                    m_resizeColumnIndex = i;
                    m_resizeCurrentXPos = x;
                    res = true;
                    break;
                }else if (qAbs(delta) < iconWidth + 5){
                    if (delta < 0
                            && event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                            && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2){
                        setCursor(Qt::PointingHandCursor);
                        return;
                    }
                }
            }
            //! last column
            auto ll = m_columns[m_columnIndeces[m_columns.size() - 1]];
            if (m_columns.size() != 0)
                if (qAbs(visualX -  ll.offset - ll.width - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)) < 10){
                    m_resizeColumnIndex = m_columns.size() - 1;
                    m_resizeCurrentXPos = visualX;
                    res = true;
                }


            if (res){
                setCursor(Qt::SizeHorCursor);
            }else{
                setCursor(Qt::ArrowCursor);
            }
        }else if (m_columns.size() > 0){
            //! selection
            int index = m_currentColumn + selectedColumnsCount() - (selectedColumnsCount() > 0 ? 1 : 0);
            if (index < 0) index = 0;
            auto p = internalToNormal(QPointF{m_columns[index].offset + m_columns[index].width + 5,
                                              (m_currentRow + selectedRowsCount() - (selectedRowsCount() > 0 ? 1 : 0)) * m_style->cellStyle()->height() + m_style->cellStyle()->height() + 5});

            auto pos = event->posF();
            if (p.x() >= pos.x() && p.x() <= pos.x() + 10
                    && p.y() >= pos.y() && p.y() <= pos.y() + 10) {
                setCursor(Qt::PointingHandCursor);
            }else{
                setCursor(Qt::ArrowCursor);
            }
            if (m_selecting)
                checkSelect(event->posF().x(), event->posF().y());
        }
    }
}

void Table::wheelEvent(QWheelEvent *event)
{
    double delta = event->angleDelta().y() / 120.0;
    if (delta > 0){
        if (viewPos().y() - 50 < 0){
            moveToPos(viewPos().x(), 0);
        }else{
            moveToPos(viewPos().x(), viewPos().y() - 50);
        }
        //scrollTop();
    }else{
        if (viewPos().y() + viewPos().height() + 50 > contentHeight() + (reachedBottom() ? 0 : 50)){
            moveToPos(viewPos().x(), contentHeight() - viewPos().height());
        }else{
            moveToPos(viewPos().x(), viewPos().y() + 50);
        }
    }

    //! scale (doesn't work)
    //if (event->modifiers() & Qt::ControlModifier){
    //    double zoomFactor = 0.9;
    //    if (delta > 0)
    //    {
    //        zoomFactor = 1.0/zoomFactor;
    //    }
    //    double z = this->scale() * zoomFactor;
    //    if (z < 3 && z > 0.01)
    //       setScale(z);
    //}

    event->setAccepted(true);
}

QSGGeometryNode *Table::drawGrid(QSGNode *node)
{
    QSGGeometryNode *gridNode;
    if (node == nullptr) {
        gridNode = new QSGGeometryNode;
        gridNode->setFlag(QSGNode::OwnsGeometry);
        // color
        auto m = new QSGFlatColorMaterial();
        m->setColor("gray");
        gridNode->setMaterial(m);
    } else {
        gridNode = static_cast<QSGGeometryNode*>(node);
    }

    int l = 2*(m_columns.size()) + 2*(m_rowsCount+2); //points two for each vertical and horizontal lines
    auto g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), l);
    g->setDrawingMode(QSGGeometry::DrawLines);

    double h = m_style->cellStyle()->height() * (m_rowsCount) + m_style->horizontalHeader()->height();

    //column lines
    int vi =0;
    for (int i =0, l= m_columns.size(); i <l; ++i) {
        auto offset = m_columns[m_columnIndeces[i]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
        g->vertexDataAsPoint2D()[vi++].set(offset, 0);
        g->vertexDataAsPoint2D()[vi++].set(offset, h);
    }
    //last column
    g->vertexDataAsPoint2D()[vi++].set(m_columns[m_columnIndeces[m_columns.size()-1]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_columns[m_columnIndeces[m_columns.size()-1]].width, 0);
    g->vertexDataAsPoint2D()[vi++].set(m_columns[m_columnIndeces[m_columns.size()-1]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_columns[m_columnIndeces[m_columns.size()-1]].width, h);

    //row lines
    auto& a = m_columns[m_columnIndeces[m_columns.size()-1]];
    double w = a.offset + a.width + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    for (int i =0, l= m_rowsCount; i < l; ++i) {
        auto y = m_style->horizontalHeader()->height() + i * m_style->cellStyle()->height();
        g->vertexDataAsPoint2D()[vi++].set(0, y);
        g->vertexDataAsPoint2D()[vi++].set(w, y);
    }
    //last row
    auto y = m_style->horizontalHeader()->height() + m_rowsCount * m_style->cellStyle()->height();
    g->vertexDataAsPoint2D()[vi++].set(0, y);
    g->vertexDataAsPoint2D()[vi++].set(w, y);

    gridNode->setGeometry(g);
    gridNode->markDirty(QSGNode::DirtyGeometry);

    return gridNode;
}

QSGGeometryNode *Table::drawHorizontalHeader(QSGNode *node)
{
    QSGGeometryNode *rectNode;
    QSGGeometry *g;
    if (node == nullptr) {
        rectNode = new QSGGeometryNode;
        // geometry
        g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
        g->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rectNode->setGeometry(g);
        // rect color
        auto m = new QSGFlatColorMaterial();
        m->setColor(QColor(120, 120, 120));
        rectNode->setMaterial(m);
    } else {
        rectNode = static_cast<QSGGeometryNode*>(node);
        g = rectNode->geometry();
    }

    if (updateReasons.testFlag(UPDATE_HEADER)){
        //update rect geom
        auto& a = m_columns[m_columnIndeces[m_columns.size()-1]];
        double w = a.offset + a.width + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
        g->vertexDataAsPoint2D()[0].set(0, 0);
        g->vertexDataAsPoint2D()[1].set(w, 0);
        g->vertexDataAsPoint2D()[2].set(w, m_style->horizontalHeader()->height());
        g->vertexDataAsPoint2D()[3].set(0, m_style->horizontalHeader()->height());
        rectNode->markDirty(QSGNode::DirtyGeometry);
        //header text node
        // setup text node
        QSGSimpleTextureNode *textNode = nullptr;
        if (rectNode->childCount()>0) {
            textNode = static_cast<QSGSimpleTextureNode *>(rectNode->childAtIndex(0));
        } else {
            textNode = new QSGSimpleTextureNode();
            textNode->setOwnsTexture(true);
            rectNode->appendChildNode(textNode);
        }
        //draw image with texts
        QImage img(w, m_style->horizontalHeader()->height(), QImage::Format_ARGB32);

        img.fill(QColor(0,0,0,0));
        QPainter painter(&img);
        painter.setFont(m_style->horizontalHeader()->fontStyle()->font());
        painter.drawLine((m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() - 1 : 0), 0, (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() - 1 : 0), m_style->horizontalHeader()->height());
        for (int i = 0, l = m_columns.size(); i < l; ++i) {

            if (columnsMoving() && i == m_movingColumnIndex) continue;
            QRectF cellRect = {
                m_columns[m_columnIndeces[i]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0),
                0,
                m_columns[m_columnIndeces[i]].width,
                m_style->horizontalHeader()->height()
            };
            //filling bg
            QBrush brush;
            brush.setColor(m_pinnedColumns.contains(i) ? m_style->horizontalHeader()->pinnedColor() : m_style->horizontalHeader()->backgroundColor());
            brush.setStyle(Qt::SolidPattern);
            painter.fillRect(cellRect, brush);
            //drawing text
            cellRect -= QMarginsF{2.5, 2.5 , m_style->iconsWidth() + 6, 0};
            painter.setPen(m_style->horizontalHeader()->textColor());

            painter.drawText(cellRect, m_style->horizontalHeader()->alignment(), m_columns[m_columnIndeces[i]].name);
            //drawing icon
            painter.setPen(m_style->iconsColor());
            QRectF iconRect = QRectF{
                    m_columns[m_columnIndeces[i]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_columns[m_columnIndeces[i]].width - 5 - m_style->iconsWidth(),
                    (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2,
                    m_style->iconsWidth(),
                    m_style->iconsWidth()
        };
        painter.drawRect(iconRect);
        iconRect -= QMarginsF{1,1,1,1};
        painter.drawImage(iconRect, m_style->icon("sort"));

        double x = m_columns[m_columnIndeces[i]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_columns[m_columnIndeces[i]].width;

        painter.setPen("black");
        painter.drawLine(x - 1, 0, x - 1, m_style->horizontalHeader()->height());
    }
    painter.drawLine(0, m_style->cellStyle()->height() - 1, m_contentWidth + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), m_style->cellStyle()->height() - 1);
    auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
    textNode->setRect(QRectF(0,0,w, m_style->horizontalHeader()->height()));
    textNode->setTexture(t);
}
return rectNode;
}

HighlightNode *Table::drawVerticalHeaderHightLight(QSGNode *node)
{
    auto hnode = static_cast<HighlightNode*>(node);

    if (hnode == nullptr){
        hnode = new HighlightNode();
        hnode->highlight = new QSGGeometryNode();
        hnode->background = new QSGGeometryNode();

        hnode->highlight->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        hnode->background->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));

        hnode->bgMaterial = new QSGFlatColorMaterial();
        hnode->highlightMaterial = new QSGFlatColorMaterial();

        hnode->background->setMaterial(hnode->bgMaterial);
        hnode->highlight->setMaterial(hnode->highlightMaterial);

        hnode->appendChildNode(hnode->background);
        hnode->appendChildNode(hnode->highlight);

        hnode->background->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);
        hnode->highlight->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);
    }

    if (updateReasons.testFlag(UPDATE_HEADER_HIGHLIGHT)){
        double y1 = 0.0, y2 = 0.0;
        if (m_currentColumn >= 0 && m_currentRow >= 0){
            if (m_selectedRowsCount > 0){
                y1 = m_currentRow*m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
                y2 = y1 + m_style->cellStyle()->height() * selectedRowsCount();
            }else{
                y2 = m_currentRow * m_style->cellStyle()->height() + m_style->cellStyle()->height() * 2;
                y1 = y2 - qAbs(m_selectedRowsCount) * m_style->cellStyle()->height();
            }
        }

        hnode->highlight->geometry()->vertexDataAsPoint2D()[0].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ), y1);
        hnode->highlight->geometry()->vertexDataAsPoint2D()[1].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ) -3, y1);
        hnode->highlight->geometry()->vertexDataAsPoint2D()[2].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ), y2);
        hnode->highlight->geometry()->vertexDataAsPoint2D()[3].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ) -3, y2);

        hnode->background->geometry()->vertexDataAsPoint2D()[0].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ), y1);
        hnode->background->geometry()->vertexDataAsPoint2D()[1].set(0, y1);
        hnode->background->geometry()->vertexDataAsPoint2D()[2].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ), y2);
        hnode->background->geometry()->vertexDataAsPoint2D()[3].set(0, y2);

        QColor bgColor = m_style->selectionColor();
        bgColor.setAlphaF(m_style->selectionOpacity() * 0.8);

        hnode->bgMaterial->setColor(bgColor);
        hnode->highlightMaterial->setColor(m_style->highlightColor());

        hnode->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);
    }
    return hnode;
}

HighlightNode *Table::drawHorizontalHeaderHightLight(QSGNode *node)
{
    auto hnode = static_cast<HighlightNode*>(node);

    if (hnode == nullptr){
        hnode = new HighlightNode();
        hnode->highlight = new QSGGeometryNode();
        hnode->background = new QSGGeometryNode();

        hnode->highlight->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        hnode->background->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));

        hnode->bgMaterial = new QSGFlatColorMaterial();
        hnode->highlightMaterial = new QSGFlatColorMaterial();

        hnode->appendChildNode(hnode->background);
        hnode->appendChildNode(hnode->highlight);
    }

    if (updateReasons.testFlag(UPDATE_HEADER_HIGHLIGHT)){

        double x1 = 0.0, x2 = 0.0;
        int xIndex1 = m_currentColumn + selectedColumnsCount() - 1;
        int xIndex2 = m_currentColumn + selectedColumnsCount() + 1;
        if (xIndex1 < 0) xIndex1 = 0;
        if (xIndex2 > m_columns.size() - 1) xIndex2 = m_columns.size() - 1;
        if (m_currentColumn >= 0 && m_currentRow >= 0){
            if (m_selectedColumnsCount >= 0){
                x1 = m_columns[m_columnIndeces[m_currentColumn]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
                x2 = m_columns[m_columnIndeces[xIndex1]].offset + m_columns[m_columnIndeces[m_currentColumn + selectedColumnsCount() - 1]].width + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
            }else{
                x1 = m_columns[m_columnIndeces[xIndex2]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
                x2 = m_columns[m_columnIndeces[m_currentColumn]].offset + m_columns[m_columnIndeces[m_currentColumn]].width+ (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
            }
        }

        auto g1 = hnode->highlight->geometry();

        g1->vertexDataAsPoint2D()[0].set(x2, m_style->horizontalHeader()->height() - 3);
        g1->vertexDataAsPoint2D()[1].set(x1, m_style->horizontalHeader()->height() - 3);
        g1->vertexDataAsPoint2D()[2].set(x2, m_style->horizontalHeader()->height() - 1);
        g1->vertexDataAsPoint2D()[3].set(x1, m_style->horizontalHeader()->height() - 1);

        auto g2 = hnode->background->geometry();

        g2->vertexDataAsPoint2D()[0].set(x2, 0);
        g2->vertexDataAsPoint2D()[1].set(x1, 0);
        g2->vertexDataAsPoint2D()[2].set(x2, m_style->horizontalHeader()->height());
        g2->vertexDataAsPoint2D()[3].set(x1, m_style->horizontalHeader()->height());

        hnode->highlight->setGeometry(g1);

        hnode->background->setGeometry(g2);

        QColor bgColor = m_style->selectionColor();
        bgColor.setAlphaF(m_style->selectionOpacity() * 0.8);

        hnode->bgMaterial->setColor(bgColor);
        hnode->highlightMaterial->setColor(m_style->highlightColor());

        hnode->background->setMaterial(hnode->bgMaterial);
        hnode->highlight->setMaterial(hnode->highlightMaterial);

        hnode->background->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);
        hnode->highlight->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);

        hnode->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);
    }
    return hnode;
}

QSGNode *Table::drawVerticalHeader(QSGNode *node)
{
    if (updateReasons.testFlag(UPDATE_HEADER)){
        if (m_style->numericalHeaderAvailable()){
            QSGNode *rectNode = static_cast<QSGNode*>(node);


            if (rectNode == nullptr) {
                if (node != nullptr) delete rectNode;
                rectNode = new QSGNode;
                node = rectNode;
            }

            if (updateReasons.testFlag(UPDATE_PAGE)) {
                while(rectNode->childCount() != 0){
                    delete rectNode->firstChild();
                }
                for (const auto &page : qAsConst(m_pages)){
                    if (page.visible){
                        auto textNode = new QSGSimpleTextureNode;
                        textNode->setOwnsTexture(true);
                        auto& img = page.vertHeader;
                        auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
                        textNode->setRect(QRectF(0,page.yoffset, img.width(), img.height()));
                        textNode->setTexture(t);
                        rectNode->appendChildNode(textNode);
                    }
                }
                node->markDirty(QSGNode::DirtyNodeAdded);
            }

        }else{
            QSGGeometryNode *rectNode = static_cast<QSGGeometryNode*>(node);;
            if (rectNode == nullptr) {
                if (node != nullptr) delete node;
                rectNode = new QSGGeometryNode;
                node = rectNode;
                // geometry
                auto g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
                g->setDrawingMode(QSGGeometry::DrawTriangleFan);
                rectNode->setGeometry(g);
                // rect color
                auto m = new QSGFlatColorMaterial();
                m->setColor(m_style->verticalHeader()->backgroundColor());
                rectNode->setMaterial(m);
                rectNode->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);
            }
            if (m_style->verticalHeaderAvailable()){
                rectNode->geometry()->vertexDataAsPoint2D()[0].set(0, 0);
                rectNode->geometry()->vertexDataAsPoint2D()[1].set(m_style->verticalHeader()->width(), 0);
                rectNode->geometry()->vertexDataAsPoint2D()[2].set(m_style->verticalHeader()->width(), m_rowsCount*m_style->cellStyle()->height() + m_style->horizontalHeader()->height());
                rectNode->geometry()->vertexDataAsPoint2D()[3].set(0, m_rowsCount*m_style->cellStyle()->height() + m_style->horizontalHeader()->height());
            }else{
                rectNode->geometry()->vertexDataAsPoint2D()[0].set(0,0);
                rectNode->geometry()->vertexDataAsPoint2D()[1].set(0,0);
                rectNode->geometry()->vertexDataAsPoint2D()[2].set(0,0);
                rectNode->geometry()->vertexDataAsPoint2D()[3].set(0,0);

            }

            node->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);

        }
    }
    return node;
}

SelectionNode *Table::drawColorProviderBackground(SelectionNode *node)
{
    if (node == nullptr) {
        node = new SelectionNode();
    }

    if (updateReasons.testFlag(UPDATE_COLOR_BACKGROUND)){
        while(node->childCount() != 0){
            delete node->firstChild();
        }

        QMap<int, QVector<QColor>> rowsScheme;
        //create scheme
        for (auto p : qAsConst(m_colorProviders)){
            auto id = p->selectedID();
            //qDebug() << "Drawing bg" <<id;
            for (const auto &i : id){
                int key = m_provider->rowById(i);
                QVector<QColor> v = rowsScheme.value(key, QVector<QColor>());
                v.append(p->selectionColor());
                rowsScheme.insert(key, v);
            }
        }

        for (auto it = rowsScheme.begin(), end = rowsScheme.end(); it != end; it++){
            const QVector<QColor> &colors = it.value();
            //qDebug() << colors;
            for (int i = 0; i < colors.length(); i++){
                QSGGeometryNode *n = new QSGGeometryNode();
                QSGGeometry *g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
                QSGFlatColorMaterial *m = new QSGFlatColorMaterial();

                m->setColor(colors[i]);
                g->setDrawingMode(QSGGeometry::DrawTriangleFan);
                int r = it.key();
                double y1 = r * m_style->cellStyle()->height() + m_style->horizontalHeader()->height() + (double)(i / colors.length()) * m_style->cellStyle()->height();
                double y2 = (r + 1) * m_style->cellStyle()->height() + m_style->horizontalHeader()->height() - (i /(double) colors.length()) * m_style->cellStyle()->height();
                g->vertexDataAsPoint2D()[0].set(m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0, y1);
                g->vertexDataAsPoint2D()[1].set(m_contentWidth, y1);
                g->vertexDataAsPoint2D()[2].set(m_contentWidth, y2);
                g->vertexDataAsPoint2D()[3].set(m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0, y2);

                n->setGeometry(g);
                n->setMaterial(m);
                node->appendChildNode(n);
                n->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);
                //n->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);
            }
        }
        node->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);
    }
    return node;
}

QSGGeometryNode *Table::drawSelectedCell(QSGNode *node)
{
    QSGGeometryNode *selectNode;
    if (node == nullptr) {
        selectNode = new QSGGeometryNode();
        selectNode->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        selectNode->setFlag(QSGNode::OwnsGeometry);
        // color
        auto m = new QSGFlatColorMaterial();
        m->setColor(m_style->highlightColor());
        selectNode->setMaterial(m);
    } else {
        selectNode = static_cast<QSGGeometryNode*>(node);
    }

    auto g = selectNode->geometry();
    if (!g){
        selectNode->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        g = selectNode->geometry();
    }
    g->setLineWidth(2);
    g->setDrawingMode(QSGGeometry::DrawLineLoop);

    //! draw selection
    double x1 = 0.0, y1 = 0.0, x2 = 0.0, y2 = 0.0;
    int xIndex1 = m_currentColumn + selectedColumnsCount() - 1;
    int xIndex2 = m_currentColumn + selectedColumnsCount() + 1;
    if (xIndex1 < 0) xIndex1 = 0;
    if (xIndex2 > m_columns.size() - 1) xIndex2 = m_columns.size() - 1;
    if (m_currentColumn >= 0 && m_currentRow >= 0){
        if (m_selectedColumnsCount >= 0){
            x1 = m_columns[m_columnIndeces[m_currentColumn]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
            x2 = m_columns[m_columnIndeces[xIndex1]].offset + m_columns[m_columnIndeces[m_currentColumn + selectedColumnsCount() - 1]].width + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
        }else{
            x1 = m_columns[m_columnIndeces[xIndex2]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
            x2 = m_columns[m_columnIndeces[m_currentColumn]].offset + m_columns[m_columnIndeces[m_currentColumn]].width+ (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
        }

        if (m_selectedRowsCount > 0){
            y1 = m_currentRow*m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
            y2 = y1 + m_style->cellStyle()->height() * selectedRowsCount();
        }else{
            y2 = m_currentRow * m_style->cellStyle()->height() + m_style->cellStyle()->height() * 2;
            y1 = y2 - qAbs(m_selectedRowsCount) * m_style->cellStyle()->height();
        }
    }
    g->vertexDataAsPoint2D()[0].set(x1, y1);
    g->vertexDataAsPoint2D()[1].set(x2, y1);
    g->vertexDataAsPoint2D()[2].set(x2, y2);
    g->vertexDataAsPoint2D()[3].set(x1, y2);

    //! draw selection background
    //! =========================
    //! |current|               |
    //! =========               |
    //! |       |               |
    //! |       |     huge      |
    //! | small |               |
    //! |       |               |
    //! |       |               |
    //! =========================
    QSGGeometryNode *rect1 = nullptr;
    QSGGeometry *rg1 = nullptr;
    QSGFlatColorMaterial *rm1 = nullptr;

    QSGGeometryNode *rect2 = nullptr;
    QSGGeometry *rg2 = nullptr;
    QSGFlatColorMaterial *rm2 = nullptr;

    if (selectNode->childCount() > 2) {
        rect1 = static_cast<QSGGeometryNode *>(selectNode->childAtIndex(0));
        rm1 = static_cast<QSGFlatColorMaterial *>(rect1->material());
        rg1 = rect1->geometry();

        rect2 = static_cast<QSGGeometryNode *>(selectNode->childAtIndex(1));
        rm2 = static_cast<QSGFlatColorMaterial *>(rect2->material());
        rg2 = rect2->geometry();
    } else {
        rect1 = new QSGGeometryNode();
        rg1 = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
        rg1->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rect1->setGeometry(rg1);
        rect1->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);
        rm1 = new QSGFlatColorMaterial();
        rect1->setMaterial(rm1);
        selectNode->appendChildNode(rect1);

        rect2 = new QSGGeometryNode();
        rg2 = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
        rg2->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rect2->setGeometry(rg2);
        rect2->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);
        rm2 = new QSGFlatColorMaterial();
        rect2->setMaterial(rm2);
        selectNode->appendChildNode(rect2);
    }

    double c_x1 = 0, c_x2 = 0, c_y2 = 0;

    c_x1 = (m_selectedColumnsCount > 0 ? x1 : x2 - m_columns[m_columnIndeces[m_currentColumn]].width);
    c_x2 = (m_selectedColumnsCount > 0 ? x1 + m_columns[m_columnIndeces[m_currentColumn]].width : x2);
    c_y2 = (m_selectedRowsCount > 0 ? m_style->cellStyle()->height() : y2);

    //small rect
    rg1->vertexDataAsPoint2D()[0].set(c_x1, m_selectedRowsCount > 0 ? y1 + m_style->cellStyle()->height() : y1);
    rg1->vertexDataAsPoint2D()[1].set(c_x2, m_selectedRowsCount > 0 ? y1 + m_style->cellStyle()->height() : y1);
    rg1->vertexDataAsPoint2D()[2].set(c_x2, m_selectedRowsCount > 0 ? y2 : c_y2 - m_style->cellStyle()->height());
    rg1->vertexDataAsPoint2D()[3].set(c_x1, m_selectedRowsCount > 0 ? y2 : c_y2 - m_style->cellStyle()->height());
    //huge rect
    rg2->vertexDataAsPoint2D()[0].set(m_selectedColumnsCount > 0 ? c_x2 : x1,   y1);
    rg2->vertexDataAsPoint2D()[1].set(m_selectedColumnsCount > 0 ? x2 : c_x1,   y1);
    rg2->vertexDataAsPoint2D()[2].set(m_selectedColumnsCount > 0 ? x2 : c_x1,   y2);
    rg2->vertexDataAsPoint2D()[3].set(m_selectedColumnsCount > 0 ? c_x2 : x1, y2);

    //! color of selection bg
    QColor c = m_style->selectionColor();
    c.setAlphaF(m_style->selectionOpacity());
    rm1->setColor(c);
    rect1->setGeometry(rg1);

    rm2->setColor(c);
    rect2->setGeometry(rg2);

    //! handle
    QSGGeometryNode *handleNode = nullptr;
    QSGGeometry *g2 = nullptr;
    QSGFlatColorMaterial *m2 = nullptr;
    if (selectNode->childCount() > 2) {
        handleNode = static_cast<QSGGeometryNode *>(selectNode->childAtIndex(2));
        m2 = static_cast<QSGFlatColorMaterial *>(handleNode->material());
        g2 = handleNode->geometry();
    } else {
        handleNode = new QSGGeometryNode();
        g2 = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
        g2->setDrawingMode(QSGGeometry::DrawTriangleFan);
        handleNode->setGeometry(g2);
        handleNode->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);
        m2 = new QSGFlatColorMaterial();
        handleNode->setMaterial(m2);
        selectNode->appendChildNode(handleNode);
    }
    //! handle geometry
    m2->setColor(m_style->highlightColor());
    g2->vertexDataAsPoint2D()[0].set((m_selectedColumnsCount > 0 ? x2 : x1) - 5, (m_selectedRowsCount > 0 ? y2 : y1) - 5);
    g2->vertexDataAsPoint2D()[1].set((m_selectedColumnsCount > 0 ? x2 : x1) - 5, (m_selectedRowsCount > 0 ? y2 : y1) + 5);
    g2->vertexDataAsPoint2D()[2].set((m_selectedColumnsCount > 0 ? x2 : x1) + 5, (m_selectedRowsCount > 0 ? y2 : y1) + 5);
    g2->vertexDataAsPoint2D()[3].set((m_selectedColumnsCount > 0 ? x2 : x1) + 5, (m_selectedRowsCount > 0 ? y2 : y1) - 5);
    handleNode->setGeometry(g2);
    handleNode->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);

    selectNode->setGeometry(g);
    selectNode->markDirty(QSGNode::DirtyGeometry);

    return selectNode;
}

QSGNode *Table::drawPages(QSGNode *n)
{
    if (n == nullptr) {
        n = new QSGNode;
    }

    if (updateReasons.testFlag(UPDATE_PAGE)) {
        while(n->childCount() != 0){
            delete n->firstChild();
        }
        for (const auto &page : qAsConst(m_pages)){
            if (page.visible){
                auto textNode = new QSGSimpleTextureNode;
                textNode->setOwnsTexture(true);
                auto& img = page.image;
                auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
                textNode->setRect(QRectF(0, page.yoffset, img.width(), img.height()));
                textNode->setTexture(t);
                n->appendChildNode(textNode);
            }
        }
    }
    return n;
}

MovingHeaderNode *Table::drawMovingHeader(MovingHeaderNode *n)
{
    if (n == nullptr){
        n = new MovingHeaderNode();
        n->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        n->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);

        auto m = new QSGFlatColorMaterial();
        m->setColor("transparent");
        n->setMaterial(m);
        n->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);

        n->texture = new QSGSimpleTextureNode();
        n->texture->setOwnsTexture(true);
        n->appendChildNode(n->texture);

        n->shadowNode = new ShadowedRectangleNode();
        n->appendChildNode(n->shadowNode);
    }

    if (updateReasons.testFlag(UPDATE_HEADER_MOVING)){
        if (columnsMoving()){
            auto col = m_columns[m_columnIndeces[m_movingColumnIndex]];
            //draw image with texts
            if (n->prevColumnIndex != m_movingColumnIndex || n->img.width() != col.width){
                QImage img(col.width, m_style->horizontalHeader()->height(), QImage::Format_ARGB32);
                img.fill(QColor(0,0,0,0));

                QPainter painter(&img);
                painter.setFont(m_style->horizontalHeader()->fontStyle()->font());

                QRectF cellRect = {
                    0,
                    0,
                    col.width,
                    m_style->horizontalHeader()->height()
                };
                //filling bg
                QBrush brush;
                brush.setColor(m_style->horizontalHeader()->backgroundColor());
                brush.setStyle(Qt::SolidPattern);
                painter.fillRect(cellRect, brush);
                //drawing text
                cellRect -= QMarginsF{2.5, 2.5 , m_style->iconsWidth() + 6, 0};
                painter.setPen(m_style->horizontalHeader()->textColor());

                painter.drawText(cellRect, m_style->horizontalHeader()->alignment(), col.name);
                //drawing icon
                painter.setPen(m_style->iconsColor());
                QRectF iconRect = QRectF{
                        col.width - 5 - m_style->iconsWidth(),
                        (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2,
                        m_style->iconsWidth(),
                        m_style->iconsWidth()
            };
                painter.drawRect(iconRect);
                iconRect -= QMarginsF{1,1,1,1};
                painter.drawImage(iconRect, m_style->icon("sort"));

                double x = (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + col.width;

                painter.setPen("black");
                painter.drawLine(x - 1, 0, x - 1, m_style->horizontalHeader()->height());

                painter.drawLine(0, m_style->cellStyle()->height() - 1, m_contentWidth + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), m_style->cellStyle()->height() - 1);

                n->img = img;
            }
            auto t = window()->createTextureFromImage(n->img, QQuickWindow::TextureHasAlphaChannel);
            n->texture->setTexture(t);
            n->texture->setRect(QRectF(m_moveCurrentXPos, 0, col.width, m_style->horizontalHeader()->height()));

            n->prevColumnIndex = m_movingColumnIndex;

            n->shadowNode->setBorderEnabled(true);
            //auto col = m_columns[m_columnIndeces[m_movingColumnIndex]];
            n->shadowNode->setRect({m_moveCurrentXPos, 0, col.width, m_style->horizontalHeader()->height()});
            n->shadowNode->setRadius(QVector4D(0, 0, 0, 0));
            n->shadowNode->setOffset(QVector2D{2, 2});
            n->shadowNode->setColor("transparent");
            n->shadowNode->setShadowColor("black");
            n->shadowNode->setSize(4);
            n->shadowNode->setBorderWidth(0.5);
            n->shadowNode->setBorderColor("black");

            n->shadowNode->updateGeometry();
        }else{
            n->shadowNode->setBorderEnabled(true);
            n->shadowNode->setRect({0,0,1,1});
            n->shadowNode->setSize(0);
            n->shadowNode->setRadius(QVector4D(0, 0, 0, 0));
            n->shadowNode->setOffset(QVector2D{0, 0});
            n->shadowNode->setColor("transparent");
            n->shadowNode->setShadowColor("transparent");
            n->shadowNode->setBorderWidth(1);
            n->shadowNode->setBorderColor("transparent");
            n->shadowNode->updateGeometry();

            QImage img(1, 1, QImage::Format_ARGB32);
            n->texture->setRect(QRectF(0,0,0,0));
            n->texture->setTexture(window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel));
        }
        n->markDirty(QSGNode::DirtyGeometry);
    }
    return n;
}

QSGNode *Table::drawBackground(QSGNode *n)
{
    if (n == nullptr) {
        n = new QSGNode;
    }

    if (updateReasons.testFlag(UPDATE_PAGE)) {
        while(n->childCount() != 0){
            delete n->firstChild();
        }
        for (const auto &page : qAsConst(m_pages)){
            if (page.visible){
                auto textNode = new QSGSimpleTextureNode;
                textNode->setOwnsTexture(true);
                auto& img = page.background;
                auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
                textNode->setRect(QRectF(0,page.yoffset, img.width(), img.height()));
                textNode->setTexture(t);
                n->appendChildNode(textNode);
            }
        }
    }
    return n;
}

QSGGeometryNode *Table::drawColumnResizer(QSGNode *n)
{
    QSGGeometryNode *resizerNode;
    if (n == nullptr) {
        resizerNode = new QSGGeometryNode;
        resizerNode->setFlag(QSGNode::OwnsGeometry);
        // color
        auto m = new QSGFlatColorMaterial();
        m->setColor(m_style->highlightColor());
        resizerNode->setMaterial(m);
    } else {
        resizerNode = static_cast<QSGGeometryNode*>(n);
    }

    auto g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), resizing() ? 2 : 0);
    g->setDrawingMode(QSGGeometry::DrawLines);

    if (resizing()) {
        double h = m_style->cellStyle()->height() * m_rowsCount + m_style->horizontalHeader()->height();
        g->vertexDataAsPoint2D()[0].set(m_resizeCurrentXPos + viewPos().x(), 0);
        g->vertexDataAsPoint2D()[1].set(m_resizeCurrentXPos + viewPos().x(), h);
    }

    resizerNode->setGeometry(g);
    resizerNode->markDirty(QSGNode::DirtyGeometry);

    return resizerNode;
}

QPair<QImage, QImage> Table::drawPageImage(const QVector<Row> &data)
{
    if (m_columns.size() == 0) return {};
    auto& a = m_columns[m_columnIndeces[m_columns.size()-1]];
    double w = a.offset + a.width + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    QImage img(w, m_style->cellStyle()->height() * m_pageRowsCount, QImage::Format_ARGB32);
    QImage bgimg(w, m_style->cellStyle()->height() * m_pageRowsCount, QImage::Format_ARGB32);
    img.fill(QColor(0,0,0,0));
    bgimg.fill(QColor(0,0,0,0));

    QPainter painter(&img);
    QPainter bgPainter(&bgimg);
    for (int i = 0; i < data.size(); ++i) {
        const auto &row = data[i];
        auto yoffset = i * m_style->cellStyle()->height();
        //const QFontMetrics fontMetrics = painter.fontMetrics();
        for (int j = 0; j < row.cells.size(); j++) {
            const auto &cell = row.cells[m_columnIndeces[j]];
            QRectF cellRect = {
                m_columns[m_columnIndeces[j]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0),
                yoffset ,
                m_columns[m_columnIndeces[j]].width,
                m_style->cellStyle()->height()
            };

            QRectF cellMarginedRect = {
                m_columns[m_columnIndeces[j]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_style->cellStyle()->leftMargin(),
                yoffset,
                m_columns[m_columnIndeces[j]].width - m_style->cellStyle()->rightMargin() - m_style->cellStyle()->leftMargin(),
                m_style->cellStyle()->height()
            };
            QBrush brush;
            brush.setColor(cell.flags.testFlag(Cell::OWN_BGCOLOR) ? cell.backgroundColor : m_style->cellStyle()->backgroundColor());
            brush.setStyle(Qt::SolidPattern);
            bgPainter.fillRect(cellRect, brush);
            painter.setFont(m_style->cellStyle()->fontStyle()->font());
            painter.setPen(cell.flags.testFlag(Cell::OWN_TXTCOLOR) ? cell.textColor : m_style->cellStyle()->textColor());
            QString text = cell.displayText;//fontMetrics.elidedText(cell.displayText, static_cast<Qt::TextElideMode>(m_style->cellStyle()->elide()), qRound(0.95 * (m_columns[m_columnIndeces[j]].width - m_style->cellStyle()->rightMargin() - m_style->cellStyle()->leftMargin())));
            painter.drawText(cellMarginedRect, m_style->cellStyle()->alignment(), text);
        }
    }
    QPen pen;
    pen.setColor("red");
    pen.setWidth(4);
    painter.setPen(pen);
    //painter.drawRect(0, 0, w, m_style->cellStyle()->height() * data.size());
    return {img, bgimg};
}

QImage Table::drawPinnedImage(int index, const QVector<Row> &data)
{
    if (m_columns.size() == 0) return {};
    if (!m_pinnedColumns.contains(index)) return {};
    auto& a = m_columns[m_columnIndeces[index]];
    double w = a.width;
    QImage img(w, m_style->cellStyle()->height() * m_pageRowsCount, QImage::Format_ARGB32);
    img.fill(QColor(0,0,0,0));

    QPainter painter(&img);
    for (int i = 0; i < data.size(); ++i) {
        const auto &row = data[i];
        auto yoffset = i * m_style->cellStyle()->height();
        const auto &cell = row.cells[m_columnIndeces[index]];
        QRectF cellRect = {
            0,
            yoffset ,
            m_columns[m_columnIndeces[index]].width,
            m_style->cellStyle()->height()
        };

        QRectF cellMarginedRect = {
            static_cast<double>(m_style->cellStyle()->leftMargin()),
            yoffset,
            m_columns[m_columnIndeces[index]].width - m_style->cellStyle()->rightMargin() - m_style->cellStyle()->leftMargin(),
            m_style->cellStyle()->height()
        };

        QColor cc = "transparent";
        if (m_colorProviders.length() > 0){
            if (m_colorProviders.at(0)->hasSelection(row)){
                cc = m_colorProviders.at(0)->selectionColor();
            }
        }

        if (cc == "transparent"){
            if (cell.flags.testFlag(Cell::OWN_BGCOLOR)){
                cc = cell.backgroundColor;
            }
            else{
                cc = m_style->cellStyle()->backgroundColor();
            }
        }
        QBrush brush;
        brush.setColor(cc);
        brush.setStyle(Qt::SolidPattern);
        painter.fillRect(cellRect, brush);
        painter.setFont(m_style->cellStyle()->fontStyle()->font());
        painter.setPen(cell.flags.testFlag(Cell::OWN_TXTCOLOR) ? cell.textColor : m_style->cellStyle()->textColor());
        QString text = cell.displayText;
        painter.drawText(cellMarginedRect, m_style->cellStyle()->alignment(), text);
        painter.drawLine(0, yoffset + cellRect.height() - 1, cellRect.width(), yoffset + cellRect.height() - 1);
    }
    painter.drawLine(img.width() - 1, 0, img.width() - 1,img.height());
    painter.drawLine(0, 0, 0, img.height());
    return img;
}

QImage Table::drawPageCounter(int from, int count)
{
    if (m_style->numericalHeaderAvailable()){
        const QFontMetrics fontMetrics(m_style->verticalHeader()->fontStyle()->font());
        double w = fontMetrics.horizontalAdvance(QString::number(from + count + 1)) + 5;
        QImage img(w, m_style->cellStyle()->height() * count, QImage::Format_ARGB32);
        QPainter painter(&img);
        img.fill(m_style->verticalHeader()->backgroundColor());
        for (int i = 0; i < count; i++){
            auto yoffset = i * m_style->cellStyle()->height();
            //painter.setPen(m_currentRow == from + i ? m_style->highlightColor(): m_style->verticalHeader()->textColor());
            painter.setPen(m_style->verticalHeader()->textColor());
            painter.setFont(m_style->verticalHeader()->fontStyle()->font());
            QRectF cellRect = {
                0,
                yoffset ,
                w - 2,
                m_style->cellStyle()->height()
            };
            painter.drawText(cellRect, Qt::AlignHCenter | Qt::AlignVCenter, QString::number(from + i + 1));
            painter.setPen("black");
            painter.drawLine(0, yoffset + m_style->cellStyle()->height() - 1, w, yoffset + m_style->cellStyle()->height() - 1);
        }
        return img;
    }
    return {};
}

QPointF Table::internalToNormal(QPointF pos)
{
    QPointF result;
    result.setX(pos.x() - viewPos().x() + (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ));
    result.setY(pos.y() - viewPos().y() + m_style->horizontalHeader()->height());
    return result;
}

void Table::checkSelect(double x, double y)
{
    if (viewPos().width() - 20 < x) {
        m_autoMove.setX(qAbs(x - viewPos().width() + 20) / 10);
    }else if (x < 20){
        m_autoMove.setX(-qAbs(viewPos().width() - x  - 20) / 15);
    }else {
        m_autoMove.setX(0);
    }

    if (viewPos().height() - 20 < y) {
        m_autoMove.setY(qAbs(y - viewPos().height() + 20) / 5);
    }else if (y < 20){
        m_autoMove.setY(-qAbs(viewPos().height() - y  - 20) / 15);
    }else {
        m_autoMove.setY(0);
    }
    if (m_autoMove != QPoint{0,0}){
        updateReasons.setFlag(SELECT_CHANGED);
        qDebug() << m_autoMove;
        update();
    }

    bool needBreakX = false, needBreakY = false;

    for (int i = 0; i < m_columns.size(); i++){
        auto col = m_columns[m_columnIndeces[i]];
        auto topPos = internalToNormal(QPointF {col.offset + col.width,
                                                (m_currentRow + selectedRowsCount() - (selectedRowsCount() > 0 ? 1 : 0)) * m_style->cellStyle()->height() + m_style->cellStyle()->height() + m_style->horizontalHeader()->height()});

        if (topPos.x() - col.width / 2 >= x && topPos.x() >= x){
            if (i - m_currentColumn > 0){
                setSelectedColumnsCount(i - m_currentColumn);
            }else if (i - m_currentColumn < 0){
                setSelectedColumnsCount(i - m_currentColumn - 1);
            }else setSelectedColumnsCount(1);

            needBreakX = true;
        }
        if (m_columns.size() - 1 == i && !needBreakX ){
            setSelectedColumnsCount(i - m_currentColumn + 1);
            needBreakX = true;
        }
        double res = (viewPos().y() + y - m_currentRow * m_style->cellStyle()->height() - m_style->horizontalHeader()->headerType() - (m_selectedRowsCount < 0 ? m_style->cellStyle()->height() * 1.5: m_style->cellStyle()->height() / 2)) / m_style->cellStyle()->height();

        int rowsCount = res;
        if (rowsCount > 0){
            if (m_currentRow + rowsCount - 1 < m_rowsCount)
                setSelectedRowsCount(rowsCount);
            needBreakY = true;
        }else if (rowsCount < 0){
            if (m_currentRow >= qAbs(rowsCount)){
                setSelectedRowsCount(rowsCount - 1);
            }else{
                setSelectedRowsCount(-m_currentRow - 1);
            }
            needBreakY = true;
        }else{
            if (res > 0){
                setSelectedRowsCount(1);
            }else{
                setSelectedRowsCount(-2);
            }

            needBreakY = true;
        }

        if (needBreakY && needBreakX) {
            updateReasons.setFlag(SELECT_CHANGED);
            update();
            return;
        }
    }
    if (needBreakY || needBreakX) {
        updateReasons.setFlag(SELECT_CHANGED);
        update();
    }
}

bool Table::checkHeaderSwap()
{
    auto currcol = m_columns[m_columnIndeces[m_movingColumnIndex]];
    double lx = m_moveCurrentXPos, rx = lx + currcol.width;
    if (viewPos().width() - 20 < rx) {
        m_autoMove.setX(qAbs(rx - viewPos().width() + 20) / 10);
    }else if (lx < 20){
        m_autoMove.setX(-qAbs(viewPos().width() - lx  - 20) / 15);
    }else {
        m_autoMove.setX(0);
    }
    for (int i = 0; i < m_columns.size(); i++){
        auto col = m_columns[m_columnIndeces[i]];

        if (i < m_movingColumnIndex){
            if (lx < col.offset + col.width / 2){
                swapColumns(i, m_movingColumnIndex);
                m_movingColumnIndex = i;
                updateReasons.setFlag(UPDATE_HEADER);
                updateAllPages();
                return true;
            }
        }else if (i > m_movingColumnIndex){
            if (rx > col.offset + col.width / 2){
                swapColumns(i, m_movingColumnIndex);
                m_movingColumnIndex = i;
                updateReasons.setFlag(UPDATE_HEADER);
                updateAllPages();
                return true;
            }
        }
    }
    return false;
}

void Table::swapColumns(int index1, int index2)
{
    if (index1 > -1 && index1 < m_columns.size() && index2 > -1 && index2 < m_columns.size()){
        auto& col1 = m_columns[m_columnIndeces[index1]];
        auto& col2 = m_columns[m_columnIndeces[index2]];

        if (col1.offset > col2.offset){
            col1.offset = col2.offset;
            col2.offset = col1.offset + col1.width;
        }else{
            col2.offset = col1.offset;
            col1.offset = col2.offset + col2.width;
        }

        int index11 = m_columnIndeces[index1];
        m_columnIndeces[index1] = m_columnIndeces[index2];
        m_columnIndeces[index2] = index11;

        ////bug fix
        for (int i= 1, l = m_columns.size();i<l;++i){
            m_columns[m_columnIndeces[i]].offset = m_columns[m_columnIndeces[i-1]].offset + m_columns[m_columnIndeces[i-1]].width;
        }
    }
}

void Table::updatePage(int ind)
{
    if (!m_pages.contains(ind)) return;
    auto& p = m_pages[ind];
    auto data = loadPageData(ind * m_pageRowsCount);
    auto pair = drawPageImage(data);
    p.image = pair.first;
    p.background = pair.second;
    p.vertHeader = drawPageCounter(ind * m_pageRowsCount, m_pageRowsCount);
    p.rowsCount = data.size();
    p.pinnedPages.clear();
    for (const int &i : m_pinnedColumns){
        p.pinnedPages.insert(i, drawPinnedImage(i, data));
    }
    updateReasons.setFlag(UPDATE_PAGE);
    updateReasons.setFlag(UPDATE_HEADER);
    update();
}

void Table::updateAllPages()
{
    for (auto it = m_pages.begin(); it != m_pages.end(); it++) {
        auto& p = it.value();
        auto data = loadPageData(it.value().firstRow);
        p.yoffset = p.firstRow * m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
        auto pair = drawPageImage(data);
        p.pinnedPages.clear();
        for (const int &i : m_pinnedColumns){
            p.pinnedPages.insert(i, drawPinnedImage(i, data));
        }
        p.image = pair.first;
        p.background = pair.second;
        p.rowsCount = data.size();
        p.vertHeader = drawPageCounter(p.firstRow * m_style->cellStyle()->height() ,data.size());
    }
    updateReasons.setFlag(UPDATE_PAGE);
    updateReasons.setFlag(UPDATE_HEADER);
    update();
}

void Table::updateHeaders()
{
    updateReasons.setFlag(UPDATE_HEADER);
    update();
}

void Table::addPageByRow(int row)
{
    QVector<Row> pageRows = loadPageData(row);

    if (!pageRows.isEmpty()){
        int index = pageIndex(row);
        //! new (or start) row index of page
        if (!m_pages.contains(index)){
            int nrow = index * m_pageRowsCount;
            PageData pd;
            pd.firstRow = nrow;
            auto pair = drawPageImage(pageRows);
            pd.image = pair.first;
            pd.background = pair.second;
            pd.pinnedPages.clear();
            for (const int &i : m_pinnedColumns){
                pd.pinnedPages.insert(i, drawPinnedImage(i, pageRows));
            }
            pd.vertHeader = drawPageCounter(nrow, m_pageRowsCount);
            pd.yoffset = index * m_style->cellStyle()->height() * m_pageRowsCount + m_style->horizontalHeader()->height();
            pd.rowsCount = pageRows.length();
            pd.visible = true;
            m_pages.insert(index, pd);
            setPagesCount(m_pages.count());
            updateReasons.setFlag(UPDATE_PAGE);
            update();
        }
    }
}

void Table::updatePageVisibility()
{
    //! можно оптимизировать
    int startRow = rowIndex(viewPos().y());
    int endRow = rowIndex(viewPos().y() + viewPos().height());
    int startPageIndex = pageIndex(startRow);
    int endPageIndex = pageIndex(endRow);

    if (startPageIndex > 0 && (startRow - startPageIndex * m_pageRowsCount) < 10) {
        startPageIndex -= 1;
    }

    auto pages = m_pages.keys();
    bool needUpd = false;

    for (int  i=startPageIndex; i <=endPageIndex; ++i) {
        pages.removeAll(i);
        if (!m_pages.contains(i)) {
            addPageByRow(i*m_pageRowsCount);
            needUpd = true;
        }
    }

    for (auto p: pages) {
        m_pages.remove(p);
    }

    if (m_style->numericalHeaderAvailable()){
        //m_style->verticalHeader()->m_width = m_pages[endPageIndex].vertHeader.width();
        //emit m_style->verticalHeader()->widthChanged();
        //updateReasons.setFlag(UPDATE_HEADER);
        //updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
        //update();
    }

    if (needUpd) {
        updateReasons.setFlag(UPDATE_PAGE);
        update();
    }
    return;

}

QVector<Row> Table::loadPageData(int row)
{
    if (m_provider == nullptr) return {};

    int index = pageIndex(row);
    //! new (or start) row index of page
    int nrow = index * m_pageRowsCount;

    QVector<Row> pageRows;
    for (int i = 0; i < m_pageRowsCount; i++){
        auto row = m_provider->getRow(nrow + i);
        if (row.id != -1){
            pageRows.push_back(row);
        }else{
            break;
        }
    }
    return pageRows;

}

void Table::setStyle(TableStyle *newStyle)
{
    if (m_style == newStyle && newStyle == nullptr)
        return;
    //qDebug() << "Style changed";
    if (m_style != nullptr){
        if (m_style->parent() == this){
            m_style->deleteLater();
        }else{
            disconnect(m_style, nullptr, this, nullptr);
        }
    }

    m_style = newStyle;

    connect(m_style, &TableStyle::updateAllPages, this, &Table::updateAllPages);
    connect(m_style, &TableStyle::updateHeaders, this, &Table::updateHeaders);

    double r = 0;
    if (!provider()) return;
    for (int  i=0, l= m_provider->columnCount(); i <l; ++i) {
        m_columns[m_columnIndeces[i]].width = m_style->horizontalHeader()->width();
        m_columns[m_columnIndeces[i]].offset = r;
        r+= m_columns[m_columnIndeces[i]].width;
    }
    setContentWidth(r);

    updateAllPages();
    updatePageVisibility();
    updateHeaders();
    emit styleChanged(m_style);
}

void Table::setCurrentRow(int currentRow)
{
    if (m_currentRow == currentRow)
        return;

    m_currentRow = currentRow;
    setSelectedColumnsCount(1);
    setSelectedRowsCount(1);
    auto y = (m_currentRow == m_rowsCount ? m_currentRow - 1 : m_currentRow) * m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
    auto w = viewPos().height() - m_style->horizontalHeader()->height();
    updateReasons.setFlag(SELECT_CHANGED);
    updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
    if ((y-w) > viewPos().y()) {
        moveToPos(m_viewPos.x() , y-w);
    }else if (y - m_style->horizontalHeader()->height() < viewPos().y()){
        moveToPos(m_viewPos.x() , y - m_style->horizontalHeader()->height());
    }
    //qDebug() << "|----------------------------------------|" << "\nContent height" << contentHeight() << "\nPages count" << m_pagesCount << "\nRows count" << rowCount() << "\nReached bottom" << reachedBottom();
    update();

    emit currentRowChanged(m_currentRow);
}

void Table::setCurrentColumn(int currentColumn)
{
    if (m_currentColumn == currentColumn)
        return;

    m_currentColumn = currentColumn;
    setSelectedColumnsCount(1);
    setSelectedRowsCount(1);
    auto x = m_columns[m_currentColumn].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    auto w = m_columns[m_currentColumn].width;
    updateReasons.setFlag(SELECT_CHANGED);
    updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
    if (x > (viewPos().x() + viewPos().width() - w)) {
        moveToPos(-(viewPos().width() + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) - x - w), m_viewPos.y());
    } else if (x < viewPos().x() + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)) {
        moveToPos(x - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) , m_viewPos.y());
    }
    update();

    emit currentColumnChanged(m_currentColumn);
}

void Table::setSelectedColumnsCount(int selectedColumnsCount)
{
    if (m_selectedColumnsCount == selectedColumnsCount)
        return;

    updateReasons.setFlag(SELECT_CHANGED);
    updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
    m_selectedColumnsCount = selectedColumnsCount;
    emit selectedColumnsCountChanged(m_selectedColumnsCount);
    update();
}

void Table::setSelectedRowsCount(int selectedRowsCount)
{
    if (m_selectedRowsCount == selectedRowsCount)
        return;

    updateReasons.setFlag(SELECT_CHANGED);
    updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
    m_selectedRowsCount = selectedRowsCount;
    emit selectedRowsCountChanged(m_selectedRowsCount);
    update();
}

void Table::setProvider(QObject *provider)
{
    if ((QObject*)m_provider == provider)
        return;
    m_provider = qobject_cast<ITableDataProvider*>(provider);
    m_columns.resize(m_provider->columnCount());
    auto h = m_provider->headerData();
    if (m_provider != nullptr) {
        disconnect(m_provider, nullptr, this, nullptr);
    }

    m_provider = (ITableDataProvider*)provider;
    connect(m_provider, &ITableDataProvider::reseted, this, &Table::reset);
    connect(m_provider, &ITableDataProvider::rowsUpdated, this, &Table::updateRows);
    connect(m_provider, &ITableDataProvider::fullLoaded, this, [this](){
        setRowsCount(m_provider->rowCount());
        setReachedBottom(true);
    });

    double r = 0;
    for (int  i=0, l = m_provider->columnCount(); i <l; ++i) {
        m_columns[m_columnIndeces[i]].name = h[i];
        r += m_columns[m_columnIndeces[i]].width;
    }
    setContentWidth(r);

    //addPage(0, m_pageRowsCount);
    //addPage(40, m_pageRowsCount);
    //setContentHeight(80 * m_cellHeight);
    reset();
    //! [fix]
    setRowsCount(m_provider->rowCount());
    setColumnsCount(m_provider->columnCount());
    //! [fix]
    emit editableChanged(m_provider->isEditable());
    emit providerChanged((QObject*)m_provider);
}

void Table::setColorProvider(QObject *provider)
{
    ITableColorProvider * pprovider = qobject_cast<ITableColorProvider*>(provider);
    if (!m_colorProviders.contains(pprovider) && pprovider){

        for (int i = m_colorProviders.size() - 1; i >= 0; i--){
            removeColorProvider(m_colorProviders[i]);
        }

        m_colorProviders.append(pprovider);

        connect(pprovider, &ITableColorProvider::schemeChanged, this, [this](){
            updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
            update();
        });

        connect(provider, &ITableColorProvider::destroyed, this, [this, pprovider](){
            m_colorProviders.removeAll(pprovider);
            updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
            update();
        });

        updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
        update();
    }
}

void Table::appendColorProvider(QObject *provider)
{
    ITableColorProvider * pprovider = qobject_cast<ITableColorProvider*>(provider);
    if (!m_colorProviders.contains(pprovider) && pprovider){
        m_colorProviders.append(pprovider);

        connect(pprovider, &ITableColorProvider::schemeChanged, this, [this](){
            updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
            update();
        });

        connect(provider, &ITableColorProvider::destroyed, this, [this, pprovider](){
            m_colorProviders.removeAll(pprovider);
            updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
            update();
        });

        updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
        update();
    }
}

void Table::removeColorProvider(QObject *provider)
{
    ITableColorProvider * pprovider = qobject_cast<ITableColorProvider*>(provider);
    if (m_colorProviders.contains(pprovider) && pprovider){
        disconnect(pprovider, nullptr, this, nullptr);
        m_colorProviders.removeAll(pprovider);
        updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
        update();
    }
}

void Table::resetSelection()
{
    setSelectedColumnsCount(1);
    setSelectedRowsCount(1);
}

QRectF Table::cellPos(int row, int column)
{
    QRectF result {0,0,0,0};
    if (column < m_columns.size() && column >= 0 &&
            row < m_rowsCount && row >= 0){
        result.setX(m_columns[column].offset + (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0));
        result.setY(row * m_style->cellStyle()->height() + m_style->horizontalHeader()->height());
        result.setWidth(m_columns[column].width);
        result.setHeight(m_style->cellStyle()->height());
    }
    return result;
}

QColor Table::cellBackgroundColor(int row, int column)
{
    if (m_provider){
        auto r = m_provider->getRow(row);
        if (m_colorProviders.length() != 0){
            for (const auto &p : qAsConst(m_colorProviders)){
                if (p->hasSelection(r.id)){
                    return p->selectionColor();
                }
            }
        }

        if (r.cells.size() > column && column >= 0){
            auto c = r.cells[column];
            if (c.flags.testFlag(Cell::OWN_BGCOLOR)){
                return c.backgroundColor;
            }
        }
    }
    return m_style->cellStyle()->backgroundColor();
}

QString Table::cellValue(int row, int column)
{
    if (column < m_columns.size() && column >= 0 &&
            row < m_rowsCount && row >= 0){
        return m_provider->getRow(row).cells[column].displayText;
    }
    return "";
}

void Table::setCellValue(int row, int column, QVariant value)
{
    if (m_provider) m_provider->setCellData(row, column, value);
}

QString Table::headerTitle(int column)
{
    if (m_columnsCount > column && column >= 0)
        return m_columns[column].name;
    return "";
}

void Table::setCurrentCell(int row, int column)
{
    if (row >=0 && row < rowsCount())
        setCurrentRow(row);
    if (column >= 0 && column < columnsCount())
        setCurrentColumn(column);
}

int Table::headerIndex(QString name)
{
    for (int i = 0; i < m_columns.size(); i++){
        if (m_columns[m_columnIndeces[i]].name == name) return i;
    }
    return -1;
}

void Table::pinColumn(int column)
{
    if (m_pinnedColumns.contains(column)) return;
    //m_pinnedColumns.
}

void Table::unpinColumn(int column)
{

}

QPoint Table::normalToCell(QPointF pos)
{
    QPoint res{-1, -1};
    auto visualX = pos.x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    //! [resizing]
    if(pos.y() > m_style->horizontalHeader()->height()){
        for (int i = 0, l = m_columns.size(); i < l; ++i){
            if (m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width >= visualX){
                res.setX(i);
                break;
            }
        }
    }

    int row = (pos.y() - m_style->horizontalHeader()->height() + viewPos().y()) / m_style->cellStyle()->height();
    if (row >= m_rowsCount) row = m_rowsCount - 1;

    res.setY(row);

    return res;
}

int Table::getInternalIndex(int index)
{
    if (m_columnIndeces.contains(index)) return m_columnIndeces[index];
    return -1;
}

void Table::setColumnsMovable(bool columnsMovable)
{
    if (m_columnsMovable == columnsMovable)
        return;

    m_columnsMovable = columnsMovable;
    emit columnsMovableChanged(m_columnsMovable);
}

void Table::setPressed(bool pressed)
{
    if (m_pressed == pressed)
        return;

    m_pressed = pressed;
    emit pressedChanged(m_pressed);
}

void Table::setColumnsMoving(bool columnsMoving)
{
    if (m_columnsMoving == columnsMoving)
        return;

    m_columnsMoving = columnsMoving;
    emit columnsMovingChanged(m_columnsMoving);
}

void Table::setInterractive(bool interractive)
{
    if (m_interractive == interractive)
        return;

    m_interractive = interractive;
    emit interractiveChanged(m_interractive);
}

void Table::setReachedBottom(bool reachedBottom)
{
    if (m_reachedBottom == reachedBottom)
        return;

    m_reachedBottom = reachedBottom;
    emit reachedBottomChanged(m_reachedBottom);
}

void Table::setPagesCount(int pagesCount)
{
    if (m_pagesCount == pagesCount)
        return;

    m_pagesCount = pagesCount;
    emit pagesCountChanged(m_pagesCount);
}

void Table::setViewPos(QRectF viewPos)
{
    if (m_viewPos == viewPos)
        return;

    m_viewPos = viewPos;
    updatePageVisibility();
    emit viewPosChanged(m_viewPos);
}

void Table::setContentWidth(double contentWidth)
{
    if (qFuzzyCompare(m_contentWidth, contentWidth))
        return;

    m_contentWidth = contentWidth;
    emit contentWidthChanged(m_contentWidth);
}

void Table::setRowsCount(int rowsCount)
{
    if (m_rowsCount == rowsCount)
        return;
    m_rowsCount = rowsCount;
    emit contentHeightChanged(contentHeight());
    emit rowsCountChanged(m_rowsCount);
}

void Table::setColumnsCount(int columnCount)
{
    if (m_columnsCount == columnCount)
        return;

    m_columnsCount = columnCount;
    emit columnsCountChanged(m_columnsCount);
}

void Table::setResizing(bool resizing)
{
    if (m_resizing == resizing)
        return;

    m_resizing = resizing;
    if (!m_resizing){
        updateContentWidth();
    }
    emit resizingChanged(m_resizing);
}

/*!
    \qmlproperty Table LightTable::Table::viewPos
    This property holds the visual rectF of the Table.
*/
/*!
    \property Table::viewPos
    This property holds the visual rectF of the Table.
*/
TableStyle *Table::style() const
{
    return m_style;
}
/*!
    \qmlproperty Table LightTable::Table::viewPos
    This property holds the visual rectF of the Table.
*/
/*!
    \property Table::viewPos
    This property holds the visual rectF of the Table.
*/
QRectF Table::viewPos()                  const { return m_viewPos; }
/*!
    \qmlproperty Table LightTable::Table::resizing
    \readonly
    This property holds the state of flag. Changes its state to \c true when colmn resizing in process.
*/
/*!
    \property Table::resizing
    This property holds the state of flag. Changes its state to \c true when colmn resizing in process.
*/
bool Table::resizing()                   const { return m_resizing; }
/*!
    \qmlproperty Table LightTable::Table::provider
    This property holds the state of flag. Changes its state to \c true when colmn resizing in process.
*/
/*!
    \property Table::provider
    This property holds the ITableDataProvider.
*/
QObject *Table::provider()               const { return m_provider; }
/*!
    \qmlproperty Table LightTable::Table::rowsCount
    \readonly
    This property holds the number of rows (which had been loaded in a while ago).
*/
/*!
    \property Table::rowsCount
    This property holds the number of rows (which had been loaded in a while ago).
*/
int Table::rowsCount()                   const { return m_rowsCount; }
/*!
    \qmlproperty Table LightTable::Table::rowsCount
    \readonly
    This property holds the currentRow's index.
*/
/*!
    \property Table::rowsCount
    This property holds currentRow's index.
*/
int Table::currentRow()                  const { return m_currentRow; }
/*!
    \qmlproperty Table LightTable::Table::pagesCount
    \readonly
    This property holds the number of pages.
*/
/*!
    \property Table::pagesCount
    This property holds number of pages.
*/
int Table::pagesCount()                  const { return m_pagesCount; }
/*!
    \qmlproperty Table LightTable::Table::columnCount
    \readonly
    This property holds the number of columns.
*/
/*!
    \property Table::columnCount
    This property holds number of columns.
*/
int Table::columnsCount()                 const { return m_columnsCount; }
/*!
    \qmlproperty Table LightTable::Table::contentWidth
    \readonly
    This property holds the width of whole content.
*/
/*!
    \property Table::contentWidth
    This property holds the width of whole content.
*/
double Table::contentWidth()             const { return m_contentWidth; }
/*!
    \qmlproperty Table LightTable::Table::currentColumn
    \readonly
    This property holds the currentColumn's index.
*/
/*!
    \property Table::currentColumn
    This property holds the currentColumn's index.
*/
int Table::currentColumn()               const { return m_currentColumn; }
/*!
    \qmlproperty Table LightTable::Table::reachedBottom
    \readonly
    This property holds the flag which display state of provider's full filling.
*/
/*!
    \property Table::reachedBottom
    This property holds the flag which display state of provider's full filling.
*/
bool Table::reachedBottom()              const { return m_reachedBottom; }
/*!
    \qmlproperty Table LightTable::Table::selectedRowsCount
    \readonly
    This property holds the number of selected rows.
    This number can be less than \c 0, that show us that selection move to top of table.
    If number higher than \c 0, that means that the selection moves to bottom of table.
    The range is [-LightTable::Table::rowsCount, 0) && (0, LightTable::Table::rowsCount].
*/
/*!
    \property Table::selectedRowsCount
    This property holds the number of selected rows.
    This number can be less than \c 0, that show us that selection move to top of table.
    If number higher than \c 0, that means that the selection moves to bottom of table.
    The range is [-LightTable::Table::rowsCount, 0) && (0, LightTable::Table::rowsCount].
*/
int Table::selectedRowsCount()           const { return m_selectedRowsCount; }
/*!
    \qmlproperty Table LightTable::Table::selectedColumnsCount
    \readonly
    This property holds the number of selected columns.
    This number can be less than \c 0, that show us that selection move to top of table.
    If number higher than \c 0, that means that the selection moves to bottom of table.
    The range is [-LightTable::Table::columnsCount, 0) && (0, LightTable::Table::rowsCount].
*/
/*!
    \property Table::selectedColumnsCount
    This property holds
*/
int Table::selectedColumnsCount()        const { return m_selectedColumnsCount; }

bool Table::columnsMovable() const
{
    return m_columnsMovable;
}

bool Table::pressed() const
{
    return m_pressed;
}

bool Table::columnsMoving() const
{
    return m_columnsMoving;
}
/*!
    \qmlproperty Table LightTable::Table::contentHeight
    \readonly
    This property holds the height of whole content.
*/
/*!
    \property Table::contentHeight
    This property holds the height of whole content.
*/
double Table::contentHeight()            const { return m_rowsCount * m_style->cellStyle()->height() + m_style->horizontalHeader()->height(); }
