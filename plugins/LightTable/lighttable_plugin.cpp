#include "lighttable_plugin.h"

#include <table.h>
#include <styles/cellfontstyle.h>
#include <styles/lazytablestyle.h>
#include <QQmlModuleRegistration>
#include <qqml.h>

void LightTablePlugin::registerTypes(const char *uri)
{
    // @uri LightTable
    qmlRegisterSingletonType<LazyTableStyle>(uri, 1, 0, "LazyTableStyle", &LazyTableStyle::qmlInstance);
    qmlRegisterType<Table>("LightTablePrivate", 1, 0, "TablePrivate");
    qmlRegisterType<TableStyle>(uri, 1, 0, "TableStyle");
    qmlRegisterUncreatableType<CellStyle>(uri, 1, 0, "CellStyle", "Uncreatable type");
    qmlRegisterUncreatableType<CellFontStyle>(uri, 1, 0, "CellFontStyle", "Uncreatable type");
    qmlRegisterUncreatableType<HeaderStyle>(uri, 1, 0, "HeaderStyle", "Uncreatable type");

}


