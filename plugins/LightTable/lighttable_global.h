#ifndef LIGHTTABLE_GLOBAL_H
#define LIGHTTABLE_GLOBAL_H


#include <QtCore/qglobal.h>

#if defined(LIGHTTABLE_LIBRARY)
#  define LIGHTTABLE_EXPORT Q_DECL_EXPORT
#else
#  define LIGHTTABLE_EXPORT Q_DECL_IMPORT
#endif

#endif // LIGHTTABLE_GLOBAL_H
