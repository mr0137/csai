#ifndef LAZYTABLESTYLE_H
#define LAZYTABLESTYLE_H

#include <QObject>
#include <QQmlEngine>
#include <styles/tablestyle.h>
#include "lighttable_global.h"

class LIGHTTABLE_EXPORT LazyTableStyle : public QObject
{
    Q_OBJECT
    explicit LazyTableStyle(QObject *parent = nullptr);
public:
    static LazyTableStyle *instance();
    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine);

    enum Style{
        Default,
        Excel
    };
    Q_ENUMS(Style)

    enum HeaderSpace{
        Free,
        Button
    };
    Q_ENUMS(HeaderSpace)

public slots:
    TableStyle* getExcelStyle();
    TableStyle* getDefaultStyle();
private:
    TableStyle *m_excelStyle = nullptr;
    TableStyle *m_defaultStyle = nullptr;
};

#endif // LAZYTABLESTYLE_H
