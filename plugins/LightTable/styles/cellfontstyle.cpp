#include "cellfontstyle.h"

CellFontStyle::CellFontStyle(QObject *parent) : QObject(parent)
{
    m_font.setBold(false);
    m_font.setFamily("Arial");
    m_font.setOverline(false);
    m_font.setLetterSpacing(QFont::AbsoluteSpacing, 0);
    m_font.setPointSizeF(9);
    m_font.setWordSpacing(10);
    m_font.setUnderline(false);
}

QFont CellFontStyle::font()
{
    return m_font;
}

const QString CellFontStyle::family() const
{
    return m_font.family();
}

void CellFontStyle::setFamily(const QString &newFamily)
{
    if (m_font.family() == newFamily)
        return;
    m_font.setFamily(newFamily);
    emit fontChanged();
    emit familyChanged();
}

qreal CellFontStyle::pointSize() const
{
    return m_font.pointSizeF();
}

void CellFontStyle::setPointSize(qreal newPointSize)
{
    if (qFuzzyCompare(m_font.pointSizeF(), newPointSize))
        return;

    m_font.setPointSizeF(newPointSize);
        emit fontChanged();
    emit pointSizeChanged();
}

qreal CellFontStyle::letterSpacing() const
{
    return m_font.letterSpacing();
}

void CellFontStyle::setletterSpacing(qreal newLetterSpacing)
{
    if (qFuzzyCompare(m_font.letterSpacing(), newLetterSpacing))
        return;
    m_font.setLetterSpacing(QFont::AbsoluteSpacing, newLetterSpacing);
        emit fontChanged();
    emit letterSpacingChanged();
}

bool CellFontStyle::overline() const
{
    return m_font.overline();
}

void CellFontStyle::setOverline(bool newOverline)
{
    if (m_font.overline() == newOverline)
        return;
    m_font.setOverline(newOverline);
        emit fontChanged();
    emit overlineChanged();
}

bool CellFontStyle::bold() const
{
    return m_font.bold();
}

void CellFontStyle::setBold(bool newBold)
{
    if (m_font.bold() == newBold)
        return;
    m_font.setBold(newBold);
    emit boldChanged();
}

bool CellFontStyle::underline() const
{
    return m_font.underline();
}

void CellFontStyle::setUnderline(bool newUnderline)
{
    if (m_font.underline() == newUnderline)
        return;
    m_font.setUnderline(newUnderline);
    emit underlineChanged();
}

qreal CellFontStyle::wordSpacing() const
{
    return m_font.wordSpacing();
}

void CellFontStyle::setWordSpacing(qreal newWordSpacing)
{
    if (qFuzzyCompare(m_font.wordSpacing(), newWordSpacing))
        return;
    m_font.setWordSpacing(newWordSpacing);
    emit wordSpacingChanged();
}

void CellFontStyle::reset()
{
    m_font.setBold(false);
    m_font.setFamily("Arial");
    m_font.setOverline(false);
    m_font.setLetterSpacing(QFont::AbsoluteSpacing, 0);
    m_font.setPointSizeF(9);
    m_font.setWordSpacing(10);
    m_font.setUnderline(false);
    emit fontChanged();
}
