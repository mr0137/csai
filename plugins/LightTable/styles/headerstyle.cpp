#include "headerstyle.h"

HeaderStyle::HeaderStyle(HeaderType type, QObject *parent) : QObject(parent)
{
    m_headerType = type;
    m_font = new CellFontStyle(this);

    connect(m_font, &CellFontStyle::fontChanged, this, [this](){
        emit this->headerChanged();
    });
}

double HeaderStyle::height() const
{
    return m_height;
}

void HeaderStyle::setHeight(double newHeight)
{
    if (qFuzzyCompare(m_height, newHeight))
        return;
    m_height = newHeight;
    emit heightChanged();
    emit headerChanged();
}

double HeaderStyle::width() const
{
    return m_width;
}

void HeaderStyle::setWidth(double newWidth)
{
    if (!m_widthChangeable) {
        qDebug() << "You can't change width of the header";
        return;
    }

    if (qFuzzyCompare(m_width, newWidth))
        return;
    m_width = newWidth;
    emit widthChanged();
    emit headerChanged();
}

const QColor HeaderStyle::backgroundColor() const
{
    return m_backgroundColor;
}

void HeaderStyle::setBackgroundColor(const QColor &newBackgroundColor)
{
    if (m_backgroundColor == newBackgroundColor)
        return;
    m_backgroundColor = newBackgroundColor;
    emit backgroundColorChanged();
    emit headerChanged();
}

const QColor HeaderStyle::textColor() const
{
    return m_textColor;
}

void HeaderStyle::setTextColor(const QColor &newTextColor)
{
    if (m_textColor == newTextColor)
        return;
    m_textColor = newTextColor;
    emit textColorChanged();
    emit headerChanged();
}

int HeaderStyle::leftMargin() const
{
    return m_leftMargin;
}

void HeaderStyle::setLeftMargin(int newLeftMargin)
{
    if (m_leftMargin == newLeftMargin)
        return;
    m_leftMargin = newLeftMargin;
    emit leftMarginChanged();
    emit headerChanged();
}

int HeaderStyle::rightMargin() const
{
    return m_rightMargin;
}

void HeaderStyle::setRightMargin(int newRightMargin)
{
    if (m_rightMargin == newRightMargin)
        return;
    m_rightMargin = newRightMargin;
    emit rightMarginChanged();
    emit headerChanged();
}

CellFontStyle *HeaderStyle::fontStyle()
{
    return m_font;
}

HeaderStyle::HeaderType HeaderStyle::headerType() const
{
    return m_headerType;
}

quint32 HeaderStyle::alignment() const
{
    return m_alignment;
}

void HeaderStyle::setAlignment(quint32 newAlignment)
{
    if (m_alignment == newAlignment)
        return;
    m_alignment = newAlignment;
    emit alignmentChanged();
}

QColor HeaderStyle::pinnedColor() const
{
    return m_pinnedColor;
}

void HeaderStyle::setPinnedColor(QColor pinnedColor)
{
    if (m_pinnedColor == pinnedColor)
        return;

    m_pinnedColor = pinnedColor;
    emit pinnedColorChanged();
}
