#include "cellstyle.h"

CellStyle::CellStyle(QObject *parent) : QObject(parent)
{
    m_font = new CellFontStyle(this);
}

CellFontStyle *CellStyle::fontStyle()
{
    return m_font;
}

int CellStyle::rightMargin() const
{
    return m_rightMargin;
}

void CellStyle::setRightMargin(int newRightMargin)
{
    if (m_rightMargin == newRightMargin)
        return;
    m_rightMargin = newRightMargin;
    emit rightMarginChanged();
}

int CellStyle::leftMargin() const
{
    return m_leftMargin;
}

void CellStyle::setLeftMargin(int newLeftMargin)
{
    if (m_leftMargin == newLeftMargin)
        return;
    m_leftMargin = newLeftMargin;
    emit leftMarginChanged();
}

const QColor &CellStyle::textColor() const
{
    return m_textColor;
}

void CellStyle::setTextColor(const QColor &newTextColor)
{
    if (m_textColor == newTextColor)
        return;
    m_textColor = newTextColor;
    emit textColorChanged();
}

const QColor &CellStyle::backgroundColor() const
{
    return m_backgroundColor;
}

void CellStyle::setBackgroundColor(const QColor &newBackgroundColor)
{
    if (m_backgroundColor == newBackgroundColor)
        return;
    m_backgroundColor = newBackgroundColor;
    emit backgroundColorChanged();
}

double CellStyle::height() const
{
    return m_height;
}

void CellStyle::setHeight(double newHeight)
{
    if (qFuzzyCompare(m_height, newHeight))
        return;
    m_height = newHeight;
    emit heightChanged();
}

quint32 CellStyle::alignment() const
{
    return m_alignment;
}

void CellStyle::setAlignment(quint32 newAlignment)
{
    if (m_alignment == newAlignment)
        return;
    m_alignment = newAlignment;
    emit alignmentChanged();
}

quint8 CellStyle::elide() const
{
    return m_elide;
}

void CellStyle::setElide(quint8 newElide)
{
    if (m_elide == newElide)
        return;
    m_elide = newElide;
    emit elideChanged();
}
