#=============EDITED_BY_PLUGINHELPER=============#
TEMPLATE = lib
TARGET = LightTable
QT += qml
QT += core
QT += quick
QT += concurrent
QT += svg
CONFIG += plugin
CONFIG += c++17
CONFIG += qt
CONFIG += qmltypes

QML_IMPORT_NAME = LightTable
QML_IMPORT_MAJOR_VERSION = 1.0
DEFINES += LIGHTTABLE_LIBRARY
DESTDIR = $$PWD/../../bin/plugins/$$QML_IMPORT_NAME
QMLTYPES_FILENAME = $$DESTDIR/LightTable.qmltypes
uri = LightTable

#Inputs
SOURCES += \
        #itablecolorprovider.cpp \
        scenegraph/managedtexturenode.cpp \
        scenegraph/paintedrectangleitem.cpp \
        scenegraph/shadowedborderrectanglematerial.cpp \
        scenegraph/shadowedbordertexturematerial.cpp \
        scenegraph/shadowedrectanglematerial.cpp \
        scenegraph/shadowedrectanglenode.cpp \
        scenegraph/shadowedtexturematerial.cpp \
        scenegraph/shadowedtexturenode.cpp \
        styles/cellfontstyle.cpp \
        styles/headerstyle.cpp \
        table.cpp \
        lighttable_plugin.cpp \
        #itabledataprovider.cpp \
        styles/cellstyle.cpp \
        styles/tablestyle.cpp \
        styles/lazytablestyle.cpp


HEADERS += \
        #itablecolorprovider.h \
        scenegraph/managedtexturenode.h \
        scenegraph/paintedrectangleitem.h \
        scenegraph/shadowedborderrectanglematerial.h \
        scenegraph/shadowedbordertexturematerial.h \
        scenegraph/shadowedrectanglematerial.h \
        scenegraph/shadowedrectanglenode.h \
        scenegraph/shadowedtexturematerial.h \
        scenegraph/shadowedtexturenode.h \
        styles/cellfontstyle.h \
        styles/headerstyle.h \
        table.h \
        #itabledataprovider.h \
        lighttable_global.h \
        lighttable_plugin.h \
        styles/cellstyle.h \
        styles/tablestyle.h \
        styles/lazytablestyle.h


DISTFILES = \
    TableScrollBar.qml \
    icons/filter.svg \
    icons/sort.svg \
    qmldir \
    LazyTable.qml 


#=============QMLDIR_COPY=============#
!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
    copy_qmldir.target = $$DESTDIR/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
    copy_qmldir.commands = $(COPY_FILE) "$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)" "$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}

#====================SPECIAL_PROPERTIES_STARTS====================

CONFIG(release, debug|release){
LIBS += -L$$PWD/../../bin/libs/ -lLightTableBase
}else{
win32: LIBS += -L$$OUT_PWD/../../libs/LightTableBase/debug/ -lLightTableBase
!android: LIBS += -L$$OUT_PWD/../../libs/LightTableBase/ -lLightTableBase
}

android: {
LIBS += -L$$OUT_PWD/../../libs/LightTableBase/ -lLightTableBase_armeabi-v7a
}

INCLUDEPATH += $$PWD/../../libs/LightTableBase
DEPENDPATH += $$PWD/../../libs/LightTableBase


#====================SPECIAL_PROPERTIES_ENDS====================

#=============COPIES_STARTS=============#
qmldir.files = qmldir

default_copy.files = \ 
     LazyTable.qml \ 
     TableScrollBar.qml \
     qmldir
default_copy.path = $$DESTDIR/

icons_copy.files = \
    icons/filter.svg \
    icons/sort.svg
icons_copy.path = $$DESTDIR/

COPIES += default_copy icons_copy
#=============COPIES_END=============#

RESOURCES += \
    resources.qrc \
    scenegraph/shaders/shaders.qrc

