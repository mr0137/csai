#include "voronin_plugin.h"

#include <QColor>
#include <voroninalgorithm.h>
#include <qqml.h>

QString VoroninPlugin::titleName()
{
    return "Voronin M. KV-02mp";
}

IAlgorithm *VoroninPlugin::algorithm()
{
    return new VoroninAlgorithm(this);
}

QVariantMap VoroninPlugin::theme()
{
    return QVariantMap{
        {"textColor", "white"},
        {"lightBackgroundColor", "#065ED1"},
        {"darkBackgroundColor", "#022552"},
        {"buttonColor", "#05479E"},
        {"darkBackgroundColor2", QColor("#022552").lighter(130)},
        {"highlightColor", "black"}
    };
}

QIcon VoroninPlugin::appIcon()
{
    return QIcon(":/codeEAN.png");
}

