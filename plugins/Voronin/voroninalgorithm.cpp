#include "voroninalgorithm.h"
#include <QPainter>

QImage VoroninAlgorithm::generate(QString code, double width, double height , QColor color)
{
    QImage img(width, height, QImage::Format_ARGB32);
    //img.fill(QColor(0,0,0,0));
    img.fill(QColor("white"));

    QPainter painter(&img);
    Zint::QZint bc;

    bc.setHeight(height);
    bc.setWidth(width);
    bc.setText(code);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setSymbol(BARCODE_EANX);

    bc.render(painter, QRectF(5, 10, width - 10, height - 20));

    return img;
}

void VoroninAlgorithm::decode(const QImage &img)
{
    m_zxing->decodeImage(img, img.width(), img.height(), true);
}
