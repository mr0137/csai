#ifndef HNATENKO_GLOBAL_H
#define HNATENKO_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(VORONIN_LIBRARY)
#  define VORONIN_EXPORT Q_DECL_EXPORT
#else
#  define VORONIN_EXPORT Q_DECL_IMPORT
#endif


#endif // HNATENKO_GLOBAL_H
