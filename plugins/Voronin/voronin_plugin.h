#ifndef HNATENKO_PLUGIN_H
#define HNATENKO_PLUGIN_H

#include <csaibase.h>
#include <voronin_global.h>

class VORONIN_EXPORT VoroninPlugin : public QObject, public CSAIBase
{
    Q_OBJECT
    Q_INTERFACES(CSAIBase)
    Q_PLUGIN_METADATA(IID CSAIBase_iid FILE "csaibase.json")
public:
    virtual QString titleName() override;
    virtual IAlgorithm *algorithm() override;
    virtual QVariantMap theme() override;
    virtual QIcon appIcon() override;
};

#endif // HNATENKO_PLUGIN_H
