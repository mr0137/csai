#ifndef QRCODE_H
#define QRCODE_H

#include <array>
#include <cstdint>
#include <stdexcept>
#include <string>
#include <vector>
#include <hnatenko_global.h>

class HNATENKO_EXPORT QrSegment final {

    /*---- Public helper enumeration ----*/

    /*
     * Describes how a segment's data bits are interpreted. Immutable.
     */
public: class Mode final {

        /*-- Constants --*/

    public: static const Mode NUMERIC;
    public: static const Mode ALPHANUMERIC;
    public: static const Mode BYTE;
    public: static const Mode KANJI;
    public: static const Mode ECI;


        /*-- Fields --*/

        // The mode indicator bits, which is a uint4 value (range 0 to 15).
    private: int modeBits;

        // Number of character count bits for three different version ranges.
    private: int numBitsCharCount[3];


        /*-- Constructor --*/

    private: Mode(int mode, int cc0, int cc1, int cc2);


        /*-- Methods --*/

        /*
         * (Package-private) Returns the mode indicator bits, which is an unsigned 4-bit value (range 0 to 15).
         */
    public: int getModeBits() const;

        /*
         * (Package-private) Returns the bit width of the character count field for a segment in
         * this mode in a QR Code at the given version number. The result is in the range [0, 16].
         */
    public: int numCharCountBits(int ver) const;

    };



    /*---- Static factory functions (mid level) ----*/

    /*
     * Returns a segment representing the given binary data encoded in
     * byte mode. All input byte vectors are acceptable. Any text string
     * can be converted to UTF-8 bytes and encoded as a byte mode segment.
     */
public: static QrSegment makeBytes(const std::vector<std::uint8_t> &data);


    /*
     * Returns a segment representing the given string of decimal digits encoded in numeric mode.
     */
public: static QrSegment makeNumeric(const char *digits);


    /*
     * Returns a segment representing the given text string encoded in alphanumeric mode.
     * The characters allowed are: 0 to 9, A to Z (uppercase only), space,
     * dollar, percent, asterisk, plus, hyphen, period, slash, colon.
     */
public: static QrSegment makeAlphanumeric(const char *text);


    /*
     * Returns a list of zero or more segments to represent the given text string. The result
     * may use various segment modes and switch modes to optimize the length of the bit stream.
     */
public: static std::vector<QrSegment> makeSegments(const char *text);


    /*
     * Returns a segment representing an Extended Channel Interpretation
     * (ECI) designator with the given assignment value.
     */
public: static QrSegment makeEci(long assignVal);


    /*---- Public static helper functions ----*/

    /*
     * Tests whether the given string can be encoded as a segment in alphanumeric mode.
     * A string is encodable iff each character is in the following set: 0 to 9, A to Z
     * (uppercase only), space, dollar, percent, asterisk, plus, hyphen, period, slash, colon.
     */
public: static bool isAlphanumeric(const char *text);


    /*
     * Tests whether the given string can be encoded as a segment in numeric mode.
     * A string is encodable iff each character is in the range 0 to 9.
     */
public: static bool isNumeric(const char *text);



    /*---- Instance fields ----*/

    /* The mode indicator of this segment. Accessed through getMode(). */
private: Mode mode;

    /* The length of this segment's unencoded data. Measured in characters for
     * numeric/alphanumeric/kanji mode, bytes for byte mode, and 0 for ECI mode.
     * Always zero or positive. Not the same as the data's bit length.
     * Accessed through getNumChars(). */
private: int numChars;

    /* The data bits of this segment. Accessed through getData(). */
private: std::vector<bool> data;


    /*---- Constructors (low level) ----*/

    /*
     * Creates a new QR Code segment with the given attributes and data.
     * The character count (numCh) must agree with the mode and the bit buffer length,
     * but the constraint isn't checked. The given bit buffer is copied and stored.
     */
public: QrSegment(Mode md, int numCh, const std::vector<bool> &dt);


    /*
     * Creates a new QR Code segment with the given parameters and data.
     * The character count (numCh) must agree with the mode and the bit buffer length,
     * but the constraint isn't checked. The given bit buffer is moved and stored.
     */
public: QrSegment(Mode md, int numCh, std::vector<bool> &&dt);


    /*---- Methods ----*/

    /*
     * Returns the mode field of this segment.
     */
public: Mode getMode() const;


    /*
     * Returns the character count field of this segment.
     */
public: int getNumChars() const;


    /*
     * Returns the data bits of this segment.
     */
public: const std::vector<bool> &getData() const;


    // (Package-private) Calculates the number of bits needed to encode the given segments at
    // the given version. Returns a non-negative number if successful. Otherwise returns -1 if a
    // segment has too many characters to fit its length field, or the total bits exceeds INT_MAX.
public: static int getTotalBits(const std::vector<QrSegment> &segs, int version);


    /*---- Private constant ----*/

    /* The set of all legal characters in alphanumeric mode, where
     * each character value maps to the index in the string. */
private: static const char *ALPHANUMERIC_CHARSET;

};


class HNATENKO_EXPORT QrCode final {

public: enum class Ecc {
        LOW = 0 ,  // The QR Code can tolerate about  7% erroneous codewords
        MEDIUM  ,  // The QR Code can tolerate about 15% erroneous codewords
        QUARTILE,  // The QR Code can tolerate about 25% erroneous codewords
        HIGH    ,  // The QR Code can tolerate about 30% erroneous codewords
    };

    static QrCode encodeText(const char *text, Ecc ecl);
    static QrCode encodeBinary(const std::vector<std::uint8_t> &data, Ecc ecl);
    static QrCode encodeSegments(const std::vector<QrSegment> &segs, Ecc ecl,
                                 int minVersion=1, int maxVersion=40, int mask=-1, bool boostEcl=true);  // All optional parameters

    QrCode(int ver, Ecc ecl, const std::vector<std::uint8_t> &dataCodewords, int msk);
    int getVersion() const;
    int getSize() const;
    Ecc getErrorCorrectionLevel() const;
    int getMask() const;
    bool getModule(int x, int y) const;
    std::string toSvgString(int border) const;
    static constexpr int MIN_VERSION =  1;
    static constexpr int MAX_VERSION = 40;
private: int version;

    int size;

    Ecc errorCorrectionLevel;

    int mask;

    std::vector<std::vector<bool> > modules;

    std::vector<std::vector<bool> > isFunction;


    // Returns a value in the range 0 to 3 (unsigned 2-bit integer).
    static int getFormatBits(Ecc ecl);


    void drawFunctionPatterns();
    void drawFormatBits(int msk);
    void drawVersion();
    void drawFinderPattern(int x, int y);
    void drawAlignmentPattern(int x, int y);
    void setFunctionModule(int x, int y, bool isBlack);
    bool module(int x, int y) const;
    std::vector<std::uint8_t> addEccAndInterleave(const std::vector<std::uint8_t> &data) const;
    void drawCodewords(const std::vector<std::uint8_t> &data);
    void applyMask(int msk);
    long getPenaltyScore() const;
    std::vector<int> getAlignmentPatternPositions() const;
    static int getNumRawDataModules(int ver);
    static int getNumDataCodewords(int ver, Ecc ecl);
    static std::vector<std::uint8_t> reedSolomonComputeDivisor(int degree);
    static std::vector<std::uint8_t> reedSolomonComputeRemainder(const std::vector<std::uint8_t> &data, const std::vector<std::uint8_t> &divisor);
    static std::uint8_t reedSolomonMultiply(std::uint8_t x, std::uint8_t y);
    int finderPenaltyCountPatterns(const std::array<int,7> &runHistory) const;
    int finderPenaltyTerminateAndCount(bool currentRunColor, int currentRunLength, std::array<int,7> &runHistory) const;
    void finderPenaltyAddHistory(int currentRunLength, std::array<int,7> &runHistory) const;

    static bool getBit(long x, int i);


    static const int PENALTY_N1;
    static const int PENALTY_N2;
    static const int PENALTY_N3;
    static const int PENALTY_N4;


    static const std::int8_t ECC_CODEWORDS_PER_BLOCK[4][41];
    static const std::int8_t NUM_ERROR_CORRECTION_BLOCKS[4][41];

};


class HNATENKO_EXPORT data_too_long : public std::length_error {

public: explicit data_too_long(const std::string &msg);

};

class BitBuffer final : public std::vector<bool> {
    // Creates an empty bit buffer (length 0).
public: BitBuffer();
    // Appends the given number of low-order bits of the given value
    // to this buffer. Requires 0 <= len <= 31 and val < 2^len.
public: void appendBits(std::uint32_t val, int len);

};


#endif // QRCODE_H
