TEMPLATE = lib
TARGET = Algorithm
QT += qml quick core multimedia
CONFIG += plugin c++17
CONFIG += shared dll
QML_IMPORT_NAME = Algorithm
DESTDIR = $$PWD/../../bin/plugins/$$QML_IMPORT_NAME
uri = Algorithm
DEFINES += HNATENKO_LIBRARY \
           ZXING_ICONV_CONST \
           DISABLE_LIBRARY_FEATURES
# Input
SOURCES += \
        QZXing/BarcodeFormat.cpp \
        QZXing/Binarizer.cpp \
        QZXing/BinaryBitmap.cpp \
        QZXing/CameraImageWrapper.cpp \
        QZXing/ChecksumException.cpp \
        QZXing/DecodeHints.cpp \
        QZXing/EncodeHint.cpp \
        QZXing/Exception.cpp \
        QZXing/FormatException.cpp \
        QZXing/InvertedLuminanceSource.cpp \
        QZXing/LuminanceSource.cpp \
        QZXing/MultiFormatReader.cpp \
        QZXing/QZXingImageProvider.cpp \
        QZXing/Reader.cpp \
        QZXing/Result.cpp \
        QZXing/ResultIO.cpp \
        QZXing/ResultPoint.cpp \
        QZXing/ResultPointCallback.cpp \
        QZXing/bigint/BigInteger.cc \
        QZXing/bigint/BigIntegerAlgorithms.cc \
        QZXing/bigint/BigIntegerUtils.cc \
        QZXing/bigint/BigUnsigned.cc \
        QZXing/bigint/BigUnsignedInABase.cc \
        QZXing/common/BitArray.cpp \
        QZXing/common/BitArrayIO.cpp \
        QZXing/common/BitMatrix.cpp \
        QZXing/common/BitSource.cpp \
        QZXing/common/CharacterSetECI.cpp \
        QZXing/common/DecoderResult.cpp \
        QZXing/common/DetectorResult.cpp \
        QZXing/common/GlobalHistogramBinarizer.cpp \
        QZXing/common/GreyscaleLuminanceSource.cpp \
        QZXing/common/GreyscaleRotatedLuminanceSource.cpp \
        QZXing/common/GridSampler.cpp \
        QZXing/common/HybridBinarizer.cpp \
        QZXing/common/IllegalArgumentException.cpp \
        QZXing/common/PerspectiveTransform.cpp \
        QZXing/common/Str.cpp \
        QZXing/common/StringUtils.cpp \
        QZXing/common/detector/MonochromeRectangleDetector.cpp \
        QZXing/common/detector/WhiteRectangleDetector.cpp \
        QZXing/common/reedsolomon/GenericGF.cpp \
        QZXing/common/reedsolomon/GenericGFPoly.cpp \
        QZXing/common/reedsolomon/ReedSolomonDecoder.cpp \
        QZXing/common/reedsolomon/ReedSolomonEncoder.cpp \
        QZXing/common/reedsolomon/ReedSolomonException.cpp \
        QZXing/imagehandler.cpp \
        QZXing/oned/CodaBarReader.cpp \
        QZXing/oned/Code128Reader.cpp \
        QZXing/oned/Code39Reader.cpp \
        QZXing/oned/Code93Reader.cpp \
        QZXing/oned/EAN13Reader.cpp \
        QZXing/oned/EAN8Reader.cpp \
        QZXing/oned/ITFReader.cpp \
        QZXing/oned/MultiFormatOneDReader.cpp \
        QZXing/oned/MultiFormatUPCEANReader.cpp \
        QZXing/oned/OneDReader.cpp \
        QZXing/oned/OneDResultPoint.cpp \
        QZXing/oned/UPCAReader.cpp \
        QZXing/oned/UPCEANReader.cpp \
        QZXing/oned/UPCEReader.cpp \
        QZXing/qrcode/QRCodeReader.cpp \
        QZXing/qrcode/QRErrorCorrectionLevel.cpp \
        QZXing/qrcode/QRFormatInformation.cpp \
        QZXing/qrcode/QRVersion.cpp \
        QZXing/qrcode/decoder/QRBitMatrixParser.cpp \
        QZXing/qrcode/decoder/QRDataBlock.cpp \
        QZXing/qrcode/decoder/QRDataMask.cpp \
        QZXing/qrcode/decoder/QRDecodedBitStreamParser.cpp \
        QZXing/qrcode/decoder/QRDecoder.cpp \
        QZXing/qrcode/decoder/QRMode.cpp \
        QZXing/qrcode/detector/QRAlignmentPattern.cpp \
        QZXing/qrcode/detector/QRAlignmentPatternFinder.cpp \
        QZXing/qrcode/detector/QRDetector.cpp \
        QZXing/qrcode/detector/QRFinderPattern.cpp \
        QZXing/qrcode/detector/QRFinderPatternFinder.cpp \
        QZXing/qrcode/detector/QRFinderPatternInfo.cpp \
        QZXing/qrcode/encoder/ByteMatrix.cpp \
        QZXing/qrcode/encoder/MaskUtil.cpp \
        QZXing/qrcode/encoder/MatrixUtil.cpp \
        QZXing/qrcode/encoder/QRCode.cpp \
        QZXing/qrcode/encoder/QREncoder.cpp \
        QZXing/qzxing.cpp \
        encoder/qrencoder2.cpp \
        hnatenko_plugin.cpp \
        hnatenkoalgorithm.cpp

HEADERS += \
        QZXing/BarcodeFormat.h \
        QZXing/Binarizer.h \
        QZXing/BinaryBitmap.h \
        QZXing/CameraImageWrapper.h \
        QZXing/ChecksumException.h \
        QZXing/DecodeHints.h \
        QZXing/EncodeHint.h \
        QZXing/Exception.h \
        QZXing/FormatException.h \
        QZXing/IllegalStateException.h \
        QZXing/InvertedLuminanceSource.h \
        QZXing/LuminanceSource.h \
        QZXing/MultiFormatReader.h \
        QZXing/NotFoundException.h \
        QZXing/QZXing.h \
        QZXing/QZXingImageProvider.h \
        QZXing/Reader.h \
        QZXing/ReaderException.h \
        QZXing/Result.h \
        QZXing/ResultPoint.h \
        QZXing/ResultPointCallback.h \
        QZXing/UnsupportedEncodingException.h \
        QZXing/WriterException.h \
        QZXing/ZXing.h \
        QZXing/bigint/BigInteger.hh \
        QZXing/bigint/BigIntegerAlgorithms.hh \
        QZXing/bigint/BigIntegerLibrary.hh \
        QZXing/bigint/BigIntegerUtils.hh \
        QZXing/bigint/BigUnsigned.hh \
        QZXing/bigint/BigUnsignedInABase.hh \
        QZXing/bigint/NumberlikeArray.hh \
        QZXing/common/Array.h \
        QZXing/common/BitArray.h \
        QZXing/common/BitMatrix.h \
        QZXing/common/BitSource.h \
        QZXing/common/CharacterSetECI.h \
        QZXing/common/Counted.h \
        QZXing/common/DecoderResult.h \
        QZXing/common/DetectorResult.h \
        QZXing/common/GlobalHistogramBinarizer.h \
        QZXing/common/GreyscaleLuminanceSource.h \
        QZXing/common/GreyscaleRotatedLuminanceSource.h \
        QZXing/common/GridSampler.h \
        QZXing/common/HybridBinarizer.h \
        QZXing/common/IllegalArgumentException.h \
        QZXing/common/PerspectiveTransform.h \
        QZXing/common/Point.h \
        QZXing/common/Str.h \
        QZXing/common/StringUtils.h \
        QZXing/common/Types.h \
        QZXing/common/detector/JavaMath.h \
        QZXing/common/detector/MathUtils.h \
        QZXing/common/detector/MonochromeRectangleDetector.h \
        QZXing/common/detector/WhiteRectangleDetector.h \
        QZXing/common/reedsolomon/GenericGF.h \
        QZXing/common/reedsolomon/GenericGFPoly.h \
        QZXing/common/reedsolomon/ReedSolomonDecoder.h \
        QZXing/common/reedsolomon/ReedSolomonEncoder.h \
        QZXing/common/reedsolomon/ReedSolomonException.h \
        QZXing/imagehandler.h \
        QZXing/oned/CodaBarReader.h \
        QZXing/oned/Code128Reader.h \
        QZXing/oned/Code39Reader.h \
        QZXing/oned/Code93Reader.h \
        QZXing/oned/EAN13Reader.h \
        QZXing/oned/EAN8Reader.h \
        QZXing/oned/ITFReader.h \
        QZXing/oned/MultiFormatOneDReader.h \
        QZXing/oned/MultiFormatUPCEANReader.h \
        QZXing/oned/OneDReader.h \
        QZXing/oned/OneDResultPoint.h \
        QZXing/oned/UPCAReader.h \
        QZXing/oned/UPCEANReader.h \
        QZXing/oned/UPCEReader.h \
        QZXing/qrcode/ErrorCorrectionLevel.h \
        QZXing/qrcode/FormatInformation.h \
        QZXing/qrcode/QRCodeReader.h \
        QZXing/qrcode/Version.h \
        QZXing/qrcode/decoder/BitMatrixParser.h \
        QZXing/qrcode/decoder/DataBlock.h \
        QZXing/qrcode/decoder/DataMask.h \
        QZXing/qrcode/decoder/DecodedBitStreamParser.h \
        QZXing/qrcode/decoder/Decoder.h \
        QZXing/qrcode/decoder/Mode.h \
        QZXing/qrcode/detector/AlignmentPattern.h \
        QZXing/qrcode/detector/AlignmentPatternFinder.h \
        QZXing/qrcode/detector/Detector.h \
        QZXing/qrcode/detector/FinderPattern.h \
        QZXing/qrcode/detector/FinderPatternFinder.h \
        QZXing/qrcode/detector/FinderPatternInfo.h \
        QZXing/qrcode/encoder/BlockPair.h \
        QZXing/qrcode/encoder/ByteMatrix.h \
        QZXing/qrcode/encoder/Encoder.h \
        QZXing/qrcode/encoder/MaskUtil.h \
        QZXing/qrcode/encoder/MatrixUtil.h \
        QZXing/qrcode/encoder/QRCode.h \
    encoder/qrencoder2.h \
        hnatenko_global.h \
        hnatenko_plugin.h \
        hnatenkoalgorithm.h \


CONFIG(release, debug|release){
LIBS += -L$$PWD/../../bin/libs/ -lCSAIBase
}else{
win32: LIBS += -L$$OUT_PWD/../../libs/CSAIBase/debug/ -lCSAIBase
!android: LIBS += -L$$OUT_PWD/../../libs/CSAIBase/ -lCSAIBase
}

android: {
LIBS += -L$$OUT_PWD/../../libs/CSAIBase/ -lCSAIBase_armeabi-v7a
}

INCLUDEPATH += $$PWD/../../libs/CSAIBase
DEPENDPATH += $$PWD/../../libs/CSAIBase

DISTFILES += \
    QZXing/bigint/ChangeLog \
    QZXing/bigint/README \
    QZXing/zxing/bigint/ChangeLog \
    QZXing/zxing/bigint/README


win32-g++{
    INCLUDEPATH += $$PWD/zxing/win32/zxing
    HEADERS += $$PWD/win32/zxing/iconv.h
    SOURCES += $$PWD/win32/zxing/win_iconv.c
}

!win32{
    DEFINES += NO_ICONV
}
winrt {
    DEFINES += NO_ICONV
}
