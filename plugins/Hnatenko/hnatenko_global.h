#ifndef HNATENKO_GLOBAL_H
#define HNATENKO_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(HNATENKO_LIBRARY)
#  define HNATENKO_EXPORT Q_DECL_EXPORT
#else
#  define HNATENKO_EXPORT Q_DECL_IMPORT
#endif


#endif // HNATENKO_GLOBAL_H
