#include "hnatenko_plugin.h"

#include <QImage>
#include <hnatenkoalgorithm.h>
#include <qqml.h>

QString HnatenkoPlugin::titleName()
{
    return "Hnatenko V.D. KV-01mp";
}

IAlgorithm *HnatenkoPlugin::algorithm()
{
    return new HnatenkoAlgorithm(this);
}

QVariantMap HnatenkoPlugin::theme()
{
    return QVariantMap{
        {"textColor", "#6ab2f2"},
        {"lightBackgroundColor", QColor("#24303d").lighter(130)},
        {"darkBackgroundColor", "#232e3c"},
        {"darkBackgroundColor2", "#232e3c"},
        {"buttonColor", "#2f6ea5"},
        {"highlightColor", "#6ab2f2"}
    };
}

QIcon HnatenkoPlugin::appIcon()
{
    return QIcon(":/code.png");
}

