#include "hnatenkoalgorithm.h"
#include <QPainter>
#include <encoder/qrencoder2.h>

QImage HnatenkoAlgorithm::generate(QString code, double width, double height , QColor color)
{
    QImage img(width, height, QImage::Format_ARGB32);
       //img.fill(QColor(0,0,0,0));
     img.fill(QColor("white"));

       QrEncode qr =  QrEncode::encodeText(code.toUtf8(), QrEncode::Ecc::QUARTILE);
       const int s = qr.getSize()>0?qr.getSize():1;
       const double w = width;
       const double h = height;
       const double aspect= w / h;
       const double size=((aspect>1.0)?h:w);
       const double scale=size/(s+2);

       QPainter painter(&img);
       painter.setPen(Qt::NoPen);
       painter.setBrush(QBrush("black"));
       for(int y=0; y<s; y++) {
           for(int x=0; x<s; x++) {
               const int color=qr.getModule(x, y);  // 0 for white, 1 for black
               if(0!=color) {
                   const double rx1=(x+1)*scale, ry1=(y+1)*scale;
                   QRectF r(rx1, ry1, scale, scale);
                   painter.drawRects(&r,1);
               }
           }
       }
       return img;

}

void HnatenkoAlgorithm::decode(const QImage &img)
{
    qDebug() << "decoding";
    m_zxing->decodeImage(img, img.width(), img.height(), true);
}
