#ifndef HNATENKOALGORITHM_H
#define HNATENKOALGORITHM_H

#include <ialgorithm.h>
#include <QObject>
#include <hnatenko_global.h>
#include <QZXing/QZXing.h>

class HNATENKO_EXPORT HnatenkoAlgorithm : public IAlgorithm
{
public:
    HnatenkoAlgorithm(QObject *parent = nullptr) : IAlgorithm(parent) {
        m_zxing = new QZXing(QZXing::DecoderFormat_QR_CODE);
        connect(m_zxing, SIGNAL(	decodingStarted() ), this,SIGNAL(decodingStarted()));
        connect(m_zxing, SIGNAL(	decodingFinished(bool) ), this,SIGNAL(decodingFinished(bool)));
        connect(m_zxing, &QZXing::tagFound, this, [this](QString str){
            qDebug() << "Tag" << str;
            emit this->tagFound(str);
        });
        connect(m_zxing, SIGNAL(	error(QString) ), this,SIGNAL(error(QString)));
    }

    // IAlgorithm interface
public:
    virtual QImage generate(QString code, double width, double height, QColor color = "green") override;
    virtual void decode(const QImage &img) override;
private:
    QZXing *m_zxing = nullptr;
};

#endif // HNATENKOALGORITHM_H
