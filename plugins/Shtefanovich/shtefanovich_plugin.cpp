#include "shtefanovich_plugin.h"

#include <QColor>
#include <shtefanovichalgorithm.h>
#include <qqml.h>

QString ShtefanovichPlugin::titleName()
{
    return "Shtefanovich G. M. KV-02mp";
}

IAlgorithm *ShtefanovichPlugin::algorithm()
{
    return new ShtefanovichAlgorithm(this);
}

QVariantMap ShtefanovichPlugin::theme()
{
    return QVariantMap{
        {"textColor", QColor("#5EC6F7").darker(200) },
        {"lightBackgroundColor", QColor("#5EC6F7").darker(120)},
        {"darkBackgroundColor", "#5EC6F7"},
        {"buttonColor", "#37A0F5"},
        {"darkBackgroundColor2", QColor("#48B3F7").lighter(130)},
        {"highlightColor", "black"}
    };
}

QIcon ShtefanovichPlugin::appIcon()
{
    return QIcon(":/code93.png");
}

