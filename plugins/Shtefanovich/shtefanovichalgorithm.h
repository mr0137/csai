#ifndef HNATENKOALGORITHM_H
#define HNATENKOALGORITHM_H

#include <ialgorithm.h>
#include <QObject>
#include <shtefanovich_global.h>
#include <encoder/encodezint.h>

#include <QZXing/QZXing.h>

class SHTEFANOVICH_EXPORT ShtefanovichAlgorithm : public IAlgorithm
{
public:
    ShtefanovichAlgorithm(QObject *parent = nullptr) : IAlgorithm(parent) {
        m_zxing = new QZXing(QZXing::DecoderFormat_CODE_93);
        connect(m_zxing, SIGNAL(	decodingStarted() ), this,SIGNAL(decodingStarted()));
        connect(m_zxing, SIGNAL(	decodingFinished(bool) ), this,SIGNAL(decodingFinished(bool)));
        connect(m_zxing, &QZXing::tagFound, this, [this](QString str){
            qDebug() << "Tag" << str;
           // str.remove(0,1);
           // str.remove(5,1);
            emit this->tagFound(str);
        });
    }

    // IAlgorithm interface
public:
    virtual QImage generate(QString code, double width, double height, QColor color = "green") override;
    virtual void decode(const QImage &img) override;
private:
    QZXing *m_zxing = nullptr;
};

#endif // HNATENKOALGORITHM_H
