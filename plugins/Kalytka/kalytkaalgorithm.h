#ifndef HNATENKOALGORITHM_H
#define HNATENKOALGORITHM_H

#include <ialgorithm.h>
#include <QObject>
#include <kalytka_global.h>
#include <encoder/encodezint.h>

#include <QZXing/QZXing.h>

class KALYTKA_EXPORT KalytkaAlgorithm : public IAlgorithm
{
public:
    KalytkaAlgorithm(QObject *parent = nullptr) : IAlgorithm(parent) {
        m_zxing = new QZXing(QZXing::DecoderFormat_UPC_E);
        connect(m_zxing, SIGNAL(	decodingStarted() ), this,SIGNAL(decodingStarted()));
        connect(m_zxing, SIGNAL(	decodingFinished(bool) ), this,SIGNAL(decodingFinished(bool)));
        connect(m_zxing, &QZXing::tagFound, this, [this](QString str){
            qDebug() << "Tag" << str;

            str.remove(7,1);
            str.remove(0,1);
            emit this->tagFound(str);
        });
    }

    // IAlgorithm interface
public:
    virtual QImage generate(QString code, double width, double height, QColor color = "green") override;
    virtual void decode(const QImage &img) override;
private:
    QZXing *m_zxing = nullptr;
};

#endif // HNATENKOALGORITHM_H
