#include "kalytka_plugin.h"

#include <QColor>
#include <kalytkaalgorithm.h>
#include <qqml.h>

QString KalytkaPlugin::titleName()
{
    return "Kalytka E. O. KV-02mp";
}

IAlgorithm *KalytkaPlugin::algorithm()
{
    return new KalytkaAlgorithm(this);
}

QVariantMap KalytkaPlugin::theme()
{
    return QVariantMap{
        {"textColor", QColor("#709F97").darker(200) },
        {"lightBackgroundColor", QColor("#73C2B9").darker(120)},
        {"darkBackgroundColor", "#709F97"},
        {"buttonColor", "#7CB6C1"},
        {"darkBackgroundColor2", QColor("#73C2B9").lighter(130)},
        {"highlightColor", "black"}
    };
}

QIcon KalytkaPlugin::appIcon()
{
    return QIcon(":/codeUPC-E.png");
}

