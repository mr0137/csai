#ifndef TEST_PLUGIN_H
#define TEST_PLUGIN_H

#include <QQmlExtensionPlugin>

class TestPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override;
};

#endif // TEST_PLUGIN_H
