#include "bloshinskiy_plugin.h"

#include <QColor>
#include <bloshinskiyalgorithm.h>
#include <qqml.h>

QString BloshinskiyPlugin::titleName()
{
    return "Bloshinskiy B. V. KV-01mn";
}

IAlgorithm *BloshinskiyPlugin::algorithm()
{
    return new BloshinskiyAlgorithm(this);
}

QVariantMap BloshinskiyPlugin::theme()
{
    return QVariantMap{
        {"textColor", "#162015"},
        {"lightBackgroundColor", "#425440"},
        {"darkBackgroundColor", "#495747"},
        {"buttonColor", "#365e2f"},
        {"darkBackgroundColor2", QColor("#495747").lighter(130)},
        {"highlightColor", "green"}
    };
}

QIcon BloshinskiyPlugin::appIcon()
{
    return QIcon(":/code128.png");
}

