#include "bloshinskiyalgorithm.h"
#include <QPainter>

QImage BloshinskiyAlgorithm::generate(QString code, double width, double height , QColor color)
{
    if (height > 700){
        qDebug() << "shrek";
         return QImage(":/shrek.jpg");
    }

    QImage img(width, height, QImage::Format_ARGB32);
    //img.fill(QColor(0,0,0,0));
    img.fill(QColor("white"));

    QPainter painter(&img);
    Zint::QZint bc;

    bc.setHeight(width);
    bc.setHeight(height);
    bc.setText(code);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setSymbol(BARCODE_CODE128);

    bc.render(painter, QRectF(5, 10, width - 10, height - 20));

    return img;
}

void BloshinskiyAlgorithm::decode(const QImage &img)
{
    m_zxing->decodeImage(img, img.width(), img.height(), true);
}
