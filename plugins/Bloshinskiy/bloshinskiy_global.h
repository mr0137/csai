#ifndef HNATENKO_GLOBAL_H
#define HNATENKO_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(BLOSHINSKIY_LIBRARY)
#  define BLOSHINSKIY_EXPORT Q_DECL_EXPORT
#else
#  define BLOSHINSKIY_EXPORT Q_DECL_IMPORT
#endif


#endif // HNATENKO_GLOBAL_H
