#ifndef DBPROVIDER_H
#define DBPROVIDER_H

#include <QSqlDatabase>

class DbProvider
{
public:
    DbProvider();
    static QSqlDatabase createConnection();
};


#endif // DBPROVIDER_H
