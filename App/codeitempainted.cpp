#include "codeitempainted.h"

#include <qpainter.h>

CodeItemPainted::CodeItemPainted()
{
    connect(this, &QQuickPaintedItem::heightChanged, this, [this](){
         bc.setHeight(height());
    });
    connect(this, &QQuickPaintedItem::widthChanged, this, [this](){
         bc.setHeight(width());
    });
    bc.setText("123456");
    bc.setBgColor("#232e3c");
    bc.setFgColor("#6ab2f2");
    bc.setSymbol(BARCODE_C25MATRIX);
}

void CodeItemPainted::paint(QPainter *painter)
{
    bc.render(*painter, boundingRect());
}
