#include "dbprovider.h"
#include <QDebug>
#include <QSqlError>

DbProvider::DbProvider()
{

}

QSqlDatabase DbProvider::createConnection()
{
    static int no;
    QSqlDatabase msqls = QSqlDatabase::addDatabase("QSQLITE", QString::number(no++));

    msqls.setDatabaseName("test.sqlite");

    if (!msqls.open()) {
        qWarning() << "MSqlS error: " << msqls.lastError();
    }

    return msqls;
}
