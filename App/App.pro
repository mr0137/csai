QT += quick
QT += core
QT += sql
QT += concurrent
QT += multimedia
QT += widgets
$#CONFIG += console
CONFIG += c++17

SOURCES += \
        appcore.cpp \
        cameracontroller.cpp \
        codeitem.cpp \
        dbprovider.cpp \
        itemsdataprovider.cpp \
        main.cpp

RESOURCES += resources/qml.qrc

QML_IMPORT_PATH = $$PWD/../bin/plugins
QML_IMPORT_NAME = App
uri = App

HEADERS += \
    appcore.h \
    cameracontroller.h \
    codeitem.h \
    dbprovider.h \
    itemsdataprovider.h

CONFIG(release, debug|release){
LIBS += -L$$PWD/../bin/libs -lklibcorelite
LIBS += -L$$PWD/../bin/libs -lCSAIBase
LIBS += -L$$PWD/../bin/libs -lLightTableBase
}else{

win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libs/klibcorelite/debug/ -lklibcorelite
!android: LIBS += -L$$OUT_PWD/../libs/klibcorelite/ -lklibcorelite

win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libs/CSAIBase/debug/ -lCSAIBase
!android: LIBS += -L$$OUT_PWD/../libs/CSAIBase/ -lCSAIBase

win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libs/LightTableBase/debug/ -lLightTableBase
!android: LIBS += -L$$OUT_PWD/../libs/LightTableBase/ -lLightTableBase
}

INCLUDEPATH += $$PWD/../libs/CSAIBase
DEPENDPATH += $$PWD/../libs/CSAIBase

INCLUDEPATH += $$PWD/../libs/klibcorelite
DEPENDPATH += $$PWD/../libs/klibcorelite

INCLUDEPATH += $$PWD/../libs/LightTableBase
DEPENDPATH += $$PWD/../libs/LightTableBase

android : {
QT += androidextras
#DISTFILES += \
#    android/AndroidManifest.xml \
#    android/build.gradle \
#    android/gradle.properties \
#    android/gradle/wrapper/gradle-wrapper.jar \
#    android/gradle/wrapper/gradle-wrapper.properties \
#    android/gradlew \
#    android/gradlew.bat \
#    android/res/values/libs.xml
#
#ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

LIBS += -L$$OUT_PWD/../libs/klibcorelite/ -lklibcorelite_armeabi-v7a
LIBS += -L$$OUT_PWD/../libs/CSAIBase/ -lCSAIBase_armeabi-v7a
LIBS += -L$$OUT_PWD/../libs/LightTableBase/ -lLightTableBase_armeabi-v7a
}

