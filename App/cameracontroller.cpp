#include "cameracontroller.h"
#include <QQuickItemGrabResult>
#include <ialgorithm.h>

CameraController::CameraController(QObject *parent) : QObject(parent)
{

}

void CameraController::setAlgorithm(IAlgorithm *algo)
{
    m_algorithm = algo;

    connect(m_algorithm, &IAlgorithm::decodingFinished, this,&CameraController::decodingFinished);
    connect(m_algorithm, &IAlgorithm::tagFound, this, [this](QString str){
         emit this->tagFound(str);
    });
    connect(m_algorithm, &IAlgorithm::error, this, &CameraController::errorMessage);
}


void CameraController::decodeQMLImage(QObject *imageObj)
{
    QQuickItemGrabResult *item = 0;
    item = qobject_cast<QQuickItemGrabResult*>(imageObj);

    if (m_algorithm == nullptr) return;
    QImage item_image(item->image());
    if (item_image.isNull())
        qDebug() << "invalid QML Image";
    else
        m_algorithm->decode(item_image);
}
