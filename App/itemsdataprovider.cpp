#include "itemsdataprovider.h"
#include <QSqlDatabase>
#include <QSqlError>
#include <QDebug>
#include <QSqlRecord>
#include <QSqlQuery>
#include <dbprovider.h>
#include <QSqlQuery>
#include <QtConcurrent>
#include <QSqlField>

ItemsDataProvider::ItemsDataProvider(QObject *parent) : ITableDataProvider(parent)
{
    reload();

    connect(this, &ItemsDataProvider::currentFilterChanged, this, [this](){
        if (currentFilter() == "All"){
            setCurrentTable("tableNames");
        }else{
            setCurrentTable(currentFilter());
        }
    });

    connect(this, &ItemsDataProvider::currentTableChanged, this, &ItemsDataProvider::reload);
}

int ItemsDataProvider::rowCount() const
{
    return m_rowCount;
}

int ItemsDataProvider::columnCount() const
{
    return m_columnCount;
}

QVector<QString> ItemsDataProvider::headerData() const
{
    return m_headers;
}

void ItemsDataProvider::prepareRow(int row)
{

}

QVector<QString> ItemsDataProvider::getRowData(int row)
{
    QVector<QString> res(columnCount());
    auto s = m_rowCache.find(row);
    if (s != m_rowCache.end()) {
        for (int i =0;i<res.size();++i) {
            res[i] = s->cells[i].displayText;
        }
    }
    return res;
}

Row ItemsDataProvider::getRow(int row)
{
    auto s = m_rowCache.find(row);
    return (s != m_rowCache.end()) ? *s : Row();
}

void ItemsDataProvider::sort(int column)
{

}

bool ItemsDataProvider::isFullLoaded()
{
    return m_loadAllData;
}

int ItemsDataProvider::rowById(int id)
{
    int res = -1;
    for (int i = 0; i < m_rowCache.size(); i++){
        if (m_rowCache[i].id == id){
            res = i;
            break;
        }
    }
    return res;

}

bool ItemsDataProvider::addType(QString name)
{
    if (types().contains(name)) return false;
    auto db = DbProvider::createConnection();
    QSqlQuery q(db);

    QString queryString = "INSERT INTO tableNames (name) VALUES (\"" + name +"\");";
    q.prepare(queryString);
    if (!q.exec()) {
        qDebug() << "\nError:" << q.lastError()
                 << "\nQuery:" << q.lastQuery();
        return false;
    }


    queryString = QString(
                "CREATE TABLE IF NOT EXISTS " + name + "(" + defaultColumnsQuery + ");");
    q.prepare(queryString);
    if (!q.exec()) {
        qDebug() << "\nError:" << q.lastError()
                 << "\nQuery:" << q.lastQuery();
        return false;
    }


    m_types.append(name);
    emit typesChanged(m_types);

    return true;
}

bool ItemsDataProvider::addProduct(QString name, QString manufacturer, QString type, int count)
{
    if (!manufacturers().contains(manufacturer) || !types().contains(type)) return false;
    auto db = DbProvider::createConnection();
    QSqlQuery q(db);

    QString queryString = "INSERT INTO " + type + " (ProductName, Manufacturer, Count) VALUES (\"" + name + "\"," + QString::number(m_manufacturerCode[manufacturer]) + " ," + QString::number(count) + " );";
    q.prepare(queryString);
    if (!q.exec()) {
        qDebug() << "\nError:" << q.lastError()
                 << "\nQuery:" << q.lastQuery();
        return false;
    }

    // queryString = "SELECT MAX(id) FROM ProductNames";
    // q.prepare(queryString);
    // if (!q.exec()) {
    //     qDebug() << "\nError:" << q.lastError()
    //              << "\nQuery:" << q.lastQuery();
    //     return false;
    // }

    // int code = q.value(0).toInt() + 100;

    queryString = "INSERT INTO ProductNames (name, code) VALUES (\"" + name + "\", (SELECT MAX(id) FROM ProductNames) + 100);";
    q.prepare(queryString);
    if (!q.exec()) {
        qDebug() << "\nError:" << q.lastError()
                 << "\nQuery:" << q.lastQuery();
        return false;
    }

    reload();
    return true;
}

bool ItemsDataProvider::addManufacturer(QString name)
{    if (m_productNameCode.contains(name)) return false;
    auto db = DbProvider::createConnection();
     QSqlQuery q(db);
      QString queryString;
       if (manufacturers().length() != 0){
           queryString = "INSERT INTO Manufacturers (name, code) VALUES (\"" + name + "\", (SELECT MAX(id) FROM Manufacturers) + 100);";

       }else{
           queryString = "INSERT INTO Manufacturers (name, code) VALUES (\"" + name + "\", 100);";
       }
        q.prepare(queryString);
         if (!q.exec()) {
             qDebug() << "\nError:" << q.lastError()
                      << "\nQuery:" << q.lastQuery();
             return false;
         }

         m_manufacturers.append(name);
          emit manufacturersChanged(m_types);
          reload();
           return true;
}

bool ItemsDataProvider::editCount(int count, int row)
{

}

bool ItemsDataProvider::removeProduct(int row)
{
    if (row >= 0 && row < m_rowCache.count()){

        auto productName = getRowData(row)[0];

        if (m_productNameCode.contains(productName)){
            auto db = DbProvider::createConnection();
            QSqlQuery q(db);

            QVariantList t = m_types;
            t.removeAll("All");

            QVariantList names;

            for (const auto &type : t){
                QString queryString = QString("DELETE FROM " + type.toString() + " WHERE ProductName = \"" + productName + "\";");
                q.prepare(queryString);
                if (!q.exec()) {
                    qDebug() << "\nError:" << q.lastError()
                             << "\nQuery:" << q.lastQuery();
                    return false;
                }
            }

            QString queryString = QString("DELETE FROM ProductNames WHERE name = \"" + productName + "\";");
            q.prepare(queryString);
            if (!q.exec()) {
                qDebug() << "\nError:" << q.lastError()
                         << "\nQuery:" << q.lastQuery();
                return false;
            }


            setCurrentFilter("All");
            reload();
            return true;
        }
    }
    return false;
}

bool ItemsDataProvider::removeType(QString name)
{
    qDebug() << m_types << name;
    if (m_types.contains(name)){
        setCurrentFilter("All");
        auto db = DbProvider::createConnection();
        QSqlQuery q(db);

        QString queryString = QString("DROP TABLE IF EXISTS " + name + ";");
        q.prepare(queryString);
        if (!q.exec()) {
            qDebug() << "\nError:" << q.lastError()
                     << "\nQuery:" << q.lastQuery();
            return false;
        }

        queryString = QString("DELETE FROM tableNames WHERE name = \"" + name + "\";");
        q.prepare(queryString);
        if (!q.exec()) {
            qDebug() << "\nError:" << q.lastError()
                     << "\nQuery:" << q.lastQuery();
            return false;
        }
        reload();
        return true;
    }
    return false;
}

bool ItemsDataProvider::removeManufacturer(QString name)
{
    if (m_manufacturers.contains(name)){
        auto db = DbProvider::createConnection();
        QSqlQuery q(db);

        if (!m_manufacturerCode.contains(name)){
            return false;
        }
        int id = m_manufacturerCode[name];

        QVariantList t = m_types;
        t.removeAll("All");

        QVariantList names;

        for (const auto &type : t){
            QString queryString = QString("SELECT ProductName FROM " + type.toString() + " WHERE Manufacturer = " + QString::number(id) + ";");
            q.prepare(queryString);
            if (!q.exec()) {
                qDebug() << "\nError:" << q.lastError()
                         << "\nQuery:" << q.lastQuery();
                return false;
            }

            while (m_threadCouldRun && q.next()) {
                Row r;
                auto rec = q.record();
                names.push_back(rec.value(0));
            }
        }

        for (const auto &type : t){
            QString queryString = QString("DELETE FROM " + type.toString() + " WHERE Manufacturer = " + QString::number(id) + ";");
            q.prepare(queryString);
            if (!q.exec()) {
                qDebug() << "\nError:" << q.lastError()
                         << "\nQuery:" << q.lastQuery();
                return false;
            }
        }

        for (const auto &name : names){
            QString queryString = QString("DELETE FROM ProductNames WHERE name = \"" + name.toString() + "\";");
            q.prepare(queryString);
            if (!q.exec()) {
                qDebug() << "\nError:" << q.lastError()
                         << "\nQuery:" << q.lastQuery();
                return false;
            }
        }

        QString queryString = QString("DELETE FROM Manufacturers WHERE name = \"" + name + "\";");
        q.prepare(queryString);
        if (!q.exec()) {
            qDebug() << "\nError:" << q.lastError()
                     << "\nQuery:" << q.lastQuery();
            return false;
        }

        setCurrentFilter("All");
        reload();
        return true;
    }
    return false;
}

QString ItemsDataProvider::generateCode(int row)
{
    auto r = getRow(row);
    if (r.id == -1) return "";
    QString code;
    if (m_productNameCode.contains(r.cells[0].displayText)){
        code += QString::number(m_productNameCode[r.cells[0].displayText]);
    }

    while (code.length() < r.cells[1].displayText.length()){
        code.push_front("0");
    }
    QString rCode = r.cells[1].displayText;
    while (code.length() > rCode.length()){
        rCode.push_front("0");
    }

    code += rCode;
    return code;
}

int ItemsDataProvider::getRowByCode(QString code)
{
    int result = -1;
    int mid = code.length() / 2;
    if (code.length() == 6){
        QString man = code.mid(mid,mid);
        QString name = code.mid(0,mid);

        for (int i = 0; i < m_rowCache.count(); i++){
            auto r = getRowData(i);

            if (r.contains(QString::number(man.toInt())) && r.contains(m_productNameCode.key(name.toInt()))){
                return i;
            }
        }
    }
    return result;
}

QString ItemsDataProvider::getManufacturerCode(QString manufacturer)
{
    auto db = DbProvider::createConnection();
    QSqlQuery q(db);

    QString queryString = QString("SELECT code FROM Manufacturers WHERE name = \"" + manufacturer + "\";");
    q.prepare(queryString);
    if (!q.exec()) {
        qDebug() << "\nError:" << q.lastError()
                 << "\nQuery:" << q.lastQuery();
        return "";
    }

    return q.value(0).toString();
}

QString ItemsDataProvider::getProductNameCode(QString productName)
{
    auto db = DbProvider::createConnection();
    QSqlQuery q(db);

    QString queryString = QString("SELECT code FROM ProductNames WHERE name = " + productName + ";");
    q.prepare(queryString);
    if (!q.exec()) {
        qDebug() << "\nError:" << q.lastError()
                 << "\nQuery:" << q.lastQuery();
        return "";
    }

    return q.value(0).toString();
}

void ItemsDataProvider::reload()
{
    if (m_loadFuture.isRunning()) {
        m_loadFuture.cancel();
        m_threadCouldRun = false;
        m_loadFuture.waitForFinished();
        m_threadCouldRun = true;
    }

    m_loadFuture = QtConcurrent::run([this](){
        m_rowCache.clear();
        m_loadAllData = false;
        m_types.clear();
        m_manufacturers.clear();
        m_manufacturerCode.clear();
        m_productNameCode.clear();

        auto db = DbProvider::createConnection();
        QSqlQuery q(db);
        QVariantList types;

        QString queryString = QString(
                    "CREATE TABLE IF NOT EXISTS Manufacturers ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    "name text,"
                    "code INTEGER"
                    ");");

        q.prepare(queryString);

        if (!q.exec()) {
            qDebug() << "\nError:" << q.lastError()
                     << "\nQuery:" << q.lastQuery();
            return;
        }

        queryString = "SELECT name, code FROM Manufacturers;";
        q.prepare(queryString);
        if (!q.exec()) {
            qDebug() << "\nError:" << q.lastError()
                     << "\nQuery:" << q.lastQuery();
            return;
        }

        while (m_threadCouldRun && q.next()) {
            Row r;
            auto rec = q.record();
            m_manufacturers.push_back(rec.value(0).toString());
            m_manufacturerCode.insert(rec.value(0).toString(), rec.value(1).toInt());
        }


        queryString = QString(
                    "CREATE TABLE IF NOT EXISTS ProductNames ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    "name text,"
                    "code INTEGER"
                    ");");

        q.prepare(queryString);

        if (!q.exec()) {
            qDebug() << "\nError:" << q.lastError()
                     << "\nQuery:" << q.lastQuery();
            return;
        }

        queryString = "SELECT name, code FROM ProductNames;";
        q.prepare(queryString);
        if (!q.exec()) {
            qDebug() << "\nError:" << q.lastError()
                     << "\nQuery:" << q.lastQuery();
            return;
        }


        while (m_threadCouldRun && q.next()) {
            Row r;
            auto rec = q.record();
            m_productNameCode.insert(rec.value(0).toString(), rec.value(1).toInt());
        }

        queryString = QString(
                    "CREATE TABLE IF NOT EXISTS tableNames("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    "name text"
                    ");");

        q.prepare(queryString);

        if (!q.exec()) {
            qDebug() << "\nError:" << q.lastError()
                     << "\nQuery:" << q.lastQuery();
            return;
        }


        queryString = "SELECT * FROM tableNames;";
        q.prepare(queryString);
        if (!q.exec()) {
            qDebug() << "\nError:" << q.lastError()
                     << "\nQuery:" << q.lastQuery();
            return;
        }
        //setup headers
        //auto rec = q.record();
        //m_columnCount = q.record().count();
        //m_headers.resize(m_columnCount);
        //for (int  i =0;i <m_columnCount; ++i) {
        //    m_headers[i] = rec.field(i).name();
        //}



        m_columnCount = 3;
        m_headers.resize(m_columnCount);
        m_headers[0] = "ProductName";
        m_headers[1] = "Manufacturer ID";
        m_headers[2] = "Count";

        m_rowCount = 0;
        while (m_threadCouldRun && q.next()) {
            Row r;
            auto rec = q.record();
            types.push_back(rec.value(1).toString());
        }
        emit reseted();

        qDebug() << currentTable();
        if (currentTable() == "tableNames" || currentTable() == "All"){

            int loadedRows = 0;
            for (const auto &varType : types){
                //load data

                queryString = "SELECT * FROM " + varType.toString() + ";";
                q.prepare(queryString);
                if (!q.exec()) {
                    qDebug() << "\nError:" << q.lastError()
                             << "\nQuery:" << q.lastQuery();
                    return;
                }

                int step = 10;
                while (m_threadCouldRun && q.next()) {
                    Row r;
                    auto rec = q.record();
                    r.id = rec.value(0).toInt();
                    for (int  i = 1; i < columnCount() + 1; ++i) {
                        auto cell = Cell{
                                .textColor = Qt::black,
                                .backgroundColor = Qt::white,
                                .displayText = rec.value(i).toString()};

                        //cell.flags.setFlag(Cell::OWN_BGCOLOR);
                        //cell.flags.setFlag(Cell::OWN_TXTCOLOR);
                        r.cells << cell;
                    }

                    m_rowCache[loadedRows++] = r;

                    if (loadedRows%step == 0) {
                        // qDebug() << "need to update" << loadedRows - step << loadedRows - 1;
                        m_rowCount = m_rowCache.count();
                        emit rowsUpdated(loadedRows - step, loadedRows - 1);
                        QThread::msleep(100);
                    }
                }


                if (m_threadCouldRun){
                    //qDebug() << "need to update" << loadedRows - loadedRows%step << loadedRows - 1;
                    m_rowCount = m_rowCache.count();
                    emit rowsUpdated(loadedRows - loadedRows%step, loadedRows - 1);


                }else{
                    return;
                }
            }
        }else{
            int loadedRows = 0;

            int step = 10;
            queryString = "SELECT * FROM " + currentTable() + ";";
            q.prepare(queryString);
            if (!q.exec()) {
                qDebug() << "\nError:" << q.lastError()
                         << "\nQuery:" << q.lastQuery();
                return;
            }

            while (m_threadCouldRun && q.next()) {
                Row r;
                auto rec = q.record();
                r.id = rec.value(0).toInt();
                for (int  i = 1; i < columnCount() + 1; ++i) {
                    auto cell = Cell{
                            .textColor = Qt::black,
                            .backgroundColor = Qt::white,
                            .displayText = rec.value(i).toString()};

                    //cell.flags.setFlag(Cell::OWN_BGCOLOR);
                    //cell.flags.setFlag(Cell::OWN_TXTCOLOR);
                    r.cells << cell;
                }

                m_rowCache[loadedRows++] = r;

                if (loadedRows%step == 0) {
                    // qDebug() << "need to update" << loadedRows - step << loadedRows - 1;
                    m_rowCount = m_rowCache.count();
                    emit rowsUpdated(loadedRows - step, loadedRows - 1);
                    QThread::msleep(100);
                }
            }


            if (m_threadCouldRun){
                //qDebug() << "need to update" << loadedRows - loadedRows%step << loadedRows - 1;
                m_rowCount = m_rowCache.count();
                emit rowsUpdated(loadedRows - loadedRows%step, loadedRows - 1);


            }else{
                return;
            }
        }
        m_loadAllData = true;
        db.close();

        types.prepend("All");
        setTypes(types);
        emit fullLoaded();
        qDebug() << m_manufacturers << m_rowCache.size();
        emit manufacturersChanged(m_manufacturers);
    });

}

void ItemsDataProvider::setCellData(int row, int column, QVariant value)
{
    if (row < rowCount() && row >= 0 &&
            column < m_columnCount && column >= 0){

        m_rowCache[row].cells[column].displayText = value.toString();
        QString productName =  m_rowCache[row].cells[0].displayText;

        auto db = DbProvider::createConnection();
        QSqlQuery q(db);
        QVariantList t = m_types;
        t.removeAll("All");

        QVariantList names;

        for (const auto &type : t){
            QString queryString = QString("UPDATE " + type.toString() + " SET Count=" + value.toString() + " WHERE ProductName= \"" + productName + "\";");
            q.prepare(queryString);
            if (!q.exec()) {
                qDebug() << "\nError:" << q.lastError()
                         << "\nQuery:" << q.lastQuery();
                return;
            }
        }

        emit rowsUpdated(row, row);
    }

}

bool ItemsDataProvider::isEditable()
{
    return true;
}
