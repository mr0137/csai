#ifndef APPCORE_H
#define APPCORE_H

#include <QObject>
#include <ialgorithm.h>
#include <kmacro.h>
#include <itemsdataprovider.h>
#include <cameracontroller.h>

class CSAIBase;
class AppCore : public QObject
{
    Q_OBJECT
    K_SINGLETON(AppCore)
    K_QML_SINGLETON(AppCore)
    K_READONLY_PROPERTY(IAlgorithm*, algorithm, algorithm, setAlgorithm, algorithmChanged, nullptr)
    K_READONLY_PROPERTY(QString, appTitle, appTitle, setAppTitle, appTitleChanged, "")
    K_READONLY_PROPERTY(QVariantMap, theme, theme, setTheme, themeChanged, QVariantMap())
    K_CONST_PROPERTY(ItemsDataProvider*, provider, new ItemsDataProvider(this))
    K_CONST_PROPERTY(CameraController*, controller, new CameraController(this))
public:
    explicit AppCore(QObject *parent = nullptr);
    void proceedConfig(CSAIBase *algo);

signals:
public slots:
    void saveCode(QString code, QString path);
private:

};

#endif // APPCORE_H
