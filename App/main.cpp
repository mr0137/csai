#include <QApplication>
#include <QErrorMessage>
#include <QGuiApplication>
#include <QIcon>
#include <QPluginLoader>
#include <QQmlApplicationEngine>
#include <QQmlEngine>
#include <appcore.h>
#include <csaibase.h>

void loadConfigPlugin(QString path){
    QPluginLoader loader(path);
    loader.load();
    if (loader.isLoaded()){
        auto st = qobject_cast<CSAIBase*>(loader.instance());
        if (st){
            qApp->setWindowIcon(st->appIcon());
            AppCore::instance()->proceedConfig(st);
        }
    }else{
        qDebug() << "error" << loader.errorString() + " | " + path;
    }
}

QString progPath(QString appPath){
    //! prog dir path only
    QString progDir = appPath;

#ifdef QT_NO_DEBUG
#ifdef Q_OS_LINUX
    int times = 2;
#endif
#ifdef Q_OS_WINDOWS
    int times = 1;
#endif
#endif

#ifdef QT_DEBUG
#ifdef Q_OS_LINUX
    int times = 3;
#endif
#ifdef Q_OS_WINDOWS
    int times = 4;
#endif
#endif

    while(times > 0 && !progDir.isEmpty()){
#ifdef Q_OS_LINUX
        if (progDir.endsWith("/")) times--;
#endif
#ifdef Q_OS_WINDOWS
        if (progDir.endsWith("\\")) times--;
#endif
        progDir.chop(1);
    }
    return progDir;
}

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);
    QString path = progPath(argv[0]);
    QQmlApplicationEngine engine;
    app.setOrganizationName("somename");
    app.setOrganizationDomain("somename");
    qmlRegisterType<QObject>("KLib", 1, 0, "");
#ifdef QT_NO_DEBUG
#ifdef Q_OS_WINDOWS
    engine.addImportPath(path);
    engine.addImportPath(path + "\\plugins");
    loadConfigPlugin(path + "\\Algorithm.dll");
#else
    engine.addImportPath(path + "/bin/plugins");
    loadConfigPlugin(path +  "/bin/plugins/Algorithm/libAlgorithm.so");
#endif
#endif
#ifdef QT_DEBUG
#ifdef Q_OS_WINDOWS
    engine.addImportPath(path + "\\bin\\plugins");
    loadConfigPlugin(path +  "\\bin\\plugins\\Algorithm\\Algorithm.dll");
#else
    engine.addImportPath(path + "/bin/plugins");
    loadConfigPlugin(path +  "/bin/plugins/Algorithm/libAlgorithm.so");
#endif
#endif
    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
