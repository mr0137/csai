import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import KLib 1.0
import CQML 1.0 as C

C.BaseBackground {
    id: root
    signal close()
    backgroundColor: AppCore.theme.darkBackgroundColor
    elevation: 5
    radius: 10
    clipContent: true

    C.BaseBackground {
        anchors.fill: parent
        elevation: 4
        internalShadow: true
        backgroundColor: AppCore.theme.darkBackgroundColor
        anchors.margins: 3
        radius: 5
        clipContent: true

        ColumnLayout{
            anchors.fill: parent

            Item{
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.maximumHeight: 40
                Layout.minimumHeight: 40

                C.BaseBackground{
                    anchors.fill: parent
                    backgroundColor: AppCore.theme.darkBackgroundColor
                    anchors.topMargin: 35
                    internalShadow: true
                    elevation: 3
                }

                Text{
                    anchors.fill: parent
                    text: "REMOVE TYPE"
                    font.pointSize: 11
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: AppCore.theme.textColor
                }
            }


            Text{
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.maximumHeight: 30
                Layout.leftMargin: 5
                text: "Type"
                verticalAlignment: Text.AlignVCenter
                color: AppCore.theme.textColor
            }

            C.ComboBox{
                id: comboboxTypes
                Layout.leftMargin: 5
                Layout.rightMargin: 5
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.maximumHeight: 40
                model: {
                    var array = AppCore.provider.types
                    array.shift();
                    return array;
                }

                textColor: AppCore.theme.textColor
                backgroundColor: AppCore.theme.lightBackgroundColor
                internalShadow: true
                elevation: 3
            }

            Item{
                Layout.fillHeight: true
            }


            C.Button{
                text: "CONFIRM"
                buttonRadius: 5
                elevation: 4
                Layout.fillWidth: true
                Layout.leftMargin: 10
                Layout.rightMargin: 10
                Layout.preferredHeight: 30
                buttonBackground: AppCore.theme.buttonColor
                textColor: AppCore.theme.textColor

                onReleased: {
                    if (comboboxTypes.count == 0){
                        root.close()
                        return
                    }

                    AppCore.provider.removeType(comboboxTypes.displayText)
                    root.close()

                }
            }

            C.Button{
                text: "CANCEL"
                buttonRadius: 5
                elevation: 4
                Layout.fillWidth: true
                Layout.leftMargin: 10
                Layout.rightMargin: 10
                Layout.preferredHeight: 30
                buttonBackground: AppCore.theme.buttonColor
                textColor: "#ef5959"

                onReleased: {
                    root.close()
                }
            }

            Item{
                Layout.preferredHeight: 5
            }
        }
    }
}
