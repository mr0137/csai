import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import KLib 1.0
import CQML 1.0 as C

C.BaseBackground {
    id: root
    signal close()
    backgroundColor: AppCore.theme.darkBackgroundColor
    elevation: 5
    radius: 10
    clipContent: true


    C.BaseBackground {
        anchors.fill: parent
        elevation: 4
        internalShadow: true
        backgroundColor: AppCore.theme.darkBackgroundColor
        anchors.margins: 3
        radius: 5
        clipContent: true


        ColumnLayout{
            anchors.fill: parent

            Item{
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.maximumHeight: 40
                Layout.minimumHeight: 40

                C.BaseBackground{
                    anchors.fill: parent
                    backgroundColor: AppCore.theme.darkBackgroundColor
                    anchors.topMargin: 35
                    internalShadow: true
                    elevation: 3
                }

                Text{
                    anchors.fill: parent
                    text: "ADD MANUFACTURER"
                    font.pointSize: 11
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: AppCore.theme.textColor
                }
            }

            C.TextField{
                id: textField
                Layout.fillWidth: true
                Layout.leftMargin: 10
                Layout.rightMargin: 10
                Layout.preferredHeight: 40
                Layout.topMargin: 10
                placeholderText: ""
                color: AppCore.theme.textColor
                hintColor: AppCore.theme.textColor
                floatingLabel: true
                floatingText: "Manufacturer name"
                placeholderTextColor: AppCore.theme.textColor

            }

            Item{
                Layout.fillHeight: true
            }

            C.Button{
                text: "CONFIRM"
                buttonRadius: 5
                elevation: 4
                Layout.fillWidth: true
                Layout.leftMargin: 10
                Layout.rightMargin: 10
                Layout.preferredHeight: 30
                buttonBackground: AppCore.theme.buttonColor
                textColor: AppCore.theme.textColor

                onReleased: {
                    if (AppCore.provider.addManufacturer(textField.text)){
                        root.close()
                    }else{
                        textField.helperText = "Error, type already exist"
                        textField.hasError = true
                    }

                }
            }

            C.Button{
                text: "CANCEL"
                buttonRadius: 5
                elevation: 4
                Layout.fillWidth: true
                Layout.leftMargin: 10
                Layout.rightMargin: 10
                Layout.preferredHeight: 30
                buttonBackground: AppCore.theme.buttonColor
                textColor: "#ef5959"

                onReleased: {
                    root.close()
                }
            }

            Item{
                Layout.preferredHeight: 5
            }
        }
    }
}
