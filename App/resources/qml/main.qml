import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtMultimedia 5.15
import QtQuick.Shapes 1.15
import KLib 1.0
import CQML 1.0 as C
import LightTable 1.0
import Qt.labs.platform 1.1

Window {
    id: window
    width: 1366
    height: 768
    minimumWidth: 640
    minimumHeight: 480
    visible: true
    title: qsTr("CSAI ") + AppCore.appTitle

    Connections{
        target: AppCore.provider
        function onTypesChanged(){
            console.log(AppCore.provider.types)
        }
    }

    property Component addType: AddType {}
    property Component addProduct: AddProduct {}
    property Component addManufacturer: AddManufacturer {}

    property Component removeType: RemoveType {}
    property Component removeProduct: RemoveProduct {}
    property Component removeManufacturer: RemoveManufacturer {}

    SplitView{
        id: mainRowLayout
        anchors.fill: parent
        orientation: "Horizontal"
        spacing: 0

        handle: C.BaseBackground{
            backgroundColor: AppCore.theme.darkBackgroundColor
            internalShadow: true
            elevation: 2
            implicitWidth: 5
            height: mainRowLayout.height
        }

        C.BaseBackground{
            SplitView.fillHeight: true
            SplitView.minimumWidth: 100
            SplitView.preferredWidth: 250
            Layout.maximumWidth: parent.width * 0.2
            backgroundColor: AppCore.theme.darkBackgroundColor
            internalShadow: false
            elevation: 0

            Item{
                anchors{
                    left: parent.left
                    right: parent.right
                    top: parent.top
                }
                height: 40

                C.BaseBackground{
                    anchors.fill: parent
                    backgroundColor: AppCore.theme.darkBackgroundColor
                    anchors.topMargin: 35
                    internalShadow: true
                    elevation: 3
                }

                Text{
                    anchors.fill: parent
                    text: "TYPES"
                    font.pointSize: 11
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: AppCore.theme.textColor
                }
            }

            ScrollView {
                clip: true
                anchors.fill: parent
                anchors.topMargin: 45
                ScrollBar.horizontal.interactive: false
                ScrollBar.vertical.interactive: true
                ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                ListView {
                    id: listView
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.margins: 3
                    model: AppCore.provider.types
                    spacing: 3
                    delegate: C.BaseBackground{
                        width: listView.width
                        height: 40
                        backgroundColor: modelData === AppCore.provider.currentFilter ? AppCore.theme.darkBackgroundColor2 : AppCore.theme.darkBackgroundColor
                        internalShadow: true
                        elevation: modelData === AppCore.provider.currentFilter ? 4 : 3
                        Text {
                            anchors.fill: parent
                            text: modelData
                            font.pointSize: 10
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            color: AppCore.theme.textColor
                        }
                        C.WavedMouseArea{
                            anchors.fill: parent
                            onReleased: {
                                AppCore.provider.currentFilter = modelData
                            }
                        }
                    }
                }
                Item{
                    Layout.fillHeight: true
                }
            }
        }

        SplitView{
            SplitView.fillHeight: true
            SplitView.fillWidth: true
            SplitView.minimumWidth: 200
            SplitView.preferredWidth: 550
            orientation: "Vertical"
            spacing: 0

            handle: C.BaseBackground{
                backgroundColor: AppCore.theme.darkBackgroundColor
                internalShadow: true
                elevation: 2
                implicitHeight: 5
                width: table.width
            }

            LazyTable{
                id: table
                SplitView.fillHeight: true
                SplitView.fillWidth: true
                provider: AppCore.provider
                backgroundColor: AppCore.theme.darkBackgroundColor2
                editableColumns: ["Count"]

                style: TableStyle{
                    verticalHeaderAvailable: true
                    iconsColor: AppCore.theme.darkBackgroundColor
                    iconsWidth: 1
                    highlightColor: AppCore.theme.textColor
                    selectionColor: "black"
                    selectionOpacity: 0.3
                    cell{
                        height: 40
                        backgroundColor: AppCore.theme.darkBackgroundColor2
                        textColor: AppCore.theme.textColor
                        font{
                            pointSize: 17
                            family: "Arial"
                            letterSpacing: 0.1
                            wordSpacing: 0.1
                        }
                    }
                    horizontalHeader{
                        height: 40
                        width: 240
                        backgroundColor: AppCore.theme.lightBackgroundColor
                        textColor: AppCore.theme.textColor
                        font{
                            pointSize: 19
                            family: "Arial"
                            letterSpacing: 0.1
                            wordSpacing: 0.1
                        }
                    }
                    verticalHeader{
                        width: 10
                        backgroundColor: AppCore.theme.darkBackgroundColor
                    }
                }
                onCurrentRowChanged: {
                    codeItem.code = (table.currentRow === -1 ? "1010101" : AppCore.provider.generateCode(table.currentRow))
                }

            }

            C.BaseBackground{
                SplitView.fillHeight: true
                SplitView.preferredHeight: 100
                SplitView.fillWidth: true
                backgroundColor: AppCore.theme.darkBackgroundColor
                internalShadow: true
                elevation: 0

                RowLayout{
                    spacing: 5
                    anchors.fill: parent
                    anchors.margins: 5


                    ColumnLayout{
                        Layout.preferredWidth: 170
                        Layout.preferredHeight: 65
                        C.Button{
                            text: "ADD TYPE"
                            buttonRadius: 5
                            elevation: 4
                            Layout.preferredWidth: 170
                            Layout.preferredHeight: 30
                            buttonBackground: AppCore.theme.buttonColor
                            textColor: AppCore.theme.textColor

                            onReleased: {
                                contextPopup.openPopup(window.width / 2 - 150, window.height / 2 - 250, 300, 500, true, addType)
                            }
                        }

                        C.Button{
                            text: "REMOVE TYPE"
                            buttonRadius: 5
                            elevation: 4
                            Layout.preferredWidth: 170
                            Layout.preferredHeight: 30
                            buttonBackground: AppCore.theme.buttonColor
                            textColor: AppCore.theme.textColor

                            onReleased: {
                                contextPopup.openPopup(window.width / 2 - 150, window.height / 2 - 250, 300, 500, true, removeType)
                            }
                        }
                    }

                    ColumnLayout{
                        Layout.preferredWidth: 170
                        Layout.preferredHeight: 65

                        C.Button{
                            text: "ADD PRODUCT"
                            buttonRadius: 5
                            elevation: 4
                            Layout.preferredWidth: 170
                            Layout.preferredHeight: 30
                            buttonBackground: AppCore.theme.buttonColor
                            textColor: AppCore.theme.textColor
                            onReleased: {
                                contextPopup.openPopup(window.width / 2 - 150, window.height / 2 - 250, 300, 500, true, addProduct)
                            }
                        }

                        C.Button{
                            text: "REMOVE PRODUCT"
                            buttonRadius: 5
                            elevation: 4
                            Layout.preferredWidth: 170
                            Layout.preferredHeight: 30
                            buttonBackground: AppCore.theme.buttonColor
                            textColor: AppCore.theme.textColor

                            onReleased: {
                                AppCore.provider.removeProduct(table.currentRow)
                            }
                        }
                    }


                    ColumnLayout{
                        Layout.preferredWidth: 210
                        Layout.preferredHeight: 65

                        C.Button{
                            text: "ADD MANUFACTURER"
                            buttonRadius: 5
                            elevation: 4
                            Layout.preferredWidth: 210
                            Layout.preferredHeight: 30
                            buttonBackground: AppCore.theme.buttonColor
                            textColor: AppCore.theme.textColor

                            onReleased: {
                                contextPopup.openPopup(window.width / 2 - 150, window.height / 2 - 250, 300, 500, true, addManufacturer)
                            }
                        }

                        C.Button{
                            text: "REMOVE MANUFACTURER"
                            buttonRadius: 5
                            elevation: 4
                            Layout.preferredWidth: 210
                            Layout.preferredHeight: 30
                            buttonBackground: AppCore.theme.buttonColor
                            textColor: AppCore.theme.textColor

                            onReleased: {
                                contextPopup.openPopup(window.width / 2 - 150, window.height / 2 - 250, 300, 500, true, removeManufacturer)
                            }
                        }
                    }

                    ColumnLayout{
                        Layout.preferredWidth: 170
                        Layout.preferredHeight: 65

                        C.Button{
                            text: "SCAN PRODUCT"
                            buttonRadius: 5
                            elevation: 4
                            Layout.preferredWidth: 170
                            Layout.preferredHeight: 30
                            buttonBackground: AppCore.theme.buttonColor
                            textColor: AppCore.theme.textColor

                            onReleased: {
                                scanWindow.visible = true
                            }
                        }
                        C.Button{
                            text: "SAVE CODE"
                            buttonRadius: 5
                            elevation: 4
                            Layout.preferredWidth: 170
                            Layout.preferredHeight: 30
                            buttonBackground: AppCore.theme.buttonColor
                            textColor: AppCore.theme.textColor

                            onReleased: {
                                saveDialog.open()
                            }
                        }
                    }


                    Item{
                        Layout.fillHeight: true
                        Layout.minimumWidth: 200
                        // Layout.maximumWidth: 200
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter


                        CodeItem{
                            id: codeItem
                            anchors.right: parent.right
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                            width: height
                            color: AppCore.theme.textColor

                            onHeightChanged: {
                                if (height > 700){
                                    audio.play()
                                }else{
                                    audio.pause()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    ContextPopup{
        id: contextPopup
        source: mainRowLayout
    }

    FileDialog {
        id: saveDialog
        title: "Save Dialog"
        folder: "file://" //Here you can set your default folder
        //@disable-check M16
        currentFile: "file://code.png" //The name of the item that you want to save
        //@disable-check M16
        fileMode: FileDialog.SaveFile
        //@disable-check M16
        options: FileDialog.DontConfirmOverwrite
        defaultSuffix: ".png"
        nameFilters: ["Images (*.png)"]

        onAccepted: {
            AppCore.saveCode(codeItem.code, saveDialog.file)
        }
    }

    Audio{
        id: audio
        volume: 1
        loops: Audio.Infinite
        source: "qrc:/Somebody.mp3"
        muted: AppCore.appTitle !== "Bloshinskiy B. V. KV-01mn"
    }

    ScanWindow{
        id: scanWindow
        visible: false

        onScannedText: {
            var ind = AppCore.provider.getRowByCode(text)
            if (ind > -1){
                table.setCurrentRow(ind)
            }
        }
    }
}
