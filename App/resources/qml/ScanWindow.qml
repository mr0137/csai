import QtQuick 2.15
import QtQuick.Window 2.15
import QtMultimedia 5.5
import QtQuick.Controls 2.15
import QtQuick.Controls 1.4 as Old
import QtQuick.Controls.Private 1.0
import Qt.labs.settings 1.0
import KLib 1.0

Window {
    id:mainWnd
    visible: true
    width:viewfinder.width
    height:viewfinder.height
    title: "Scanner"
    signal scannedText(string text)

    Settings{
        //This is to retrieve the last window position and size for the next startup
        property alias x : mainWnd.x
        property alias y : mainWnd.y
        property alias width : mainWnd.width
        property alias height : mainWnd.height
    }


    Timer{
        interval:1000
        triggeredOnStart: false
        onTriggered: {
            if (mainWnd.visible){
                viewfinder.grabToImage(function (result) {
                    AppCore.controller.decodeQMLImage(result);
                });
            }
        }

        running: true//mainWnd.visibile//overlay.decodeImage
        repeat:true
    }

    Connections{
        target: AppCore.controller
        //onDecodingStarted: console.debug("start qrdecode")
        function onTagFound(idScanned) {
            console.log("done qrdecode : good")
            qrCodeFound.text = idScanned;
            qrCodeFound.color = "green";
            scannedText(idScanned)
            mainWnd.close()
        }

        function onErrorMessage(message) {
            //console.debug("done qrdecode : error")
            qrCodeFound.text = message;
            qrCodeFound.color = "red";
        }

        //onDecodingFinished: console.debug("done qrdecode")
    }

    Camera{
        //our Camera to play with
        id:camera
        imageCapture {
            onImageCaptured: {
                previewImage.source = preview;
                previewImage.visible = true;
            }
            onImageSaved: console.log("picture saved to :"+path)
        }
        captureMode: Camera.CaptureStillImage
        focus.focusMode: Camera.FocusMacro
    }

    VideoOutput{
        //our viewfinder to show what the Camera sees
        id:viewfinder
        source:camera
        visible: !previewImage.visible
        fillMode: VideoOutput.PreserveAspectFit
        autoOrientation: true
    }

    Image{
        id:previewImage
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        visible: false
        MouseArea{
            anchors.fill: parent;
            onClicked:parent.visible = false
        }
    }

    Text{
        id:qrCodeFound
        anchors.centerIn: parent
        width:200; height:50
        text:"Not found"
        color:"white"
    }
}
