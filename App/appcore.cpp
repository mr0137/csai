#include "appcore.h"

#include <csaibase.h>

AppCore::AppCore(QObject *parent) : QObject(parent)
{
    setTheme(QVariantMap{
                 {"textColor", "#6ab2f2"},
                 {"lightBackgroundColor", QColor("#24303d").lighter(130)},
                 {"darkBackgroundColor", "#232e3c"},
                 {"buttonColor", "#2f6ea5"},
                 {"highlightColor", "#6ab2f2"}
             });
}

void AppCore::proceedConfig(CSAIBase *algo)
{
    if (algo == nullptr) return;
    setAppTitle(algo->titleName());
    setAlgorithm(algo->algorithm());
    setTheme(algo->theme());
    controller()->setAlgorithm(algo->algorithm());
}

void AppCore::saveCode(QString code, QString path)
{
    QImage img = algorithm()->generate(code, 512, 512, "black");
    path.remove("file:///");
    qDebug() << "path" << path << code << img.isNull();
    img.save(path);
}
