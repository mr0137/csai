#ifndef CAMERACONTROLER_H
#define CAMERACONTROLER_H

#include <QObject>
#include <kmacro.h>
class IAlgorithm;
class CameraController :  public QObject
{
    Q_OBJECT
    K_QML_TYPE(CameraController)
public:
    explicit CameraController(QObject *parent = 0);
    void setAlgorithm(IAlgorithm* algo);
signals:
    void errorMessage(QString message);
    void decodingStarted();
    void decodingFinished(bool succeeded);
    void tagFound(QString idScanned);
public slots:
    void decodeQMLImage(QObject *imageObj);
private:
    IAlgorithm *m_algorithm = nullptr;
};

#endif // CAMERACONTROLER_H

