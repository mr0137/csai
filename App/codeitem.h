#ifndef CODEITEM_H
#define CODEITEM_H

#include <QQuickPaintedItem>
#include <kmacro.h>
#include <ialgorithm.h>
#include <appcore.h>

class CodeItem : public QQuickItem
{
    Q_OBJECT
    K_QML_TYPE(CodeItem)
    K_AUTO_PROPERTY(QString, code, code, setCode, codeChanged, "12345")
    K_AUTO_PROPERTY(QColor, color, color, setColor, colorChanged, "green")
    K_AUTO_PROPERTY(IAlgorithm*, algorithm, algorithm, setAlgorithm, algorithmChanged, AppCore::instance()->algorithm())
public:
    CodeItem();

signals:

    // QQuickItem interface
protected:
    virtual QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *) override;
private:
    QImage img;
};

#endif // CODEITEM_H
