#include "codeitem.h"

#include <QPainter>
#include <QSGFlatColorMaterial>
#include <QSGGeometryNode>
#include <QSGSimpleTextureNode>
#include <QQuickWindow>

CodeItem::CodeItem()
{
    setFlag(QQuickItem::ItemHasContents, true);
    connect(this, &CodeItem::codeChanged, this, [this](){
        img = algorithm()->generate(code(), width(), height(), color());
        update();
    });

    connect(this, &CodeItem::heightChanged, this, [this](){
        img = algorithm()->generate(code(), width(), height(), color());
        update();
    });
}

QSGNode *CodeItem::updatePaintNode(QSGNode *node, QQuickItem::UpdatePaintNodeData *)
{
    QSGGeometryNode *rectNode;
    QSGGeometry *g;
    if (node == nullptr) {
        rectNode = new QSGGeometryNode;
        // geometry
        g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
        g->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rectNode->setGeometry(g);
        // rect color
        auto m = new QSGFlatColorMaterial();
        m->setColor("transparent");
        rectNode->setMaterial(m);
    } else {
        rectNode = static_cast<QSGGeometryNode*>(node);
        g = rectNode->geometry();
    }

    //update rect geom
    g->vertexDataAsPoint2D()[0].set(0, 0);
    g->vertexDataAsPoint2D()[1].set(width(), 0);
    g->vertexDataAsPoint2D()[2].set(width(), height());
    g->vertexDataAsPoint2D()[3].set(0, height());
    rectNode->markDirty(QSGNode::DirtyGeometry);
    //header text node
    // setup text node
    QSGSimpleTextureNode *textNode = nullptr;
    if (rectNode->childCount()>0) {
        textNode = static_cast<QSGSimpleTextureNode *>(rectNode->childAtIndex(0));
    } else {
        textNode = new QSGSimpleTextureNode();
        textNode->setOwnsTexture(true);
        rectNode->appendChildNode(textNode);
    }
    //draw image with texts

    auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
    textNode->setRect(QRectF(0,0, width(), height()));
    textNode->setTexture(t);

    return rectNode;
}
