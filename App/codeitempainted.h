#ifndef CODEITEMPAINTED_H
#define CODEITEMPAINTED_H

#include <QQuickPaintedItem>
#include <QQuickItem>
#include <qzint.h>
#include <kmacro.h>

class CodeItemPainted : public QQuickPaintedItem
{
    Q_OBJECT
    K_QML_TYPE(CodeItemPainted)
public:
    CodeItemPainted();

    // QQuickPaintedItem interface
public:
    virtual void paint(QPainter *painter) override;

private:
    mutable Zint::QZint bc;
};

#endif // CODEITEMPAINTED_H
