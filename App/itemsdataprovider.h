#ifndef ITEMSDATAPROVIDER_H
#define ITEMSDATAPROVIDER_H

#include <QFuture>
#include <QObject>
#include <itabledataprovider.h>
#include <kmacro.h>

class ItemsDataProvider : public ITableDataProvider
{
    Q_OBJECT
    K_AUTO_PROPERTY(QString, currentTable, currentTable, setCurrentTable, currentTableChanged, "tableNames")
    K_AUTO_PROPERTY(QString, currentFilter, currentFilter, setCurrentFilter, currentFilterChanged, "All")
    K_READONLY_PROPERTY(QVariantList, types, types, setTypes, typesChanged, QVariantList())
    K_READONLY_PROPERTY(QVariantList, manufacturers, manufacturers, setManufacturers, manufacturersChanged, QVariantList())
    K_QML_TYPE(ItemsDataProvider)
    public:
        explicit ItemsDataProvider(QObject *parent = nullptr);
public:
    virtual int rowCount() const override;
    virtual int columnCount() const override;
    virtual QVector<QString> headerData() const override;
    virtual void prepareRow(int row) override;
    virtual QVector<QString> getRowData(int row) override;
    virtual Row getRow(int row) override;
    virtual void sort(int column) override;
    virtual bool isFullLoaded() override;
    virtual int rowById(int id) override;
    virtual bool isEditable() override;
public slots:
    bool addType(QString name);
    bool addProduct(QString name, QString manufacturer, QString type, int count);
    bool addManufacturer(QString name);
    bool editCount(int count, int row);
    bool removeProduct(int row);
    bool removeType(QString name);
    bool removeManufacturer(QString name);
    QString generateCode(int row);
    int getRowByCode(QString code);
    QString getManufacturerCode(QString manufacturer);
    QString getProductNameCode(QString productName);

private slots:
    void reload();
private:
    int m_rowCount = 0;
    int m_columnCount = 0;

    QVector<QString> m_headers;
    QFuture<void> m_loadFuture;
    bool m_loadAllData = true;
    QMap<int, Row> m_rowCache;
    QMap<QString, int> m_manufacturerCode;
    QMap<QString, int> m_productNameCode;

    bool m_threadCouldRun = true;
    QString defaultColumnsQuery = " ID INTEGER PRIMARY KEY AUTOINCREMENT, ProductName TEXT NOT NULL, Manufacturer INTEGER NOT NULL, Count INTEGER NOT NULL";


    // ITableDataProvider interface
public:
    virtual void setCellData(int row, int column, QVariant value) override;
};

#endif // ITEMSDATAPROVIDER_H
