QT -= gui
QT += core
QT += quick
TEMPLATE = lib
DEFINES += CSAIBASE_LIBRARY

CONFIG += c++17
CONFIG += shared dll

CONFIG(release, debug|release){
DESTDIR = $$PWD/../../bin/libs
}

SOURCES += \
    csaibase.cpp \
    ialgorithm.cpp

HEADERS += \
    CSAIBase_global.h \
    csaibase.h \
    ialgorithm.h

