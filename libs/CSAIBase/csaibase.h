#ifndef CSAIBASE_H
#define CSAIBASE_H

#include "CSAIBase_global.h"
#include <ialgorithm.h>

#define CSAIBase_iid "com.my.CSAIBase"

class CSAIBASE_EXPORT CSAIBase
{
public:
    virtual QString titleName() = 0;
    virtual QVariantMap theme() = 0;
    virtual IAlgorithm* algorithm() = 0;
    virtual QIcon appIcon() = 0;
};

Q_DECLARE_INTERFACE(CSAIBase, CSAIBase_iid)

#endif // CSAIBASE_H
