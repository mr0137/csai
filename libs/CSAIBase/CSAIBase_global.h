#ifndef CSAIBASE_GLOBAL_H
#define CSAIBASE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CSAIBASE_LIBRARY)
#  define CSAIBASE_EXPORT Q_DECL_EXPORT
#else
#  define CSAIBASE_EXPORT Q_DECL_IMPORT
#endif

#endif // CSAIBASE_GLOBAL_H
