#ifndef IALGORITHM_H
#define IALGORITHM_H

#include <QImage>
#include <QString>
#include <QtQml>
#include "CSAIBase_global.h"

class CSAIBASE_EXPORT IAlgorithm : public QObject
{
    Q_OBJECT
public:
    explicit IAlgorithm(QObject *parent = nullptr) : QObject(parent) {};
    virtual QImage generate(QString code, double width, double height, QColor color = "green") = 0;
    virtual void decode(const QImage &img) = 0;
signals:
    void decodingFinished(bool);
    void decodingStarted();
    void tagFound(QString result);
    void error(QString result);
};

#endif // IALGORITHM_H
