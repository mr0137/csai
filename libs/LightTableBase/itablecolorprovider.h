#ifndef ITABLECOLORPROVIDER_H
#define ITABLECOLORPROVIDER_H

#include <QColor>
#include <QObject>
#include <QMap>
#include "lighttablebase_global.h"

#define ColorScheme QMap<int, QColor>

class Row;
class LIGHTTABLEBASE_EXPORT ITableColorProvider : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QColor selectionColor READ selectionColor WRITE setSelectionColor NOTIFY selectionColorChanged)
public:
    explicit ITableColorProvider(QObject *parent = nullptr);
    /*!
     * \brief disselect disselect row with Row::id
     * \param id
     */
    virtual void disselect(int id) = 0;
    /*!
     * \brief disselect disselect row sequance with Row::id
     * \param id
     */
    virtual void disselect(const QVariantList &id) = 0;
    /*!
     * \brief select select row with Row::id
     * \param id
     */
    virtual void select(int id) = 0;
    /*!
     * \brief select select row sequance with Row::id
     * \param id
     */
    virtual void select(const QVariantList &id) = 0;
    /*!
     * \brief hasSelection Method return true if Row selected
     * \param row
     * \return
     */
    virtual bool hasSelection(const Row &row) = 0;
    /*!
     * \brief hasSelection Method return true if Row::id selected
     * \param row
     * \return
     */
    virtual bool hasSelection(const int &id) = 0;
    /*!
     * \brief selectedID Method return selected ID
     * \return
     */
    virtual QVector<int> selectedID() = 0;
    /*!
     * \brief setSelectionColor Method set color of selection
     * \param color
     */
    void setSelectionColor(QColor color);
    QColor selectionColor();
signals:
    void schemeChanged();
    void selectionColorChanged();
protected:
    QColor m_selectionColor = "black";
};

#endif // ITABLECOLORPROVIDER_H
