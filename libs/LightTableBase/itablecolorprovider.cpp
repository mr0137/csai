#include "itablecolorprovider.h"

ITableColorProvider::ITableColorProvider(QObject *parent) : QObject(parent)
{

}

void ITableColorProvider::setSelectionColor(QColor color) {
    if (m_selectionColor == color) return;

    m_selectionColor = color;
    emit schemeChanged();
    emit selectionColorChanged();
}

QColor ITableColorProvider::selectionColor()
{
    return m_selectionColor;
}

