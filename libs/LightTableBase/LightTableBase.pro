QT += gui
QT += core
QT += quick
TEMPLATE = lib
DEFINES += LIGHTTABLEBASE_LIBRARY

CONFIG += c++17
CONFIG += shared dll

#CONFIG(release, debug|release){
#DESTDIR = $$PWD/../../bin/libs
#}
android :{
DEFINES += REQUEST_PERMISSIONS_ON_ANDROID
}

SOURCES += \
    itablecolorprovider.cpp \
    itabledataprovider.cpp

HEADERS += \
    itablecolorprovider.h \
    itabledataprovider.h \
    lighttablebase_global.h

