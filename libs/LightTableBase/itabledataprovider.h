#ifndef ITABLEDATAPROVIDER_H
#define ITABLEDATAPROVIDER_H

#include <QColor>
#include <QObject>
#include <QVariant>
#include "lighttablebase_global.h"


struct Cell {
    QColor textColor = "black";
    QColor backgroundColor = "red";
    QString displayText;

    enum Flags {
        STYLE_BGCOLOR,
        STYLE_TXTCOLOR,
        OWN_BGCOLOR,
        OWN_TXTCOLOR
    };

    Q_DECLARE_FLAGS(CellFlags, Flags)
    CellFlags flags;
};

struct Row {
    int id = -1;
    QList<Cell> cells;
};

class LIGHTTABLEBASE_EXPORT ITableDataProvider : public QObject
{
    Q_OBJECT
public:
    enum Reason{ADDED, UPDATED, REMOVED};
    Q_ENUM(Reason)
    explicit ITableDataProvider(QObject *parent = nullptr) : QObject(parent) { qRegisterMetaType<Reason>("Reason");}
    virtual int rowCount() const = 0;
    virtual int columnCount() const = 0;
    virtual QVector<QString> headerData() const = 0;

    virtual void prepareRow(int row) = 0;
    virtual QVector<QString> getRowData(int row) = 0;
    virtual Row getRow(int row) = 0;

    virtual void sort(int column) = 0;
    virtual bool isFullLoaded() = 0;
    virtual void fetchMore() {};

    virtual int rowById(int id) = 0;
    virtual bool isEditable() { return false; }

    virtual void setCellData(int row, int column, QVariant value) {
        Q_UNUSED(row)
        Q_UNUSED(column)
        Q_UNUSED(value)
    }

signals:
    void reseted();
    void rowsUpdated(int start, int end, Reason r = ADDED);
    void fullLoaded();
    void columnCountChanged();
};
#endif // ITABLEDATAPROVIDER_H
